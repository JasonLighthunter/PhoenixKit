import XCTest
@testable import Reactions

class MediaReactionVoteTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "mediaReactionVotes",
    "links": [
      "self": "https://kitsu.io/api/edge/media-reaction-votes/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "4",
//    "type": "mediaReactionVotes",
//    "links": [
//      "self": "https://kitsu.io/api/edge/media-reaction-votes/4"
//    ],
//    "attributes": [
//      "createdAt": "2017-08-08T12:39:19.217Z",
//      "updatedAt": "2017-08-08T12:39:19.217Z"
//    ]
//  ]

//  let validNilDataJSON: [String : Any] = [
//    "id": "4",
//    "type": "mediaReactionVotes",
//    "links": [
//      "self": "https://kitsu.io/api/edge/media-reaction-votes/4"
//    ],
//    "attributes": [
//      "createdAt": "2017-08-08T12:39:19.217Z",
//      "updatedAt": "2017-08-08T12:39:19.217Z"
//    ]
//  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "mediaReactionVotes",
    "links": [
      "self": "https://kitsu.io/api/edge/media-reaction-votes/4"
    ],
    "attributes": [
      "updatedAt": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "mediaReactionVotes",
    "links": [
      "self": "https://kitsu.io/api/edge/media-reaction-votes/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": nil
    ]
  ]

  var mediaReactionVote: MediaReactionVote?
  var mediaReactionVoteAttributes: MediaReactionVoteAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    mediaReactionVote = nil
    mediaReactionVoteAttributes = nil
    
    super.tearDown()
  }
  
  func testMediaReactionVoteFullyFilled() {
    mediaReactionVote = parseJson(fullyFilledJSON)
    mediaReactionVoteAttributes = mediaReactionVote?.attributes
    
    XCTAssertNotNil(mediaReactionVote)
    
    XCTAssertEqual(mediaReactionVote?.objectID, "4")
    XCTAssertEqual(mediaReactionVote?.type, "mediaReactionVotes")
    
    XCTAssertNotNil(mediaReactionVoteAttributes)

    XCTAssertEqual(
      mediaReactionVoteAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      mediaReactionVoteAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
  }
  
//  func testMediaReactionVoteValidMissingData() {
//    let json = validMissingDataJSON
//
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      mediaReactionVote = try? decoder.decode(MediaReactionVote.self, from: data!)
//    } else {
//      mediaReactionVote = nil
//    }
//    mediaReactionVoteAttributes = mediaReactionVote?.attributes
//
//    XCTAssertNotNil(mediaReactionVote)
//
//    XCTAssertEqual(mediaReactionVote?.objectID, "4")
//    XCTAssertEqual(mediaReactionVote?.type, "mediaReactionVotes")
//
//    XCTAssertNotNil(mediaReactionVoteAttributes)
//
//    XCTAssertEqual(mediaReactionVoteAttributes?.createdAt, "2017-08-08T12:39:19.217Z")
//    XCTAssertEqual(mediaReactionVoteAttributes?.updatedAt, "2017-08-08T12:39:19.217Z")
//  }

//  func testMediaReactionVoteValidNilData() {
//    let json = validNilDataJSON
//
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      mediaReactionVote = try? decoder.decode(MediaReactionVote.self, from: data!)
//    } else {
//      mediaReactionVote = nil
//    }
//    mediaReactionVoteAttributes = mediaReactionVote?.attributes
//
//    XCTAssertNotNil(mediaReactionVote)
//
//    XCTAssertEqual(mediaReactionVote?.objectID, "4")
//    XCTAssertEqual(mediaReactionVote?.type, "mediaReactionVotes")
//
//    XCTAssertNotNil(mediaReactionVoteAttributes)
//
//    XCTAssertEqual(mediaReactionVoteAttributes?.createdAt, "2017-08-08T12:39:19.217Z")
//    XCTAssertEqual(mediaReactionVoteAttributes?.updatedAt, "2017-08-08T12:39:19.217Z")
//  }
  
  func testMediaReactionVoteInvalidMissingData() {
    mediaReactionVote = parseJson(invalidMissingDataJSON)
    mediaReactionVoteAttributes = mediaReactionVote?.attributes
    
    XCTAssertNotNil(mediaReactionVote)
    
    XCTAssertEqual(mediaReactionVote?.objectID, "1")
    XCTAssertEqual(mediaReactionVote?.type, "mediaReactionVotes")
    
    XCTAssertNil(mediaReactionVoteAttributes)
  }
  
  func testMediaReactionVoteInvalidNilData() {
    mediaReactionVote = parseJson(invalidNilDataJSON)
    mediaReactionVoteAttributes = mediaReactionVote?.attributes
    
    XCTAssertNotNil(mediaReactionVote)
    
    XCTAssertEqual(mediaReactionVote?.objectID, "1")
    XCTAssertEqual(mediaReactionVote?.type, "mediaReactionVotes")
    
    XCTAssertNil(mediaReactionVoteAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> MediaReactionVote? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(MediaReactionVote.self, from: data!)
  }
}
