import XCTest
@testable import Reactions

class MediaReactionTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "mediaReactions",
    "links": [
      "self": "https://kitsu.io/api/edge/media-reactions/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "reaction": "A well-crafted story of paradoxical pain and suffering.",
      "upVotesCount": 6
    ]
  ]

  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "mediaReactions",
    "links": [
      "self": "https://kitsu.io/api/edge/media-reactions/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "upVotesCount": 6
    ]
  ]
  
  let validNilDataJSON: [String : Any] = [
    "id": "4",
    "type": "mediaReactions",
    "links": [
      "self": "https://kitsu.io/api/edge/media-reactions/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "reaction": nil,
      "upVotesCount": 6
    ]
  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "mediaReactions",
    "links": [
      "self": "https://kitsu.io/api/edge/mediaReactions/4"
    ],
    "attributes": [
      "updatedAt": "",
      "reaction": "",
      "upVotesCount": 6
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "mediaReactions",
    "links": [
      "self": "https://kitsu.io/api/edge/mediaReactions/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": "",
      "reaction": "",
      "upVotesCount": 6
    ]
  ]

  var mediaReaction: MediaReaction?
  var mediaReactionAttributes: MediaReactionAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    mediaReaction = nil
    mediaReactionAttributes = nil

    super.tearDown()
  }

  func testMediaReactionFullyFilled() {
    mediaReaction = parseJson(fullyFilledJSON)
    mediaReactionAttributes = mediaReaction?.attributes

    XCTAssertNotNil(mediaReaction)

    XCTAssertEqual(mediaReaction?.objectID, "4")
    XCTAssertEqual(mediaReaction?.type, "mediaReactions")

    XCTAssertNotNil(mediaReactionAttributes)

    XCTAssertEqual(
      mediaReactionAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      mediaReactionAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(mediaReactionAttributes?.reaction,
                   "A well-crafted story of paradoxical pain and suffering.")
    XCTAssertEqual(mediaReactionAttributes?.upVotesCount, 6)
  }

  func testMediaReactionValidMissingData() {
    mediaReaction = parseJson(validMissingDataJSON)
    mediaReactionAttributes = mediaReaction?.attributes

    XCTAssertNotNil(mediaReaction)

    XCTAssertEqual(mediaReaction?.objectID, "4")
    XCTAssertEqual(mediaReaction?.type, "mediaReactions")

    XCTAssertNotNil(mediaReactionAttributes)
    
    XCTAssertEqual(
      mediaReactionAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      mediaReactionAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertNil(mediaReactionAttributes?.reaction)
    XCTAssertEqual(mediaReactionAttributes?.upVotesCount, 6)
  }
  
  func testMediaReactionValidNilData() {
    mediaReaction = parseJson(validNilDataJSON)
    mediaReactionAttributes = mediaReaction?.attributes
    
    XCTAssertNotNil(mediaReaction)
    
    XCTAssertEqual(mediaReaction?.objectID, "4")
    XCTAssertEqual(mediaReaction?.type, "mediaReactions")
    
    XCTAssertNotNil(mediaReactionAttributes)

    XCTAssertEqual(
      mediaReactionAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      mediaReactionAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertNil(mediaReactionAttributes?.reaction)
    XCTAssertEqual(mediaReactionAttributes?.upVotesCount, 6)
  }

  func testMediaReactionInvalidMissingData() {
    mediaReaction = parseJson(invalidMissingDataJSON)
    mediaReactionAttributes = mediaReaction?.attributes
    
    XCTAssertNotNil(mediaReaction)
    
    XCTAssertEqual(mediaReaction?.objectID, "1")
    XCTAssertEqual(mediaReaction?.type, "mediaReactions")
    
    XCTAssertNil(mediaReactionAttributes)
  }
  
  func testMediaReactionInvalidNilData() {
    mediaReaction = parseJson(invalidNilDataJSON)
    mediaReactionAttributes = mediaReaction?.attributes
    
    XCTAssertNotNil(mediaReaction)
    
    XCTAssertEqual(mediaReaction?.objectID, "1")
    XCTAssertEqual(mediaReaction?.type, "mediaReactions")
    
    XCTAssertNil(mediaReactionAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> MediaReaction? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(MediaReaction.self, from: data!)
  }
}
