import XCTest
@testable import CharactersAndPeople

class MediaCharacterRoleTests: XCTestCase {
  func testMediaCharacterRole() {
    XCTAssertEqual(MediaCharacterRole(rawValue: "main"), .main)
    XCTAssertEqual(MediaCharacterRole(rawValue: "supporting"), .supporting)
    XCTAssertNil(MediaCharacterRole(rawValue: "InvalidInput"))
  }
}
