import XCTest
@testable import CharactersAndPeople

class ProducerTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "producers",
    "links": [
      "self": "https://kitsu.io/api/edge/producers/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
      "slug": "sunrise",
      "name": "Sunrise"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "producers",
    "links": [
      "self": "https://kitsu.io/api/edge/producers/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "producers",
    "links": [
      "self": "https://kitsu.io/api/edge/producers/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
      "slug": nil,
      "name": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/producers/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "producers",
    "links": [
      "self": "https://kitsu.io/api/edge/producers/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var producer: Producer?
  var producerAttributes: ProducerAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    producer = nil
    producerAttributes = nil
    
    super.tearDown()
  }
  
  func testProducerFullyFilled() {
    producer = parseJson(fullyFilledJSON)
    producerAttributes = producer?.attributes
    
    XCTAssertNotNil(producer)
    
    XCTAssertEqual(producer?.objectID, "4")
    XCTAssertEqual(producer?.type, "producers")
    
    XCTAssertNotNil(producerAttributes)
    
    XCTAssertEqual(
      producerAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      producerAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertEqual(producerAttributes?.slug, "sunrise")
    XCTAssertEqual(producerAttributes?.name, "Sunrise")
  }
  
  func testProducerValidMissingData() {
    producer = parseJson(validMissingDataJSON)
    producerAttributes = producer?.attributes
    
    XCTAssertNotNil(producer)
    
    XCTAssertEqual(producer?.objectID, "4")
    XCTAssertEqual(producer?.type, "producers")
    
    XCTAssertNotNil(producerAttributes)
    
    XCTAssertEqual(
      producerAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      producerAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertNil(producerAttributes?.slug)
    XCTAssertNil(producerAttributes?.name)
  }
  
  func testProducerValidNilData() {
    producer = parseJson(validNilDataJSON)
    producerAttributes = producer?.attributes
    
    XCTAssertNotNil(producer)
    
    XCTAssertEqual(producer?.objectID, "4")
    XCTAssertEqual(producer?.type, "producers")
    
    XCTAssertNotNil(producerAttributes)
    
    XCTAssertEqual(
      producerAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      producerAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertNil(producerAttributes?.slug)
    XCTAssertNil(producerAttributes?.name)
  }
  
  func testProducerInvalidMissingData() {
    producer = parseJson(invalidMissingDataJSON)

    XCTAssertNil(producer)
  }
  
  func testProducerInvalidNilData() {
    producer = parseJson(invalidNilDataJSON)

    XCTAssertNil(producer)
  }

  func parseJson(_ json: [String : Any?]) -> Producer? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Producer.self, from: data!)
  }
}
