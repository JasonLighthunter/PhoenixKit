import XCTest
@testable import CharactersAndPeople

class AnimeCharacterTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "animeCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-characters/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": "main"
    ]
  ]

  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "animeCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-characters/4"
    ],
    "attributes": [
      "role": "supporting"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "animeCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-characters/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": "supporting"
    ]
  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-characters/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "animeCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-characters/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]

  var animeCharacter: AnimeCharacter?
  var animeCharacterAttributes: AnimeCharacterAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    animeCharacter = nil
    animeCharacterAttributes = nil
    
    super.tearDown()
  }

  func testAnimeCharacterFullyFilled() {
    animeCharacter = parseJson(fullyFilledJSON)
    animeCharacterAttributes = animeCharacter?.attributes

    XCTAssertNotNil(animeCharacter)

    XCTAssertEqual(animeCharacter?.objectID, "4")
    XCTAssertEqual(animeCharacter?.type, "animeCharacters")
    
    XCTAssertNotNil(animeCharacterAttributes)

    XCTAssertEqual(
      animeCharacterAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      animeCharacterAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(animeCharacterAttributes?.role, .main)
  }

  func testAnimeCharacterValidMissingData() {
   animeCharacter = parseJson(validMissingDataJSON)
    animeCharacterAttributes = animeCharacter?.attributes
    
    XCTAssertNotNil(animeCharacter)
    
    XCTAssertEqual(animeCharacter?.objectID, "4")
    XCTAssertEqual(animeCharacter?.type, "animeCharacters")
    
    XCTAssertNotNil(animeCharacterAttributes)
    
    XCTAssertNil(animeCharacterAttributes?.createdAt)
    XCTAssertNil(animeCharacterAttributes?.updatedAt)
    XCTAssertEqual(animeCharacterAttributes?.role, .supporting)
  }
  
  func testAnimeCharacterValidNilData() {
    animeCharacter = parseJson(validNilDataJSON)
    animeCharacterAttributes = animeCharacter?.attributes
    
    XCTAssertNotNil(animeCharacter)
    
    XCTAssertEqual(animeCharacter?.objectID, "4")
    XCTAssertEqual(animeCharacter?.type, "animeCharacters")
    
    XCTAssertNotNil(animeCharacterAttributes)
    
    XCTAssertNil(animeCharacterAttributes?.createdAt)
    XCTAssertNil(animeCharacterAttributes?.updatedAt)
    XCTAssertEqual(animeCharacterAttributes?.role, .supporting)
  }

  func testAnimeCharacterInvalidMissingData() {
    animeCharacter = parseJson(invalidMissingDataJSON)

    XCTAssertNil(animeCharacter)
  }
  
  func testAnimeCharacterInvalidNilData() {
    animeCharacter = parseJson(invalidNilDataJSON)

    XCTAssertNil(animeCharacter)
  }

  func parseJson(_ json: [String : Any?]) -> AnimeCharacter? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(AnimeCharacter.self, from: data!)
  }
}
