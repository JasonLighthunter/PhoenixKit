import XCTest
@testable import CharactersAndPeople

class AnimeProductionRoleTests: XCTestCase {
  func testAnimeProductionRole() {
    XCTAssertEqual(AnimeProductionRole(rawValue: "producer"), .producer)
    XCTAssertEqual(AnimeProductionRole(rawValue: "licensor"), .licensor)
    XCTAssertEqual(AnimeProductionRole(rawValue: "studio"), .studio)
    XCTAssertNil(AnimeProductionRole(rawValue: "InvalidInput"))
  }
}
