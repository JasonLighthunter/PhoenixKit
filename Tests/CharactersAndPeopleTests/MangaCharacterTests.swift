import XCTest
@testable import CharactersAndPeople

class MangaCharacterTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "mangaCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-characters/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": "main"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "mangaCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-characters/4"
    ],
    "attributes": [
      "role": "supporting"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "mangaCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-characters/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": "supporting"
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-characters/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "mangaCharacters",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-characters/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var mangaCharacter: MangaCharacter?
  var mangaCharacterAttributes: MangaCharacterAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    mangaCharacter = nil
    mangaCharacterAttributes = nil
    
    super.tearDown()
  }
  
  func testMangaCharacterFullyFilled() {
    mangaCharacter = parseJson(fullyFilledJSON)
    mangaCharacterAttributes = mangaCharacter?.attributes
    
    XCTAssertNotNil(mangaCharacter)
    
    XCTAssertEqual(mangaCharacter?.objectID, "4")
    XCTAssertEqual(mangaCharacter?.type, "mangaCharacters")
    
    XCTAssertNotNil(mangaCharacterAttributes)
    
    XCTAssertEqual(
      mangaCharacterAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      mangaCharacterAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(mangaCharacterAttributes?.role, .main)
  }
  
  func testMangaCharacterValidMissingData() {
    mangaCharacter = parseJson(validMissingDataJSON)
    mangaCharacterAttributes = mangaCharacter?.attributes
    
    XCTAssertNotNil(mangaCharacter)
    
    XCTAssertEqual(mangaCharacter?.objectID, "4")
    XCTAssertEqual(mangaCharacter?.type, "mangaCharacters")
    
    XCTAssertNotNil(mangaCharacterAttributes)
    
    XCTAssertNil(mangaCharacterAttributes?.createdAt)
    XCTAssertNil(mangaCharacterAttributes?.updatedAt)
    XCTAssertEqual(mangaCharacterAttributes?.role, .supporting)
  }
  
  func testMangaCharacterValidNilData() {
    mangaCharacter = parseJson(validNilDataJSON)
    mangaCharacterAttributes = mangaCharacter?.attributes
    
    XCTAssertNotNil(mangaCharacter)
    
    XCTAssertEqual(mangaCharacter?.objectID, "4")
    XCTAssertEqual(mangaCharacter?.type, "mangaCharacters")
    
    XCTAssertNotNil(mangaCharacterAttributes)
    
    XCTAssertNil(mangaCharacterAttributes?.createdAt)
    XCTAssertNil(mangaCharacterAttributes?.updatedAt)
    XCTAssertEqual(mangaCharacterAttributes?.role, .supporting)
  }
  
  func testMangaCharacterInvalidMissingData() {
    mangaCharacter = parseJson(invalidMissingDataJSON)

    XCTAssertNil(mangaCharacter)
  }
  
  func testMangaCharacterInvalidNilData() {
    mangaCharacter = parseJson(invalidNilDataJSON)

    XCTAssertNil(mangaCharacter)
  }

  func parseJson(_ json: [String : Any?]) -> MangaCharacter? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(MangaCharacter.self, from: data!)
  }
}
