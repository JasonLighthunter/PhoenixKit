import XCTest
@testable import CharactersAndPeople

class CharacterTests: XCTestCase {
  typealias KitsuCharacter = CharactersAndPeople.Character

  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "characters",
    "links": [
      "self": "https://kitsu.io/api/edge/characters/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
      "slug": "jet",
      "name": "Jet Black",
      "malId": 3,
      "description": "description",
      "image": [
        "original": "https://media.kitsu.io/characters/images/1/original.jpg?1483096805"
      ]
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "characters",
    "links": [
      "self": "https://kitsu.io/api/edge/characters/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "characters",
    "links": [
      "self": "https://kitsu.io/api/edge/characters/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
      "slug": nil,
      "name": nil,
      "malId": nil,
      "description": nil,
      "image": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/characters/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "characters",
    "links": [
      "self": "https://kitsu.io/api/edge/characters/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil
    ]
  ]
  
  var character: KitsuCharacter?
  var characterAttributes: CharacterAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    character = nil
    characterAttributes = nil
    
    super.tearDown()
  }
  
  func testCharacterFullyFilled() {
    character = parseJson(fullyFilledJSON)
    characterAttributes = character?.attributes
    
    XCTAssertNotNil(character)
    
    XCTAssertEqual(character?.objectID, "4")
    XCTAssertEqual(character?.type, "characters")
    
    XCTAssertNotNil(characterAttributes)
    
    XCTAssertEqual(
      characterAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      characterAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertEqual(characterAttributes?.slug, "jet")
    XCTAssertEqual(characterAttributes?.name, "Jet Black")
    XCTAssertEqual(characterAttributes?.myAnimeListID, 3)
    XCTAssertEqual(characterAttributes?.description, "description")
    XCTAssertEqual(
      characterAttributes?.image?.original,
      URL(string: "https://media.kitsu.io/characters/images/1/original.jpg?1483096805")
    )
  }
  
  func testCharacterValidMissingData() {
    character = parseJson(validMissingDataJSON)
    characterAttributes = character?.attributes
    
    XCTAssertNotNil(character)
    
    XCTAssertEqual(character?.objectID, "4")
    XCTAssertEqual(character?.type, "characters")
    
    XCTAssertNotNil(characterAttributes)
    
    XCTAssertEqual(
      characterAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      characterAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertNil(characterAttributes?.slug)
    XCTAssertNil(characterAttributes?.name)
    XCTAssertNil(characterAttributes?.myAnimeListID)
    XCTAssertNil(characterAttributes?.description)
  }
  
  func testCharacterValidNilData() {
    character = parseJson(validNilDataJSON)
    characterAttributes = character?.attributes
    
    XCTAssertNotNil(character)
    
    XCTAssertEqual(character?.objectID, "4")
    XCTAssertEqual(character?.type, "characters")
    
    XCTAssertNotNil(characterAttributes)
    
    XCTAssertEqual(
      characterAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      characterAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertNil(characterAttributes?.slug)
    XCTAssertNil(characterAttributes?.name)
    XCTAssertNil(characterAttributes?.myAnimeListID)
    XCTAssertNil(characterAttributes?.description)
  }
  
  func testCharacterInvalidMissingData() {
    character = parseJson(invalidMissingDataJSON)

    XCTAssertNil(character)
  }
  
  func testCharacterInvalidNilData() {
    character = parseJson(invalidNilDataJSON)

    XCTAssertNil(character)
  }

  func parseJson(_ json: [String : Any?]) -> KitsuCharacter? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(KitsuCharacter.self, from: data!)
  }
}
