import XCTest
@testable import CharactersAndPeople

class AnimeStaffTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "animeStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-staff/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": "studio"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "animeStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-staff/4"
    ],
    "attributes": [:]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "animeStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-staff/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-staff/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "animeStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-staff/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var animeStaff: AnimeStaff?
  var animeStaffAttributes: AnimeStaffAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    animeStaff = nil
    animeStaffAttributes = nil
    
    super.tearDown()
  }
  
  func testAnimeStaffFullyFilled() {
    animeStaff = parseJson(fullyFilledJSON)
    animeStaffAttributes = animeStaff?.attributes
    
    XCTAssertNotNil(animeStaff)
    
    XCTAssertEqual(animeStaff?.objectID, "4")
    XCTAssertEqual(animeStaff?.type, "animeStaff")
    
    XCTAssertNotNil(animeStaffAttributes)
    
    XCTAssertEqual(
      animeStaffAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      animeStaffAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(animeStaffAttributes?.role, "studio")
  }
  
  func testAnimeStaffValidMissingData() {
    animeStaff = parseJson(validMissingDataJSON)
    animeStaffAttributes = animeStaff?.attributes
    
    XCTAssertNotNil(animeStaff)
    
    XCTAssertEqual(animeStaff?.objectID, "4")
    XCTAssertEqual(animeStaff?.type, "animeStaff")
    
    XCTAssertNotNil(animeStaffAttributes)
    
    XCTAssertNil(animeStaffAttributes?.createdAt)
    XCTAssertNil(animeStaffAttributes?.updatedAt)
    XCTAssertNil(animeStaffAttributes?.role)
  }
  
  func testAnimeStaffValidNilData() {
    animeStaff = parseJson(validNilDataJSON)
    animeStaffAttributes = animeStaff?.attributes
    
    XCTAssertNotNil(animeStaff)
    
    XCTAssertEqual(animeStaff?.objectID, "4")
    XCTAssertEqual(animeStaff?.type, "animeStaff")
    
    XCTAssertNotNil(animeStaffAttributes)
    
    XCTAssertNil(animeStaffAttributes?.createdAt)
    XCTAssertNil(animeStaffAttributes?.updatedAt)
    XCTAssertNil(animeStaffAttributes?.role)
  }
  
  func testAnimeStaffInvalidMissingData() {
    animeStaff = parseJson(invalidMissingDataJSON)

    XCTAssertNil(animeStaff)
  }
  
  func testAnimeStaffInvalidNilData() {
    animeStaff = parseJson(invalidNilDataJSON)

    XCTAssertNil(animeStaff)
  }

  func parseJson(_ json: [String : Any?]) -> AnimeStaff? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(AnimeStaff.self, from: data!)
  }
}
