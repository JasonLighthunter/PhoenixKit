import XCTest
@testable import CharactersAndPeople

class PersonTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "people",
    "links": [
      "self": "https://kitsu.io/api/edge/people/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
      "image": "https://media.kitsu.io/people/images/1/original.jpg?1416260317",
      "name": "Masahiko Minami",
      "malId": 6519
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "people",
    "links": [
      "self": "https://kitsu.io/api/edge/people/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "people",
    "links": [
      "self": "https://kitsu.io/api/edge/people/4"
    ],
    "attributes": [
      "createdAt": "2017-07-27T22:21:26.824Z",
      "updatedAt": "2017-07-27T22:47:45.129Z",
      "image": nil,
      "name": nil,
      "malId": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/people/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "people",
    "links": [
      "self": "https://kitsu.io/api/edge/people/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var person: Person?
  var personAttributes: PersonAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    person = nil
    personAttributes = nil
    
    super.tearDown()
  }
  
  func testPersonFullyFilled() {
    person = parseJson(fullyFilledJSON)
    personAttributes = person?.attributes
    
    XCTAssertNotNil(person)
    
    XCTAssertEqual(person?.objectID, "4")
    XCTAssertEqual(person?.type, "people")
    
    XCTAssertNotNil(personAttributes)
    
    XCTAssertEqual(
      personAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      personAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertEqual(
      personAttributes?.image,
      URL(string: "https://media.kitsu.io/people/images/1/original.jpg?1416260317")
    )
    XCTAssertEqual(personAttributes?.name, "Masahiko Minami")
    XCTAssertEqual(personAttributes?.myAnimeListID, 6519)
  }
  
  func testPersonValidMissingData() {
    person = parseJson(validMissingDataJSON)
    personAttributes = person?.attributes
    
    XCTAssertNotNil(person)
    
    XCTAssertEqual(person?.objectID, "4")
    XCTAssertEqual(person?.type, "people")
    
    XCTAssertNotNil(personAttributes)
    
    XCTAssertEqual(
      personAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      personAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertNil(personAttributes?.image)
    XCTAssertNil(personAttributes?.name)
    XCTAssertNil(personAttributes?.myAnimeListID)
  }
  
  func testPersonValidNilData() {
    person = parseJson(validNilDataJSON)
    personAttributes = person?.attributes
    
    XCTAssertNotNil(person)
    
    XCTAssertEqual(person?.objectID, "4")
    XCTAssertEqual(person?.type, "people")
    
    XCTAssertNotNil(personAttributes)
    
    XCTAssertEqual(
      personAttributes?.createdAt, dateFormatter.date(from: "2017-07-27T22:21:26.824Z")
    )
    XCTAssertEqual(
      personAttributes?.updatedAt, dateFormatter.date(from: "2017-07-27T22:47:45.129Z")
    )
    XCTAssertNil(personAttributes?.image)
    XCTAssertNil(personAttributes?.name)
    XCTAssertNil(personAttributes?.myAnimeListID)
  }
  
  func testPersonInvalidMissingData() {
    person = parseJson(invalidMissingDataJSON)

    XCTAssertNil(person)
  }
  
  func testPersonInvalidNilData() {
    person = parseJson(invalidNilDataJSON)

    XCTAssertNil(person)
  }

  func parseJson(_ json: [String : Any?]) -> Person? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Person.self, from: data!)
  }
}
