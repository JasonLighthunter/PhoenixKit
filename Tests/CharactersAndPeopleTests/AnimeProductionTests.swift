import XCTest
@testable import CharactersAndPeople

class AnimeProductionTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "animeProductions",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-productions/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": "studio"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "animeProductions",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-productions/4"
    ],
    "attributes": [
      "role": "studio"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "animeProductions",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-productions/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": "studio"
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-productions/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "animeProductions",
    "links": [
      "self": "https://kitsu.io/api/edge/anime-productions/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var animeProduction: AnimeProduction?
  var animeProductionAttributes: AnimeProductionAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    animeProduction = nil
    animeProductionAttributes = nil
    
    super.tearDown()
  }
  
  func testAnimeProductionFullyFilled() {
    animeProduction = parseJson(fullyFilledJSON)
    animeProductionAttributes = animeProduction?.attributes
    
    XCTAssertNotNil(animeProduction)
    
    XCTAssertEqual(animeProduction?.objectID, "4")
    XCTAssertEqual(animeProduction?.type, "animeProductions")
    
    XCTAssertNotNil(animeProductionAttributes)
    
    XCTAssertEqual(
      animeProductionAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      animeProductionAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(animeProductionAttributes?.role, .studio)
  }
  
  func testAnimeProductionValidMissingData() {
    animeProduction = parseJson(validMissingDataJSON)
    animeProductionAttributes = animeProduction?.attributes
    
    XCTAssertNotNil(animeProduction)
    
    XCTAssertEqual(animeProduction?.objectID, "4")
    XCTAssertEqual(animeProduction?.type, "animeProductions")
    
    XCTAssertNotNil(animeProductionAttributes)
    
    XCTAssertNil(animeProductionAttributes?.createdAt)
    XCTAssertNil(animeProductionAttributes?.updatedAt)
    XCTAssertEqual(animeProductionAttributes?.role, .studio)
  }
  
  func testAnimeProductionValidNilData() {
    animeProduction = parseJson(validNilDataJSON)
    animeProductionAttributes = animeProduction?.attributes
    
    XCTAssertNotNil(animeProduction)
    
    XCTAssertEqual(animeProduction?.objectID, "4")
    XCTAssertEqual(animeProduction?.type, "animeProductions")
    
    XCTAssertNotNil(animeProductionAttributes)
    
    XCTAssertNil(animeProductionAttributes?.createdAt)
    XCTAssertNil(animeProductionAttributes?.updatedAt)
    XCTAssertEqual(animeProductionAttributes?.role, .studio)
  }
  
  func testAnimeProductionInvalidMissingData() {
    animeProduction = parseJson(invalidMissingDataJSON)

    XCTAssertNil(animeProduction)
  }
  
  func testAnimeProductionInvalidNilData() {
    animeProduction = parseJson(invalidNilDataJSON)

    XCTAssertNil(animeProduction)
  }

  func parseJson(_ json: [String : Any?]) -> AnimeProduction? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(AnimeProduction.self, from: data!)
  }
}
