import XCTest
@testable import CharactersAndPeople

class MangaStaffTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "mangaStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-staff/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": "studio"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "mangaStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-staff/4"
    ],
    "attributes": [:]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "mangaStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-staff/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-staff/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "mangaStaff",
    "links": [
      "self": "https://kitsu.io/api/edge/manga-staff/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var mangaStaff: MangaStaff?
  var mangaStaffAttributes: MangaStaffAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    mangaStaff = nil
    mangaStaffAttributes = nil
    
    super.tearDown()
  }
  
  func testMangaStaffFullyFilled() {
    mangaStaff = parseJson(fullyFilledJSON)
    mangaStaffAttributes = mangaStaff?.attributes
    
    XCTAssertNotNil(mangaStaff)
    
    XCTAssertEqual(mangaStaff?.objectID, "4")
    XCTAssertEqual(mangaStaff?.type, "mangaStaff")
    
    XCTAssertNotNil(mangaStaffAttributes)
    
    XCTAssertEqual(
      mangaStaffAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      mangaStaffAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(mangaStaffAttributes?.role, "studio")
  }
  
  func testMangaStaffValidMissingData() {
    mangaStaff = parseJson(validMissingDataJSON)
    mangaStaffAttributes = mangaStaff?.attributes
    
    XCTAssertNotNil(mangaStaff)
    
    XCTAssertEqual(mangaStaff?.objectID, "4")
    XCTAssertEqual(mangaStaff?.type, "mangaStaff")
    
    XCTAssertNotNil(mangaStaffAttributes)
    
    XCTAssertNil(mangaStaffAttributes?.createdAt)
    XCTAssertNil(mangaStaffAttributes?.updatedAt)
    XCTAssertNil(mangaStaffAttributes?.role)
  }
  
  func testMangaStaffValidNilData() {
    mangaStaff = parseJson(validNilDataJSON)
    mangaStaffAttributes = mangaStaff?.attributes
    
    XCTAssertNotNil(mangaStaff)
    
    XCTAssertEqual(mangaStaff?.objectID, "4")
    XCTAssertEqual(mangaStaff?.type, "mangaStaff")
    
    XCTAssertNotNil(mangaStaffAttributes)
    
    XCTAssertNil(mangaStaffAttributes?.createdAt)
    XCTAssertNil(mangaStaffAttributes?.updatedAt)
    XCTAssertNil(mangaStaffAttributes?.role)
  }
  
  func testMangaStaffInvalidMissingData() {
    mangaStaff = parseJson(invalidMissingDataJSON)

    XCTAssertNil(mangaStaff)
  }
  
  func testMangaStaffInvalidNilData() {
    mangaStaff = parseJson(invalidNilDataJSON)

    XCTAssertNil(mangaStaff)
  }

  func parseJson(_ json: [String : Any?]) -> MangaStaff? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(MangaStaff.self, from: data!)
  }
}
