import XCTest
@testable import CharactersAndPeople

class CastingTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "castings",
    "links": [
      "self": "https://kitsu.io/api/edge/castings/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": "producer",
      "voiceActor": false,
      "featured": false,
      "language": "English"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "castings",
    "links": [
      "self": "https://kitsu.io/api/edge/castings/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "voiceActor": false,
      "featured": false,
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "castings",
    "links": [
      "self": "https://kitsu.io/api/edge/castings/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z",
      "role": nil,
      "voiceActor": false,
      "featured": false,
      "language": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "links": [
      "self": "https://kitsu.io/api/edge/castings/4"
    ],
    "attributes": [
      "createdAt": "",
      "updatedAt": "",
      "role": ""
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": nil,
    "type": "castings",
    "links": [
      "self": "https://kitsu.io/api/edge/castings/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": nil
    ]
  ]
  
  var casting: Casting?
  var castingAttributes: CastingAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    casting = nil
    castingAttributes = nil
    
    super.tearDown()
  }
  
  func testCastingFullyFilled() {
    casting = parseJson(fullyFilledJSON)
    castingAttributes = casting?.attributes
    
    XCTAssertNotNil(casting)
    
    XCTAssertEqual(casting?.objectID, "4")
    XCTAssertEqual(casting?.type, "castings")
    
    XCTAssertNotNil(castingAttributes)
    
    XCTAssertEqual(
      castingAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      castingAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(castingAttributes?.role, "producer")
    XCTAssertFalse((castingAttributes?.isVoiceActor)!)
    XCTAssertFalse((castingAttributes?.isFeatured)!)
    XCTAssertEqual(castingAttributes?.language, "English")
  }
  
  func testCastingValidMissingData() {
    casting = parseJson(validMissingDataJSON)
    castingAttributes = casting?.attributes
    
    XCTAssertNotNil(casting)
    
    XCTAssertEqual(casting?.objectID, "4")
    XCTAssertEqual(casting?.type, "castings")
    
    XCTAssertNotNil(castingAttributes)
    
    XCTAssertEqual(
      castingAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      castingAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertNil(castingAttributes?.role)
    XCTAssertFalse((castingAttributes?.isVoiceActor)!)
    XCTAssertFalse((castingAttributes?.isFeatured)!)
    XCTAssertNil(castingAttributes?.language)
  }
  
  func testCastingValidNilData() {
    casting = parseJson(validNilDataJSON)
    castingAttributes = casting?.attributes
    
    XCTAssertNotNil(casting)
    
    XCTAssertEqual(casting?.objectID, "4")
    XCTAssertEqual(casting?.type, "castings")
    
    XCTAssertNotNil(castingAttributes)
    
    XCTAssertEqual(
      castingAttributes?.createdAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      castingAttributes?.updatedAt, dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertNil(castingAttributes?.role)
    XCTAssertFalse((castingAttributes?.isVoiceActor)!)
    XCTAssertFalse((castingAttributes?.isFeatured)!)
    XCTAssertNil(castingAttributes?.language)
  }
  
  func testCastingInvalidMissingData() {
    casting = parseJson(invalidMissingDataJSON)

    XCTAssertNil(casting)
  }
  
  func testCastingInvalidNilData() {
    casting = parseJson(invalidNilDataJSON)

    XCTAssertNil(casting)
  }

  func parseJson(_ json: [String : Any?]) -> Casting? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Casting.self, from: data!)
  }
}
