import XCTest
@testable import Users

class StatKindTests: XCTestCase {
  func testStatKind() {
    XCTAssertEqual(StatKind(rawValue: "anime-category-breakdown"), .animeCategoryBreakdown)
    XCTAssertEqual(StatKind(rawValue: "manga-category-breakdown"), .mangaCategoryBreakdown)
    XCTAssertEqual(StatKind(rawValue: "anime-activity-history"), .animeActivityHistory)
    XCTAssertEqual(StatKind(rawValue: "manga-activity-history"), .mangaActivityHistory)
    XCTAssertEqual(StatKind(rawValue: "anime-favorite-year"), .animeFavoriteYear)
    XCTAssertEqual(StatKind(rawValue: "manga-favorite-year"), .mangaFavoriteYear)
    XCTAssertEqual(StatKind(rawValue: "anime-amount-consumed"), .animeAmountConsumed)
    XCTAssertEqual(StatKind(rawValue: "manga-amount-consumed"), .mangaAmountConsumed)
    XCTAssertNil(StatKind(rawValue: "InvalidInput"))
  }
}
