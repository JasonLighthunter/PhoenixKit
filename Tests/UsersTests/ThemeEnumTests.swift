import XCTest
@testable import Users

class ThemeTests: XCTestCase {
  func testTheme() {
    XCTAssertEqual(Theme(rawValue: "light"), .light)
    XCTAssertEqual(Theme(rawValue: "dark"), .dark)
    XCTAssertNil(Theme(rawValue: "InvalidInput"))
  }
}
