import XCTest
@testable import Users

class LinkedAccountTokenTests: XCTestCase {
  let decoder = JSONDecoder()
  
  let fullyFilledJSON: [String : Any] = [
    "resource_owner_id" : 29629,
    "scopes": [
      "public"
    ],
    "expires_in_seconds": 2592000,
    "application": [
        "uid" : "4"
    ],
    "created_at": 1508818017
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "resource_owner_id" : 29629,
    "scopes": [
    ],
    "expires_in_seconds": 2592000,
    "created_at": 1508818017
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "resource_owner_id" : 29629,
    "scopes": [
      "public"
    ],
    "expires_in_seconds": 2592000,
    "application": nil,
    "created_at": 1508818017
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "scopes": [
      "public"
    ],
    "expires_in_seconds": 2592000,
    "application": [
      "uid" : "4"
    ],
    "created_at": 1508818017
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "resource_owner_id" : 29629,
    "scopes": [
      "public"
    ],
    "expires_in_seconds": nil,
    "application": [
      "uid" : "4"
    ],
    "created_at": 1508818017
  ]
  
  var linkedAccountToken: LinkedAccountToken?
  
  override func tearDown() {
    linkedAccountToken = nil
    
    super.tearDown()
  }
  
  func testLinkedAccountTokenFullyFilled() {
    linkedAccountToken = parseJson(fullyFilledJSON)
    
    XCTAssertNotNil(linkedAccountToken)
    
    XCTAssertEqual(linkedAccountToken?.resourceOwnerID, 29629)
    XCTAssertEqual((linkedAccountToken?.scopes)!, fullyFilledJSON["scopes"] as! [String])
    XCTAssertEqual(linkedAccountToken?.expiresInSeconds, 2592000)
    
    XCTAssertNotNil(linkedAccountToken?.application)
    
    XCTAssertEqual(linkedAccountToken?.createdAt, 1508818017)
  }
  
  func testLinkedAccountTokenValidMissingData() {
    linkedAccountToken = parseJson(validMissingDataJSON)
    
    XCTAssertNotNil(linkedAccountToken)
    
    XCTAssertEqual(linkedAccountToken?.resourceOwnerID, 29629)
    XCTAssertEqual((linkedAccountToken?.scopes)!, [])
    XCTAssertEqual(linkedAccountToken?.expiresInSeconds, 2592000)
    
    XCTAssertNil(linkedAccountToken?.application)
    
    XCTAssertEqual(linkedAccountToken?.createdAt, 1508818017)
  }
  
  func testLinkedAccountTokenValidNilData() {
    linkedAccountToken = parseJson(validNilDataJSON)
    
    XCTAssertNotNil(linkedAccountToken)
    
    XCTAssertEqual(linkedAccountToken?.resourceOwnerID, 29629)
    XCTAssertEqual((linkedAccountToken?.scopes)!, validNilDataJSON["scopes"] as! [String])
    XCTAssertEqual(linkedAccountToken?.expiresInSeconds, 2592000)
    
    XCTAssertNil(linkedAccountToken?.application)
    
    XCTAssertEqual(linkedAccountToken?.createdAt, 1508818017)
  }
  
  func testLinkedAccountTokenInvalidMissingData() {
    linkedAccountToken = parseJson(invalidMissingDataJSON)
    
    XCTAssertNil(linkedAccountToken)
  }
  
  func testLinkedAccountTokenInvalidNilData() {
    linkedAccountToken = parseJson(invalidNilDataJSON)
    
    XCTAssertNil(linkedAccountToken)
  }

  func parseJson(_ json: [String : Any?]) -> LinkedAccountToken? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? decoder.decode(LinkedAccountToken.self, from: data!)
  }
}


