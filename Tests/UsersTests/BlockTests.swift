import XCTest
@testable import Users

class BlockTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "blocks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/blocks/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id" : "4",
//    "type" : "blocks",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/blocks/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z"
//    ]
//  ]

//  let validNilDataJSON: [String : Any?] = [
//    "id" : "4",
//    "type" : "blocks",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/blocks/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "blocks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/blocks/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "blocks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/blocks/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : nil
    ]
  ]
  
  var block: Block?
  var blockAttributes: BlockAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    block = nil
    blockAttributes = nil
    
    super.tearDown()
  }
  
  func testBlockFullyFilled() {
    block = parseJson(fullyFilledJSON)
    blockAttributes = block?.attributes
    
    XCTAssertNotNil(block)
    
    XCTAssertEqual(block?.objectID, "4")
    XCTAssertEqual(block?.type, "blocks")
    
    XCTAssertNotNil(blockAttributes)

    XCTAssertEqual(
      blockAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      blockAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
  }
  
//  func testBlockValidMissingData() {
//    block = parseJson(validMissingDataJSON)
//    blockAttributes = block?.attributes
//
//    XCTAssertNotNil(block)
//
//    XCTAssertEqual(block?.objectID, "4")
//    XCTAssertEqual(block?.type, "blocks")
//
//    XCTAssertNotNil(blockAttributes)
//
//    XCTAssertEqual(
//      blockAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      blockAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//  }
  
//  func testBlockValidNilData() {
//    block = parseJson(validNilDataJSON)
//    blockAttributes = block?.attributes
//
//    XCTAssertNotNil(block)
//
//    XCTAssertEqual(block?.objectID, "4")
//    XCTAssertEqual(block?.type, "blocks")
//
//    XCTAssertNotNil(blockAttributes)
//
//    XCTAssertEqual(blockAttributes?.createdAt, "2017-08-08T12:39:19.217Z")
//    XCTAssertEqual(blockAttributes?.updatedAt, "2017-08-08T12:39:19.217Z")
//  }

  func testBlockInvalidMissingData() {
    block = parseJson(invalidMissingDataJSON)
    blockAttributes = block?.attributes
    
    XCTAssertNotNil(block)
    
    XCTAssertEqual(block?.objectID, "4")
    XCTAssertEqual(block?.type, "blocks")
    
    XCTAssertNil(blockAttributes)
  }
  
  func testBlockInvalidNilData() {
    block = parseJson(invalidNilDataJSON)
    blockAttributes = block?.attributes
    
    XCTAssertNotNil(block)
    
    XCTAssertEqual(block?.objectID, "4")
    XCTAssertEqual(block?.type, "blocks")
    
    XCTAssertNil(blockAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Block? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Block.self, from: data!)
  }
}

