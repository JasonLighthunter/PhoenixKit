import XCTest
@testable import Users

class LinkedAccountTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1911",
    "type": "linkedAccounts",
    "links": [
      "self": "https://kitsu.io/api/edge/linked-accounts/1911"
    ],
    "attributes": [
      "createdAt": "2017-08-22T20:36:46.580Z",
      "updatedAt": "2017-08-22T20:36:46.580Z",
      "kind": "my-anime-list",
      "externalUserId": "synthtech",
      "token": [
        "resource_owner_id" : 29629,
        "scopes": [
          "public"
        ],
        "expires_in_seconds": 2592000,
        "application": [
          "uid": "4"
        ],
        "created_at": 1508818017
      ],
      "shareTo": false,
      "shareFrom": false,
      "syncTo": true,
      "disabledReason": "reason"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "1911",
    "type": "linkedAccounts",
    "links": [
      "self": "https://kitsu.io/api/edge/linked-accounts/1911"
    ],
    "attributes": [
      "createdAt": "2017-08-22T20:36:46.580Z",
      "updatedAt": "2017-08-22T20:36:46.580Z",
      "kind": "my-anime-list",
      "externalUserId": "synthtech",
      "shareTo": false,
      "shareFrom": false,
      "syncTo": true
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "1911",
    "type": "linkedAccounts",
    "links": [
      "self": "https://kitsu.io/api/edge/linked-accounts/1911"
    ],
    "attributes": [
      "createdAt": "2017-08-22T20:36:46.580Z",
      "updatedAt": "2017-08-22T20:36:46.580Z",
      "kind": "my-anime-list",
      "externalUserId": "synthtech",
      "token": nil,
      "shareTo": false,
      "shareFrom": false,
      "syncTo": true,
      "disabledReason": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1911",
    "type": "linkedAccounts",
    "links": [
      "self": "https://kitsu.io/api/edge/linked-accounts/1911"
    ],
    "attributes": [
      "createdAt": "2017-08-22T20:36:46.580Z",
      "updatedAt": "2017-08-22T20:36:46.580Z",
      "kind": "my-anime-list",
      "token": [
        "resource_owner_id" : 29629,
        "scopes": [
          "public"
        ],
        "expires_in_seconds": 2592000,
        "application": [
          "uid": "4"
        ],
        "created_at": 1508818017
      ],
      "shareTo": false,
      "shareFrom": false,
      "syncTo": true,
      "disabledReason": "reason"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1911",
    "type": "linkedAccounts",
    "links": [
      "self": "https://kitsu.io/api/edge/linked-accounts/1911"
    ],
    "attributes": [
      "createdAt": "2017-08-22T20:36:46.580Z",
      "updatedAt": "2017-08-22T20:36:46.580Z",
      "kind": nil,
      "externalUserId": "synthtech",
      "token": [
        "resource_owner_id" : 29629,
        "scopes": [
          "public"
        ],
        "expires_in_seconds": 2592000,
        "application": [
          "uid": "4"
        ],
        "created_at": 1508818017
      ],
      "shareTo": false,
      "shareFrom": false,
      "syncTo": true,
      "disabledReason": "reason"
    ]
  ]
  
  var linkedAccount: LinkedAccount?
  var linkedAccountAttributes: LinkedAccountAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    linkedAccount = nil
    linkedAccountAttributes = nil
    
    super.tearDown()
  }
  
  func testLinkedAccountFullyFilled() {
    linkedAccount = parseJson(fullyFilledJSON)
    linkedAccountAttributes = linkedAccount?.attributes

    XCTAssertNotNil(linkedAccount)
    
    XCTAssertEqual(linkedAccount?.objectID, "1911")
    XCTAssertEqual(linkedAccount?.type, "linkedAccounts")
    
    XCTAssertNotNil(linkedAccountAttributes)

    XCTAssertEqual(
      linkedAccountAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-22T20:36:46.580Z")
    )
    XCTAssertEqual(
      linkedAccountAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-22T20:36:46.580Z")
    )
    XCTAssertEqual(linkedAccountAttributes?.kind, "my-anime-list")
    XCTAssertEqual(linkedAccountAttributes?.externalUserID, "synthtech")
    
    XCTAssertNotNil(linkedAccountAttributes?.token)
    
    XCTAssertFalse((linkedAccountAttributes?.shareTo)!)
    XCTAssertFalse((linkedAccountAttributes?.shareFrom)!)
    XCTAssertTrue((linkedAccountAttributes?.syncTo)!)
    XCTAssertEqual(linkedAccountAttributes?.disabledReason, "reason")
  }
  
  func testLinkedAccountValidMissingData() {
    linkedAccount = parseJson(validMissingDataJSON)
    linkedAccountAttributes = linkedAccount?.attributes
    
    XCTAssertNotNil(linkedAccount)
    
    XCTAssertEqual(linkedAccount?.objectID, "1911")
    XCTAssertEqual(linkedAccount?.type, "linkedAccounts")
    
    XCTAssertNotNil(linkedAccountAttributes)
    
    XCTAssertEqual(
      linkedAccountAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-22T20:36:46.580Z")
    )
    XCTAssertEqual(
      linkedAccountAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-22T20:36:46.580Z")
    )
    XCTAssertEqual(linkedAccountAttributes?.kind, "my-anime-list")
    XCTAssertEqual(linkedAccountAttributes?.externalUserID, "synthtech")
    
    XCTAssertNil(linkedAccountAttributes?.token)
    
    XCTAssertFalse((linkedAccountAttributes?.shareTo)!)
    XCTAssertFalse((linkedAccountAttributes?.shareFrom)!)
    XCTAssertTrue((linkedAccountAttributes?.syncTo)!)
    
    XCTAssertNil(linkedAccountAttributes?.disabledReason)
  }
  
  func testLinkedAccountValidNilData() {
    linkedAccount = parseJson(validNilDataJSON)
    linkedAccountAttributes = linkedAccount?.attributes
    
    XCTAssertNotNil(linkedAccount)
    
    XCTAssertEqual(linkedAccount?.objectID, "1911")
    XCTAssertEqual(linkedAccount?.type, "linkedAccounts")
    
    XCTAssertNotNil(linkedAccountAttributes)
    
    XCTAssertEqual(
      linkedAccountAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-22T20:36:46.580Z")
    )
    XCTAssertEqual(
      linkedAccountAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-22T20:36:46.580Z")
    )
    XCTAssertEqual(linkedAccountAttributes?.kind, "my-anime-list")
    XCTAssertEqual(linkedAccountAttributes?.externalUserID, "synthtech")
    
    XCTAssertNil(linkedAccountAttributes?.token)
    
    XCTAssertFalse((linkedAccountAttributes?.shareTo)!)
    XCTAssertFalse((linkedAccountAttributes?.shareFrom)!)
    XCTAssertTrue((linkedAccountAttributes?.syncTo)!)
    
    XCTAssertNil(linkedAccountAttributes?.disabledReason)
  }
  
  func testLinkedAccountInvalidMissingData() {
    linkedAccount = parseJson(invalidMissingDataJSON)
    linkedAccountAttributes = linkedAccount?.attributes
    
    XCTAssertNotNil(linkedAccount)
    
    XCTAssertEqual(linkedAccount?.objectID, "1911")
    XCTAssertEqual(linkedAccount?.type, "linkedAccounts")
    
    XCTAssertNil(linkedAccountAttributes)
  }
  
  func testLinkedAccountInvalidNilData() {
    linkedAccount = parseJson(invalidNilDataJSON)
    linkedAccountAttributes = linkedAccount?.attributes
    
    XCTAssertNotNil(linkedAccount)
    
    XCTAssertEqual(linkedAccount?.objectID, "1911")
    XCTAssertEqual(linkedAccount?.type, "linkedAccounts")
    
    XCTAssertNil(linkedAccountAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> LinkedAccount? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(LinkedAccount.self, from: data!)
  }
}
