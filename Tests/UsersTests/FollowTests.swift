import XCTest
@testable import Users

class FollowTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "follows",
    "links" : [
      "self" : "https://kitsu.io/api/edge/follows/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id" : "4",
//    "type" : "follows",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/follows/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z"
//    ]
//  ]

//  let validNilDataJSON: [String : Any?] = [
//    "id" : "4",
//    "type" : "follows",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/follows/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "follows",
    "links" : [
      "self" : "https://kitsu.io/api/edge/follows/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "follows",
    "links" : [
      "self" : "https://kitsu.io/api/edge/follows/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : nil
    ]
  ]
  
  var follow: Follow?
  var followAttributes: FollowAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    follow = nil
    followAttributes = nil
    
    super.tearDown()
  }
  
  func testFollowFullyFilled() {
    follow = parseJson(fullyFilledJSON)
    followAttributes = follow?.attributes
    
    XCTAssertNotNil(follow)
    
    XCTAssertEqual(follow?.objectID, "4")
    XCTAssertEqual(follow?.type, "follows")
    
    XCTAssertNotNil(followAttributes)

    XCTAssertEqual(
      followAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      followAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
  }
  
//  func testFollowValidMissingData() {
//    follow = parseJson(validMissingDataJSON)
//    followAttributes = follow?.attributes
//
//    XCTAssertNotNil(follow)
//
//    XCTAssertEqual(follow?.objectID, "4")
//    XCTAssertEqual(follow?.type, "follows")
//
//    XCTAssertNotNil(followAttributes)
//
//    XCTAssertEqual(
//      followAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      followAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//  }

//  func testFollowValidNilData() {
//    follow = parseJson(validNilDataJSON)
//    followAttributes = follow?.attributes
//
//    XCTAssertNotNil(follow)
//
//    XCTAssertEqual(follow?.objectID, "4")
//    XCTAssertEqual(follow?.type, "follows")
//
//    XCTAssertNotNil(followAttributes)
//
//    XCTAssertEqual(
//      followAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      followAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//  }

  func testFollowInvalidMissingData() {
    follow = parseJson(invalidMissingDataJSON)
    followAttributes = follow?.attributes
    
    XCTAssertNotNil(follow)
    
    XCTAssertEqual(follow?.objectID, "4")
    XCTAssertEqual(follow?.type, "follows")
    
    XCTAssertNil(followAttributes)
  }
  
  func testFollowInvalidNilData() {
    follow = parseJson(invalidNilDataJSON)
    followAttributes = follow?.attributes
    
    XCTAssertNotNil(follow)
    
    XCTAssertEqual(follow?.objectID, "4")
    XCTAssertEqual(follow?.type, "follows")
    
    XCTAssertNil(followAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Follow? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Follow.self, from: data!)
  }
}
