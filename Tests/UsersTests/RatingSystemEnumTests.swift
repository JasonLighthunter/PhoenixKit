import XCTest
@testable import Users

class RatingSystemTests: XCTestCase {
  func testRatingSystem() {
    XCTAssertEqual(RatingSystem(rawValue: "advanced"), .advanced)
    XCTAssertEqual(RatingSystem(rawValue: "regular"), .regular)
    XCTAssertEqual(RatingSystem(rawValue: "simple"), .simple)
    XCTAssertNil(RatingSystem(rawValue: "InvalidInput"))
  }
}
