import XCTest
import Foundation
@testable import Users

class ProfileLinkTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "profileLinks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-links/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "url" : "http://example.com"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "profileLinks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-links/4"
    ],
    "attributes" : [
      "url" : "http://example.com"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "profileLinks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-links/4"
    ],
    "attributes" : [
      "createdAt" : nil,
      "updatedAt" : nil,
      "url" : "http://example.com"
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "profileLinks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-links/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "profileLinks",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-links/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "url" : nil
    ]
  ]
  
  var profileLink: ProfileLink?
  var profileLinkAttributes: ProfileLinkAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    profileLink = nil
    profileLinkAttributes = nil
    
    super.tearDown()
  }
  
  func testProfileLinkFullyFilled() {
    profileLink = parseJson(fullyFilledJSON)
    profileLinkAttributes = profileLink?.attributes
    
    XCTAssertNotNil(profileLink)
    
    XCTAssertEqual(profileLink?.objectID, "4")
    XCTAssertEqual(profileLink?.type, "profileLinks")
    
    XCTAssertNotNil(profileLinkAttributes)

    XCTAssertEqual(
      profileLinkAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      profileLinkAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(profileLinkAttributes?.url, URL(string: "http://example.com"))
  }
  
  func testProfileLinkValidMissingData() {
    profileLink = parseJson(validMissingDataJSON)
    profileLinkAttributes = profileLink?.attributes
    
    XCTAssertNotNil(profileLink)
    
    XCTAssertEqual(profileLink?.objectID, "4")
    XCTAssertEqual(profileLink?.type, "profileLinks")
    
    XCTAssertNotNil(profileLinkAttributes)
    
    XCTAssertNil(profileLinkAttributes?.createdAt)
    XCTAssertNil(profileLinkAttributes?.updatedAt)
    XCTAssertEqual(profileLinkAttributes?.url, URL(string: "http://example.com"))
  }
  
  func testProfileLinkValidNilData() {
    profileLink = parseJson(validNilDataJSON)
    profileLinkAttributes = profileLink?.attributes
    
    XCTAssertNotNil(profileLink)
    
    XCTAssertEqual(profileLink?.objectID, "4")
    XCTAssertEqual(profileLink?.type, "profileLinks")
    
    XCTAssertNotNil(profileLinkAttributes)
    
    XCTAssertNil(profileLinkAttributes?.createdAt)
    XCTAssertNil(profileLinkAttributes?.updatedAt)
    XCTAssertEqual(profileLinkAttributes?.url, URL(string: "http://example.com"))
  }
  
  func testProfileLinkInvalidMissingData() {
    profileLink = parseJson(invalidMissingDataJSON)
    profileLinkAttributes = profileLink?.attributes
    
    XCTAssertNotNil(profileLink)
    
    XCTAssertEqual(profileLink?.objectID, "4")
    XCTAssertEqual(profileLink?.type, "profileLinks")
    
    XCTAssertNil(profileLinkAttributes)
  }
  
  func testProfileLinkInvalidNilData() {
    profileLink = parseJson(invalidNilDataJSON)
    profileLinkAttributes = profileLink?.attributes
    
    XCTAssertNotNil(profileLink)
    
    XCTAssertEqual(profileLink?.objectID, "4")
    XCTAssertEqual(profileLink?.type, "profileLinks")
    
    XCTAssertNil(profileLinkAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> ProfileLink? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(ProfileLink.self, from: data!)
  }
}
