import XCTest
@testable import Users

class UserRoleTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "userRoles",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "userRoles",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [:]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "userRoles",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [
      "createdAt" : nil,
      "updatedAt" : nil
    ]
  ]
  
//  let invalidMissingDataJSON: [String : Any] = [
//    "id" : "4",
//    "type" : "userRoles",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/user-roles/4"
//    ],
//    "attributes" : [:]
//  ]
//
//  let invalidNilDataJSON: [String : Any?] = [
//    "id" : "4",
//    "type" : "userRoles",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/user-roles/4"
//    ],
//    "attributes" : [
//      "createdAt" : nil,
//      "updatedAt" : nil
//    ]
//  ]
  
  var userRole: UserRole?
  var userRoleAttributes: UserRoleAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    userRole = nil
    userRoleAttributes = nil
    
    super.tearDown()
  }
  
  func testUserRoleFullyFilled() {
    userRole = parseJson(fullyFilledJSON)
    userRoleAttributes = userRole?.attributes
    
    XCTAssertNotNil(userRole)
    
    XCTAssertEqual(userRole?.objectID, "4")
    XCTAssertEqual(userRole?.type, "userRoles")
    
    XCTAssertNotNil(userRoleAttributes)

    XCTAssertEqual(
      userRoleAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      userRoleAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
  }
  
  func testUserRoleValidMissingData() {
    userRole = parseJson(validMissingDataJSON)
    userRoleAttributes = userRole?.attributes
    
    XCTAssertNotNil(userRole)
    
    XCTAssertEqual(userRole?.objectID, "4")
    XCTAssertEqual(userRole?.type, "userRoles")
    
    XCTAssertNotNil(userRoleAttributes)
    
    XCTAssertNil(userRoleAttributes?.createdAt)
    XCTAssertNil(userRoleAttributes?.updatedAt)
  }
  
  func testUserRoleValidNilData() {
    userRole = parseJson(validNilDataJSON)
    userRoleAttributes = userRole?.attributes
    
    XCTAssertNotNil(userRole)
    
    XCTAssertEqual(userRole?.objectID, "4")
    XCTAssertEqual(userRole?.type, "userRoles")
    
    XCTAssertNotNil(userRoleAttributes)
    
    XCTAssertNil(userRoleAttributes?.createdAt)
    XCTAssertNil(userRoleAttributes?.updatedAt)
  }
  
//  func testUserRoleInvalidMissingData() {
//    let json = invalidMissingDataJSON
//    
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      userRole = try? decoder.decode(UserRole.self, from: data!)
//    } else {
//      userRole = nil
//    }
//    userRoleAttributes = userRole?.attributes
//    
//    XCTAssertNotNil(userRole)
//    
//    XCTAssertEqual(userRole?.objectID, "4")
//    XCTAssertEqual(userRole?.type, "userRoles")
//    
//    XCTAssertNil(userRoleAttributes)
//  }
//  
//  func testUserRoleInvalidNilData() {
//    let json = invalidNilDataJSON
//    
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      userRole = try? decoder.decode(UserRole.self, from: data!)
//    } else {
//      userRole = nil
//    }
//    userRoleAttributes = userRole?.attributes
//    
//    XCTAssertNotNil(userRole)
//    
//    XCTAssertEqual(userRole?.objectID, "4")
//    XCTAssertEqual(userRole?.type, "userRoles")
//    
//    XCTAssertNil(userRoleAttributes)
//  }

  func parseJson(_ json: [String : Any?]) -> UserRole? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(UserRole.self, from: data!)
  }
}
