import XCTest
@testable import Users

class LinkedAccountTokenApplicationTests: XCTestCase {
  let decoder = JSONDecoder()
  
  let fullyFilledJSON: [String : Any] = [
    "uid" : "4"
  ]
  
  let validMissingDataJSON: [String : Any] = [:]
  
  let validNilDataJSON: [String : Any?] = [
    "uid" : nil
  ]
  
//  let invalidMissingDataJSON: [String : Any] = [
//    "uid" : "4"
//  ]
//
//  let invalidNilDataJSON: [String : Any?] = [
//    "uid" : "4"
//  ]
  
  var linkedAccountTokenApplication: LinkedAccountTokenApplication?
  
  override func tearDown() {
    linkedAccountTokenApplication = nil
    
    super.tearDown()
  }
  
  func testLinkedAccountTokenApplicationFullyFilled() {
    linkedAccountTokenApplication = parseJson(fullyFilledJSON)
    
    XCTAssertNotNil(linkedAccountTokenApplication)
    
    XCTAssertEqual(linkedAccountTokenApplication?.uid, "4")
  }
  
  func testLinkedAccountTokenApplicationValidMissingData() {
    linkedAccountTokenApplication = parseJson(validMissingDataJSON)

    XCTAssertNotNil(linkedAccountTokenApplication)
    
    XCTAssertNil(linkedAccountTokenApplication?.uid)
  }
  
  func testLinkedAccountTokenApplicationValidNilData() {
    linkedAccountTokenApplication = parseJson(validNilDataJSON)

    XCTAssertNotNil(linkedAccountTokenApplication)
    
    XCTAssertNil(linkedAccountTokenApplication?.uid)
  }
  
//  func testLinkedAccountTokenApplicationInvalidMissingData() {
//    let json = invalidMissingDataJSON
//    
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      linkedAccountTokenApplication = try? decoder.decode(LinkedAccountTokenApplication.self,
//                                                          from: data!)
//    } else {
//      linkedAccountTokenApplication = nil
//    }
//    
//    XCTAssertNotNil(linkedAccountTokenApplication)
//    
//    XCTAssertEqual(linkedAccountTokenApplication?.uid, "4")
//  }
//  
//  func testLinkedAccountTokenApplicationInvalidNilData() {
//    let json = invalidNilDataJSON
//    
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      linkedAccountTokenApplication = try? decoder.decode(LinkedAccountTokenApplication.self,
//                                                          from: data!)
//    } else {
//      linkedAccountTokenApplication = nil
//    }
//    
//    XCTAssertNotNil(linkedAccountTokenApplication)
//    
//    XCTAssertEqual(linkedAccountTokenApplication?.uid, "4")
//  }

  func parseJson(_ json: [String : Any?]) -> LinkedAccountTokenApplication? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? decoder.decode(LinkedAccountTokenApplication.self, from: data!)
  }
}

