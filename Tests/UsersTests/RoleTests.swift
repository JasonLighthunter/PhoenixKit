import XCTest
@testable import Users

class RoleTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "roles",
    "links" : [
      "self" : "https://kitsu.io/api/edge/roles/4"
    ],
    "attributes": [
      "createdAt": "2016-12-12T14:32:06.707Z",
      "updatedAt": "2016-12-12T14:32:06.707Z",
      "name": "admin",
      "resourceId": "3",
      "resourceType": "person"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "roles",
    "links" : [
      "self" : "https://kitsu.io/api/edge/roles/4"
    ],
    "attributes" : [:]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "roles",
    "links" : [
      "self" : "https://kitsu.io/api/edge/roles/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "name": nil,
      "resourceId": nil,
      "resourceType": nil
    ]
  ]
  
//  let invalidMissingDataJSON: [String : Any] = [
//    "id" : "4",
//    "type" : "roles",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/roles/4"
//    ],
//    "attributes": [
//      "createdAt": "2016-12-12T14:32:06.707Z",
//      "updatedAt": "2016-12-12T14:32:06.707Z",
//      "name": "admin",
//      "resourceId": "3",
//      "resourceType": "person"
//    ]
//  ]
//
//  let invalidNilDataJSON: [String : Any?] = [
//    "id" : "4",
//    "type" : "roles",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/roles/4"
//    ],
//    "attributes": [
//      "createdAt": "2016-12-12T14:32:06.707Z",
//      "updatedAt": "2016-12-12T14:32:06.707Z",
//      "name": "admin",
//      "resourceId": "3",
//      "resourceType": "person"
//    ]
//  ]
  
  var role: Role?
  var roleAttributes: RoleAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    role = nil
    roleAttributes = nil
    
    super.tearDown()
  }
  
  func testRoleFullyFilled() {
    role = parseJson(fullyFilledJSON)
    roleAttributes = role?.attributes
    
    XCTAssertNotNil(role)
    
    XCTAssertEqual(role?.objectID, "4")
    XCTAssertEqual(role?.type, "roles")
    
    XCTAssertNotNil(roleAttributes)

    XCTAssertEqual(
      roleAttributes?.createdAt,
      dateFormatter.date(from: "2016-12-12T14:32:06.707Z")
    )
    XCTAssertEqual(
      roleAttributes?.updatedAt,
      dateFormatter.date(from: "2016-12-12T14:32:06.707Z")
    )
    XCTAssertEqual(roleAttributes?.name, "admin")
    XCTAssertEqual(roleAttributes?.resourceID, "3")
    XCTAssertEqual(roleAttributes?.resourceType, "person")
  }
  
  func testRoleValidMissingData() {
    role = parseJson(validMissingDataJSON)
    roleAttributes = role?.attributes
    
    XCTAssertNotNil(role)
    
    XCTAssertEqual(role?.objectID, "4")
    XCTAssertEqual(role?.type, "roles")
    
    XCTAssertNotNil(roleAttributes)
    
    XCTAssertNil(roleAttributes?.createdAt)
    XCTAssertNil(roleAttributes?.updatedAt)
    XCTAssertNil(roleAttributes?.name)
    XCTAssertNil(roleAttributes?.resourceID)
    XCTAssertNil(roleAttributes?.resourceType)
  }
  
  func testRoleValidNilData() {
    role = parseJson(validNilDataJSON)
    roleAttributes = role?.attributes
    
    XCTAssertNotNil(role)
    
    XCTAssertEqual(role?.objectID, "4")
    XCTAssertEqual(role?.type, "roles")
    
    XCTAssertNotNil(roleAttributes)
    
    XCTAssertNil(roleAttributes?.createdAt)
    XCTAssertNil(roleAttributes?.updatedAt)
    XCTAssertNil(roleAttributes?.name)
    XCTAssertNil(roleAttributes?.resourceID)
    XCTAssertNil(roleAttributes?.resourceType)
  }
  
//  func testRoleInvalidMissingData() {
//    let json = invalidMissingDataJSON
//    
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      role = try? decoder.decode(Role.self, from: data!)
//    } else {
//      role = nil
//    }
//    roleAttributes = role?.attributes
//    
//    XCTAssertNotNil(role)
//    
//    XCTAssertEqual(role?.objectID, "4")
//    XCTAssertEqual(role?.type, "roles")
//    
//    XCTAssertNil(roleAttributes)
//  }
//  
//  func testRoleInvalidNilData() {
//    let json = invalidNilDataJSON
//    
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      role = try? decoder.decode(Role.self, from: data!)
//    } else {
//      role = nil
//    }
//    roleAttributes = role?.attributes
//    
//    XCTAssertNotNil(role)
//    
//    XCTAssertEqual(role?.objectID, "4")
//    XCTAssertEqual(role?.type, "roles")
//    
//    XCTAssertNil(roleAttributes)
//  }

  func parseJson(_ json: [String : Any?]) -> Role? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Role.self, from: data!)
  }
}

