import XCTest
@testable import Users

class StatDataActivityTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any?] = [
    "id": 2081224,
    "event": "updated",
    "user_id": 634,
    "anime_id": nil,
    "drama_id": nil,
    "manga_id": 26193,
    "created_at": "2017-08-16T13:00:26.784Z",
    "updated_at": "2017-08-16T13:00:26.784Z",
    "changed_data": [:],
    "library_entry_id": 15377956
  ]
  
  let validMissingDataJSON: [String : Any] = [:]
  
  let validNilDataJSON: [String : Any?] = [
    "id": nil,
    "event": nil,
    "user_id": nil,
    "anime_id": nil,
    "drama_id": nil,
    "manga_id": nil,
    "created_at": nil,
    "updated_at": nil,
    "changed_data": nil,
    "library_entry_id": nil
  ]
  
//  let invalidMissingDataJSON: [String : Any] = [:]
  
//  let invalidNilDataJSON: [String : Any?] = [
//    "id": nil,
//    "event": nil,
//    "user_id": nil,
//    "anime_id": nil,
//    "drama_id": nil,
//    "manga_id": nil,
//    "created_at": nil,
//    "updated_at": nil,
//    "changed_data": nil,
//    "library_entry_id": nil
//  ]
  
  var statDataActivity: StatDataActivity?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    statDataActivity = nil
    
    super.tearDown()
  }
  
  func testStatDataActivityFullyFilled() {
    statDataActivity = parseJson(fullyFilledJSON)
    
    XCTAssertNotNil(statDataActivity)
    
    XCTAssertEqual(statDataActivity?.objectID, 2081224)
    XCTAssertEqual(statDataActivity?.event, "updated")
    XCTAssertEqual(statDataActivity?.userID, 634)
    XCTAssertNil(statDataActivity?.animeID)
    XCTAssertNil(statDataActivity?.dramaID)
    XCTAssertEqual(statDataActivity?.mangaID, 26193)
    XCTAssertEqual(
      statDataActivity?.createdAt, dateFormatter.date(from: "2017-08-16T13:00:26.784Z")
    )
    XCTAssertEqual(
      statDataActivity?.updatedAt, dateFormatter.date(from: "2017-08-16T13:00:26.784Z")
    )
    XCTAssertNotNil(statDataActivity?.changedData)
    
    XCTAssertEqual(statDataActivity?.libraryEntryID, 15377956)
  }
  
  func testStatDataActivityValidMissingData() {
    statDataActivity = parseJson(validMissingDataJSON)
    
    XCTAssertNotNil(statDataActivity)
    
    XCTAssertNil(statDataActivity?.objectID)
    XCTAssertNil(statDataActivity?.event)
    XCTAssertNil(statDataActivity?.userID)
    XCTAssertNil(statDataActivity?.animeID)
    XCTAssertNil(statDataActivity?.dramaID)
    XCTAssertNil(statDataActivity?.mangaID)
    XCTAssertNil(statDataActivity?.createdAt)
    XCTAssertNil(statDataActivity?.updatedAt)
    XCTAssertNil(statDataActivity?.changedData)
    XCTAssertNil(statDataActivity?.libraryEntryID)
  }
  
  func testStatDataActivityValidNilData() {
    statDataActivity = parseJson(validNilDataJSON)
    
    XCTAssertNotNil(statDataActivity)
    
    XCTAssertNil(statDataActivity?.objectID)
    XCTAssertNil(statDataActivity?.event)
    XCTAssertNil(statDataActivity?.userID)
    XCTAssertNil(statDataActivity?.animeID)
    XCTAssertNil(statDataActivity?.dramaID)
    XCTAssertNil(statDataActivity?.mangaID)
    XCTAssertNil(statDataActivity?.createdAt)
    XCTAssertNil(statDataActivity?.updatedAt)
    XCTAssertNil(statDataActivity?.changedData)
    XCTAssertNil(statDataActivity?.libraryEntryID)
  }
  
//  func testStatDataActivityInvalidMissingData() {
//    let json = invalidMissingDataJSON
//
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      statDataActivity = try? decoder.decode(StatDataActivity.self, from: data!)
//    } else {
//      statDataActivity = nil
//    }
//
//    XCTAssertNil(statDataActivity)
//  }
  
//  func testStatDataActivityInvalidNilData() {
//    let json = invalidNilDataJSON
//
//    if JSONSerialization.isValidJSONObject(json as Any) {
//      let data = try? JSONSerialization.data(withJSONObject: json as Any)
//      statDataActivity = try? decoder.decode(StatDataActivity.self, from: data!)
//    } else {
//      statDataActivity = nil
//    }
//
//    XCTAssertNil(statDataActivity)
//  }

  func parseJson(_ json: [String : Any?]) -> StatDataActivity? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(StatDataActivity.self, from: data!)
  }
}
