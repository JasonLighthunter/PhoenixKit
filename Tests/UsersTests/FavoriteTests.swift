import XCTest
@testable import Users

class FavoriteTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "favorites",
    "links" : [
      "self" : "https://kitsu.io/api/edge/favorites/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "favRank" : 2
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id" : "4",
//    "type" : "favorites",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/favorites/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z",
//      "favRank" : 2
//    ]
//  ]
  
//  let validNilDataJSON: [String : Any?] = [
//    "id" : "4",
//    "type" : "favorites",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/favorites/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z",
//      "favRank" : 2
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "favorites",
    "links" : [
      "self" : "https://kitsu.io/api/edge/favorites/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "favRank" : 2
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "favorites",
    "links" : [
      "self" : "https://kitsu.io/api/edge/favorites/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : nil,
      "favRank" : 2
    ]
  ]
  
  var favorite: Favorite?
  var favoriteAttributes: FavoriteAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    favorite = nil
    favoriteAttributes = nil
    
    super.tearDown()
  }
  
  func testFavoriteFullyFilled() {
    favorite = parseJson(fullyFilledJSON)
    favoriteAttributes = favorite?.attributes
    
    XCTAssertNotNil(favorite)
    
    XCTAssertEqual(favorite?.objectID, "4")
    XCTAssertEqual(favorite?.type, "favorites")
    
    XCTAssertNotNil(favoriteAttributes)

    XCTAssertEqual(
      favoriteAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      favoriteAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(favoriteAttributes?.favoritesRank, 2)
  }
  
//  func testFavoriteValidMissingData() {
//    favorite = parseJson(validMissingDataJSON)
//    favoriteAttributes = favorite?.attributes
//
//    XCTAssertNotNil(favorite)
//
//    XCTAssertEqual(favorite?.objectID, "4")
//    XCTAssertEqual(favorite?.type, "favorites")
//
//    XCTAssertNotNil(favoriteAttributes)
//
//    XCTAssertEqual(
//      favoriteAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      favoriteAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(favoriteAttributes?.favoritesRank, 2)
//  }

//  func testFavoriteValidNilData() {
//    favorite = parseJson(validNilDataJSON)
//    favoriteAttributes = favorite?.attributes
//
//    XCTAssertNotNil(favorite)
//
//    XCTAssertEqual(favorite?.objectID, "4")
//    XCTAssertEqual(favorite?.type, "favorites")
//
//    XCTAssertNotNil(favoriteAttributes)
//
//    XCTAssertEqual(
//      favoriteAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      favoriteAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(favoriteAttributes?.favoritesRank, 2)
//  }

  func testFavoriteInvalidMissingData() {
    favorite = parseJson(invalidMissingDataJSON)
    favoriteAttributes = favorite?.attributes
    
    XCTAssertNotNil(favorite)
    
    XCTAssertEqual(favorite?.objectID, "4")
    XCTAssertEqual(favorite?.type, "favorites")
    
    XCTAssertNil(favoriteAttributes)
  }
  
  func testFavoriteInvalidNilData() {
    favorite = parseJson(invalidNilDataJSON)
    favoriteAttributes = favorite?.attributes
    
    XCTAssertNotNil(favorite)
    
    XCTAssertEqual(favorite?.objectID, "4")
    XCTAssertEqual(favorite?.type, "favorites")
    
    XCTAssertNil(favoriteAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Favorite? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Favorite.self, from: data!)
  }
}
