import XCTest
@testable import Users

class StatTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "stats",
    "links" : [
      "self" : "https://kitsu.io/api/edge/stats/4"
    ],
    "attributes" : [
      "createdAt": "2017-07-28T02:17:44.910Z",
      "updatedAt": "2017-10-26T22:10:45.456Z",
      "kind": "anime-category-breakdown",
      "statsData": [:]
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "stats",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [
      "createdAt": "2017-07-28T02:17:44.910Z",
      "updatedAt": "2017-10-26T22:10:45.456Z",
      "kind": "anime-category-breakdown",
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "stats",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [
      "createdAt": "2017-07-28T02:17:44.910Z",
      "updatedAt": "2017-10-26T22:10:45.456Z",
      "kind": "anime-category-breakdown",
      "statsData": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "stats",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [
      "createdAt": "2017-07-28T02:17:44.910Z",
      "updatedAt": "2017-10-26T22:10:45.456Z",
      "statsData": [:]
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "stats",
    "links" : [
      "self" : "https://kitsu.io/api/edge/user-roles/4"
    ],
    "attributes" : [
      "createdAt": "2017-07-28T02:17:44.910Z",
      "updatedAt": nil,
      "kind": "anime-category-breakdown",
      "statsData": [:]
    ]
  ]
  
  var stat: Stat?
  var statAttributes: StatAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    stat = nil
    statAttributes = nil
    
    super.tearDown()
  }
  
  func testStatFullyFilled() {
    stat = parseJson(fullyFilledJSON)
    statAttributes = stat?.attributes
    
    XCTAssertNotNil(stat)
    
    XCTAssertEqual(stat?.objectID, "4")
    XCTAssertEqual(stat?.type, "stats")
    
    XCTAssertNotNil(statAttributes)

    let createdAtString = "2017-07-28T02:17:44.910Z"
    XCTAssertEqual(statAttributes?.createdAt, dateFormatter.date(from: createdAtString))
    let updatedAtString = "2017-10-26T22:10:45.456Z"
    XCTAssertEqual(statAttributes?.updatedAt, dateFormatter.date(from: updatedAtString))
    XCTAssertEqual(statAttributes?.kind, .animeCategoryBreakdown)
    
//    XCTAssertNotNil(statAttributes?.statsData)
  }
  
  func testStatValidMissingData() {
    stat = parseJson(validMissingDataJSON)
    statAttributes = stat?.attributes
    
    XCTAssertNotNil(stat)
    
    XCTAssertEqual(stat?.objectID, "4")
    XCTAssertEqual(stat?.type, "stats")
    
    XCTAssertNotNil(statAttributes)

    let createdAtString = "2017-07-28T02:17:44.910Z"
    XCTAssertEqual(statAttributes?.createdAt, dateFormatter.date(from: createdAtString))
    let updatedAtString = "2017-10-26T22:10:45.456Z"
    XCTAssertEqual(statAttributes?.updatedAt, dateFormatter.date(from: updatedAtString))
    XCTAssertEqual(statAttributes?.kind, .animeCategoryBreakdown)
    
    XCTAssertNil(statAttributes?.statsData)
  }
  
  func testStatValidNilData() {
    stat = parseJson(validNilDataJSON)
    statAttributes = stat?.attributes
    
    XCTAssertNotNil(stat)
    
    XCTAssertEqual(stat?.objectID, "4")
    XCTAssertEqual(stat?.type, "stats")
    
    XCTAssertNotNil(statAttributes)
    
    let createdAtString = "2017-07-28T02:17:44.910Z"
    XCTAssertEqual(statAttributes?.createdAt, dateFormatter.date(from: createdAtString))
    let updatedAtString = "2017-10-26T22:10:45.456Z"
    XCTAssertEqual(statAttributes?.updatedAt, dateFormatter.date(from: updatedAtString))
    XCTAssertEqual(statAttributes?.kind, .animeCategoryBreakdown)
    
    XCTAssertNil(statAttributes?.statsData)
  }
  
  func testStatInvalidMissingData() {
    stat = parseJson(invalidMissingDataJSON)
    statAttributes = stat?.attributes
    
    XCTAssertNotNil(stat)
    
    XCTAssertEqual(stat?.objectID, "4")
    XCTAssertEqual(stat?.type, "stats")
    
    XCTAssertNil(statAttributes)
  }
  
  func testStatInvalidNilData() {
    stat = parseJson(invalidNilDataJSON)
    statAttributes = stat?.attributes
    
    XCTAssertNotNil(stat)
    
    XCTAssertEqual(stat?.objectID, "4")
    XCTAssertEqual(stat?.type, "stats")
    
    XCTAssertNil(statAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Stat? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Stat.self, from: data!)
  }
}
