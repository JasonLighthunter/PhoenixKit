import XCTest
@testable import Users

class ProfileLinkSiteTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "profileLinkSites",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-link-sites/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "name" : "twitter"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id" : "4",
//    "type" : "profileLinkSites",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/profile-link-sites/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z",
//      "name" : "twitter"
//    ]
//  ]

//  let validNilDataJSON: [String : Any?] = [
//    "id" : "4",
//    "type" : "profileLinkSites",
//    "links" : [
//      "self" : "https://kitsu.io/api/edge/profile-link-sites/4"
//    ],
//    "attributes" : [
//      "createdAt" : "2017-08-08T12:39:19.217Z",
//      "updatedAt" : "2017-08-08T12:39:19.217Z",
//      "name" : "twitter"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "profileLinkSites",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-link-sites/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "name" : "twitter"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "profileLinkSites",
    "links" : [
      "self" : "https://kitsu.io/api/edge/profile-link-sites/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : nil,
      "name" : "twitter"
    ]
  ]
  
  var profileLinkSite: ProfileLinkSite?
  var profileLinkSiteAttributes: ProfileLinkSiteAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    profileLinkSite = nil
    profileLinkSiteAttributes = nil
    
    super.tearDown()
  }
  
  func testProfileLinkSiteFullyFilled() {
    profileLinkSite = parseJson(fullyFilledJSON)
    profileLinkSiteAttributes = profileLinkSite?.attributes
    
    XCTAssertNotNil(profileLinkSite)
    
    XCTAssertEqual(profileLinkSite?.objectID, "4")
    XCTAssertEqual(profileLinkSite?.type, "profileLinkSites")
    
    XCTAssertNotNil(profileLinkSiteAttributes)

    XCTAssertEqual(
      profileLinkSiteAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      profileLinkSiteAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(profileLinkSiteAttributes?.name, "twitter")
  }
  
//  func testProfileLinkSiteValidMissingData() {
//    profileLinkSite = parseJson(validMissingDataJSON)
//    profileLinkSiteAttributes = profileLinkSite?.attributes
//
//    XCTAssertNotNil(profileLinkSite)
//
//    XCTAssertEqual(profileLinkSite?.objectID, "4")
//    XCTAssertEqual(profileLinkSite?.type, "profileLinkSites")
//
//    XCTAssertNotNil(profileLinkSiteAttributes)
//
//    XCTAssertEqual(
//      profileLinkSiteAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      profileLinkSiteAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(profileLinkSiteAttributes?.name, "twitter")
//  }
  
//  func testProfileLinkSiteValidNilData() {
//    profileLinkSite = parseJson(validNilDataJSON)
//    profileLinkSiteAttributes = profileLinkSite?.attributes
//
//    XCTAssertNotNil(profileLinkSite)
//
//    XCTAssertEqual(profileLinkSite?.objectID, "4")
//    XCTAssertEqual(profileLinkSite?.type, "profileLinkSites")
//
//    XCTAssertNotNil(profileLinkSiteAttributes)
//
//    XCTAssertEqual(
//      profileLinkSiteAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      profileLinkSiteAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(profileLinkSiteAttributes?.name, "twitter")
//  }

  func testProfileLinkSiteInvalidMissingData() {
    profileLinkSite = parseJson(invalidMissingDataJSON)
    profileLinkSiteAttributes = profileLinkSite?.attributes
    
    XCTAssertNotNil(profileLinkSite)
    
    XCTAssertEqual(profileLinkSite?.objectID, "4")
    XCTAssertEqual(profileLinkSite?.type, "profileLinkSites")
    
    XCTAssertNil(profileLinkSiteAttributes)
  }
  
  func testProfileLinkSiteInvalidNilData() {
    profileLinkSite = parseJson(invalidNilDataJSON)
    profileLinkSiteAttributes = profileLinkSite?.attributes
    
    XCTAssertNotNil(profileLinkSite)
    
    XCTAssertEqual(profileLinkSite?.objectID, "4")
    XCTAssertEqual(profileLinkSite?.type, "profileLinkSites")
    
    XCTAssertNil(profileLinkSiteAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> ProfileLinkSite? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(ProfileLinkSite.self, from: data!)
  }
}
