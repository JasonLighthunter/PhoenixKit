import XCTest
@testable import Users

class StatDataActivityChangedDataTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let updatedWithStartedAtJSON: [String : Any] = [
    "progress": [
      18,
      19
    ],
    "started_at": [
      nil,
      "2017-08-16T13:00:26.527Z"
    ],
    "updated_at": [
      "2017-02-04T22:09:12.867Z",
      "2017-08-16T13:00:26.528Z"
    ],
    "progressed_at": [
      "2017-02-04T22:09:12.867Z",
      "2017-08-16T13:00:26.497Z"
    ]
  ]
  
  let updatedWithoutStartedAtJSON: [String : Any] = [
    "progress": [
      18,
      19
    ],
    "updated_at": [
      "2017-02-04T22:09:12.867Z",
      "2017-08-16T13:00:26.528Z"
    ],
    "progressed_at": [
      "2017-02-04T22:09:12.867Z",
      "2017-08-16T13:00:26.497Z"
    ]
  ]
  
  let addedJSON: [String : Any] = [
    "id": [
      nil,
      17499243
    ],
    "status": [
      nil,
      "planned"
    ],
    "user_id": [
      nil,
      634
    ],
    "anime_id": [
      nil,
      11913
    ],
    "media_id": [
      nil,
      11913
    ],
    "created_at": [
      nil,
      "2017-06-12T21:16:21.149Z"
    ],
    "media_type": [
      nil,
      "Anime"
    ],
    "updated_at": [
      nil,
      "2017-06-12T21:16:21.149Z"
    ]
  ]
  
  let finishedJSON: [String : Any] = [
    "status": [
      "current",
      "completed"
    ],
    "progress": [
      23,
      24
    ],
    "updated_at": [
      "2017-06-06T20:04:25.401Z",
      "2017-06-15T19:34:28.193Z"
    ],
    "finished_at": [
      nil,
      "2017-06-15T19:34:28.192Z"
    ],
    "progressed_at": [
      "2017-06-06T20:04:25.401Z",
      "2017-06-15T19:34:28.193Z"
    ]
  ]
  
  let missingJSON: [String : Any] = [:]
  
  let emptyJSON: [String : Any] = [
    "id": [],
    "status": [],
    "user_id": [],
    "anime_id": [],
    "manga_id": [],
    "media_id": [],
    "created_at": [],
    "media_type": [],
    "started_at": [],
    "progress": [],
    "updated_at": [],
    "progressed_at": [],
    "finished_at": []
  ]
  
  let nilJSON: [String : Any?] = [
    "id": nil,
    "status": nil,
    "user_id": nil,
    "anime_id": nil,
    "manga_id": nil,
    "media_id": nil,
    "created_at": nil,
    "media_type": nil,
    "started_at": nil,
    "progress": nil,
    "updated_at": nil,
    "progressed_at": nil,
    "finished_at": nil
  ]
  
  var statDataActivityChangedData: StatDataActivityChangedData?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    statDataActivityChangedData = nil
    
    super.tearDown()
  }
  
  func testStatDataActivityChangedDataUpdatedWithStartedAt() {
    statDataActivityChangedData = parseJson(updatedWithStartedAtJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    
    XCTAssertNil(statDataActivityChangedData?.objectID)
    XCTAssertNil(statDataActivityChangedData?.status)
    XCTAssertNil(statDataActivityChangedData?.userID)
    XCTAssertNil(statDataActivityChangedData?.animeID)
    XCTAssertNil(statDataActivityChangedData?.mangaID)
    XCTAssertNil(statDataActivityChangedData?.mediaID)
    XCTAssertNil(statDataActivityChangedData?.createdAt)
    XCTAssertNil(statDataActivityChangedData?.mediaType)
    XCTAssertNil(statDataActivityChangedData?.finishedAt)
    XCTAssertEqual(statDataActivityChangedData?.progress?[0], 18)
    XCTAssertEqual(statDataActivityChangedData?.progress![1], 19)
    XCTAssertNil(statDataActivityChangedData?.startedAt?[0])
    XCTAssertEqual(
      statDataActivityChangedData?.startedAt![1],
      dateFormatter.date(from: "2017-08-16T13:00:26.527Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt!.first,
      dateFormatter.date(from: "2017-02-04T22:09:12.867Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt![1],
      dateFormatter.date(from: "2017-08-16T13:00:26.528Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.progressedAt!.first,
      dateFormatter.date(from: "2017-02-04T22:09:12.867Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.progressedAt![1],
      dateFormatter.date(from: "2017-08-16T13:00:26.497Z")
    )
  }
  
  func testStatDataActivityChangedDataUpdatedWithoutStartedAt() {
    statDataActivityChangedData = parseJson(updatedWithoutStartedAtJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    
    XCTAssertNil(statDataActivityChangedData?.objectID)
    XCTAssertNil(statDataActivityChangedData?.status)
    XCTAssertNil(statDataActivityChangedData?.userID)
    XCTAssertNil(statDataActivityChangedData?.animeID)
    XCTAssertNil(statDataActivityChangedData?.mangaID)
    XCTAssertNil(statDataActivityChangedData?.mediaID)
    XCTAssertNil(statDataActivityChangedData?.createdAt)
    XCTAssertNil(statDataActivityChangedData?.mediaType)
    XCTAssertNil(statDataActivityChangedData?.finishedAt)
    XCTAssertEqual(statDataActivityChangedData?.progress?[0], 18)
    XCTAssertEqual(statDataActivityChangedData?.progress?[1], 19)
    XCTAssertNil(statDataActivityChangedData?.startedAt)
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt![0],
      dateFormatter.date(from: "2017-02-04T22:09:12.867Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt![1],
      dateFormatter.date(from: "2017-08-16T13:00:26.528Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.progressedAt![0],
      dateFormatter.date(from: "2017-02-04T22:09:12.867Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.progressedAt![1],
      dateFormatter.date(from: "2017-08-16T13:00:26.497Z")
    )
  }
  
  func testStatDataActivityChangedDataAdded() {
    statDataActivityChangedData = parseJson(addedJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    
    XCTAssertNil(statDataActivityChangedData?.objectID?[0])
    XCTAssertEqual(statDataActivityChangedData?.objectID?[1], 17499243)
    XCTAssertNil(statDataActivityChangedData?.status?[0])
    XCTAssertEqual(statDataActivityChangedData?.status?[1], "planned")
    XCTAssertNil(statDataActivityChangedData?.userID?[0])
    XCTAssertEqual(statDataActivityChangedData?.userID![1], 634)
    XCTAssertNil(statDataActivityChangedData?.animeID?[0])
    XCTAssertEqual(statDataActivityChangedData?.animeID![1], 11913)
    XCTAssertNil(statDataActivityChangedData?.mangaID)
    XCTAssertNil(statDataActivityChangedData?.mediaID?[0])
    XCTAssertEqual(statDataActivityChangedData?.mediaID![1], 11913)
    XCTAssertNil(statDataActivityChangedData?.createdAt?[0])
    XCTAssertEqual(
      statDataActivityChangedData?.createdAt![1],
      dateFormatter.date(from: "2017-06-12T21:16:21.149Z")
    )
    XCTAssertNil(statDataActivityChangedData?.mediaType?[0])
    XCTAssertEqual(statDataActivityChangedData?.mediaType![1], "Anime")
    XCTAssertNil(statDataActivityChangedData?.startedAt)
    XCTAssertNil(statDataActivityChangedData?.progress)
    XCTAssertNil(statDataActivityChangedData?.updatedAt?[0])
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt![1],
      dateFormatter.date(from: "2017-06-12T21:16:21.149Z")
    )
    XCTAssertNil(statDataActivityChangedData?.progressedAt)
    XCTAssertNil(statDataActivityChangedData?.finishedAt)
  }
  
  func testStatDataActivityChangedDataFinished() {
    statDataActivityChangedData = parseJson(finishedJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    
    XCTAssertNil(statDataActivityChangedData?.objectID)
    XCTAssertEqual(statDataActivityChangedData?.status?[0], "current")
    XCTAssertEqual(statDataActivityChangedData?.status?[1], "completed")
    XCTAssertNil(statDataActivityChangedData?.userID)
    XCTAssertNil(statDataActivityChangedData?.animeID)
    XCTAssertNil(statDataActivityChangedData?.mangaID)
    XCTAssertNil(statDataActivityChangedData?.mediaID)
    XCTAssertNil(statDataActivityChangedData?.createdAt)
    XCTAssertNil(statDataActivityChangedData?.mediaType)
    XCTAssertNil(statDataActivityChangedData?.startedAt)
    XCTAssertEqual(statDataActivityChangedData?.progress?[0], 23)
    XCTAssertEqual(statDataActivityChangedData?.progress?[1], 24)
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt?.first,
      dateFormatter.date(from: "2017-06-06T20:04:25.401Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.updatedAt![1],
      dateFormatter.date(from: "2017-06-15T19:34:28.193Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.progressedAt![0],
      dateFormatter.date(from: "2017-06-06T20:04:25.401Z")
    )
    XCTAssertEqual(
      statDataActivityChangedData?.progressedAt![1],
      dateFormatter.date(from: "2017-06-15T19:34:28.193Z")
    )
    XCTAssertNil(statDataActivityChangedData?.finishedAt?[0])
    XCTAssertEqual(
      statDataActivityChangedData?.finishedAt![1],
      dateFormatter.date(from: "2017-06-15T19:34:28.192Z")
    )
  }
  
  func testStatDataActivityChangedDataMissing() {
    statDataActivityChangedData = parseJson(missingJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    XCTAssertNil(statDataActivityChangedData?.objectID)
    XCTAssertNil(statDataActivityChangedData?.status)
    XCTAssertNil(statDataActivityChangedData?.userID)
    XCTAssertNil(statDataActivityChangedData?.animeID)
    XCTAssertNil(statDataActivityChangedData?.mangaID)
    XCTAssertNil(statDataActivityChangedData?.mediaID)
    XCTAssertNil(statDataActivityChangedData?.createdAt)
    XCTAssertNil(statDataActivityChangedData?.mediaType)
    XCTAssertNil(statDataActivityChangedData?.startedAt)
    XCTAssertNil(statDataActivityChangedData?.progress)
    XCTAssertNil(statDataActivityChangedData?.updatedAt)
    XCTAssertNil(statDataActivityChangedData?.progressedAt)
    XCTAssertNil(statDataActivityChangedData?.finishedAt)
  }
  
  func testStatDataActivityChangedDataEmpty() {
    statDataActivityChangedData = parseJson(emptyJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    
    XCTAssertTrue(statDataActivityChangedData?.objectID?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.status?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.userID?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.animeID?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.mangaID?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.mediaID?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.createdAt?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.mediaType?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.startedAt?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.progress?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.updatedAt?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.progressedAt?.isEmpty ?? false)
    XCTAssertTrue(statDataActivityChangedData?.finishedAt?.isEmpty ?? false)
  }
  
  func testStatDataActivityChangedDataNil() {
    statDataActivityChangedData = parseJson(nilJSON)
    
    XCTAssertNotNil(statDataActivityChangedData)
    
    XCTAssertNil(statDataActivityChangedData?.objectID)
    XCTAssertNil(statDataActivityChangedData?.status)
    XCTAssertNil(statDataActivityChangedData?.userID)
    XCTAssertNil(statDataActivityChangedData?.animeID)
    XCTAssertNil(statDataActivityChangedData?.mangaID)
    XCTAssertNil(statDataActivityChangedData?.mediaID)
    XCTAssertNil(statDataActivityChangedData?.createdAt)
    XCTAssertNil(statDataActivityChangedData?.mediaType)
    XCTAssertNil(statDataActivityChangedData?.startedAt)
    XCTAssertNil(statDataActivityChangedData?.progress)
    XCTAssertNil(statDataActivityChangedData?.updatedAt)
    XCTAssertNil(statDataActivityChangedData?.progressedAt)
    XCTAssertNil(statDataActivityChangedData?.finishedAt)
  }

  func parseJson(_ json: [String : Any?]) -> StatDataActivityChangedData? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(StatDataActivityChangedData.self, from: data!)
  }
}
