import XCTest
@testable import Users

class RegistrationStatusEnumTests: XCTestCase {
  func testRegistrationStatusEnum() {
    XCTAssertEqual(RegistrationStatus(rawValue: "registered"), .registered)
    XCTAssertEqual(RegistrationStatus(rawValue: "unregistered"), .unregistered)
    XCTAssertNil(RegistrationStatus(rawValue: "InvalidInput"))
  }
}
