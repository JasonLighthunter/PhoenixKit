import XCTest
@testable import SiteAnnouncements

class SiteAnnouncementTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id" : "4",
    "type" : "siteAnnouncements",
    "links" : [
      "self" : "https://kitsu.io/api/edge/site-announcements/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "title": "It's update time!",
      "description": "A new release is upon us.",
      "imageUrl": "https://media.giphy.com/media/3og0IEXRvwMN0cLSaQ/giphy.gif",
      "link": "https://medium.com/heykitsu/kitsu-release-notes-may-11th-2017-803bacc10e34"
    ]
  ]

  let validMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "siteAnnouncements",
    "links" : [
      "self" : "https://kitsu.io/api/edge/site-announcements/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "title": "It's update time!"
    ]
  ]
  
  let validNilDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "siteAnnouncements",
    "links" : [
      "self" : "https://kitsu.io/api/edge/site-announcements/4"
    ],
    "attributes" : [
      "createdAt" : "2017-08-08T12:39:19.217Z",
      "updatedAt" : "2017-08-08T12:39:19.217Z",
      "title": "It's update time!",
      "description": nil,
      "imageUrl": nil,
      "link": nil
    ]
  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id" : "4",
    "type" : "siteAnnouncements",
    "links" : [
      "self" : "https://kitsu.io/api/edge/site-announcements/4"
    ],
    "attributes" : [
      "title": "It's update time!",
      "description": "A new release is upon us.",
      "imageUrl": "https://media.giphy.com/media/3og0IEXRvwMN0cLSaQ/giphy.gif",
      "link": "https://medium.com/heykitsu/kitsu-release-notes-may-11th-2017-803bacc10e34"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id" : "4",
    "type" : "siteAnnouncements",
    "links" : [
      "self" : "https://kitsu.io/api/edge/site-announcements/4"
    ],
    "attributes" : [
      "createdAt" : nil,
      "title": "It's update time!",
      "description": "A new release is upon us.",
      "imageUrl": "https://media.giphy.com/media/3og0IEXRvwMN0cLSaQ/giphy.gif",
      "link": "https://medium.com/heykitsu/kitsu-release-notes-may-11th-2017-803bacc10e34"
    ]
  ]

  var siteAnnouncement: SiteAnnouncement?
  var siteAnnouncementAttributes: SiteAnnouncementAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    siteAnnouncement = nil
    siteAnnouncementAttributes = nil

    super.tearDown()
  }

  func testSiteAnnouncementFullyFilled() {
    siteAnnouncement = parseJson(fullyFilledJSON)
    siteAnnouncementAttributes = siteAnnouncement?.attributes

    XCTAssertNotNil(siteAnnouncement)

    XCTAssertEqual(siteAnnouncement?.objectID, "4")
    XCTAssertEqual(siteAnnouncement?.type, "siteAnnouncements")

    XCTAssertNotNil(siteAnnouncementAttributes)

    XCTAssertEqual(
      siteAnnouncementAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      siteAnnouncementAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(siteAnnouncementAttributes?.title, "It's update time!")
    XCTAssertEqual(siteAnnouncementAttributes?.description, "A new release is upon us.")
    XCTAssertEqual(
      siteAnnouncementAttributes?.imageURL,
      URL(string: "https://media.giphy.com/media/3og0IEXRvwMN0cLSaQ/giphy.gif")
    )
    XCTAssertEqual(
      siteAnnouncementAttributes?.link,
      URL(string: "https://medium.com/heykitsu/kitsu-release-notes-may-11th-2017-803bacc10e34")
    )
  }

  func testSiteAnnouncementValidMissingData() {
    siteAnnouncement = parseJson(validMissingDataJSON)
    siteAnnouncementAttributes = siteAnnouncement?.attributes
    
    XCTAssertNotNil(siteAnnouncement)
    
    XCTAssertEqual(siteAnnouncement?.objectID, "4")
    XCTAssertEqual(siteAnnouncement?.type, "siteAnnouncements")
    
    XCTAssertNotNil(siteAnnouncementAttributes)

    XCTAssertEqual(
      siteAnnouncementAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      siteAnnouncementAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(siteAnnouncementAttributes?.title, "It's update time!")
    XCTAssertNil(siteAnnouncementAttributes?.description)
    XCTAssertNil(siteAnnouncementAttributes?.imageURL)
    XCTAssertNil(siteAnnouncementAttributes?.link)
  }

  func testSiteAnnouncementValidNilData() {
    siteAnnouncement = parseJson(validNilDataJSON)
    siteAnnouncementAttributes = siteAnnouncement?.attributes
    
    XCTAssertNotNil(siteAnnouncement)
    
    XCTAssertEqual(siteAnnouncement?.objectID, "4")
    XCTAssertEqual(siteAnnouncement?.type, "siteAnnouncements")
    
    XCTAssertNotNil(siteAnnouncementAttributes)

    XCTAssertEqual(
      siteAnnouncementAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      siteAnnouncementAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(siteAnnouncementAttributes?.title, "It's update time!")
    XCTAssertNil(siteAnnouncementAttributes?.description)
    XCTAssertNil(siteAnnouncementAttributes?.imageURL)
    XCTAssertNil(siteAnnouncementAttributes?.link)
  }

  func testSiteAnnouncementInvalidMissingData() {
    siteAnnouncement = parseJson(invalidMissingDataJSON)
    siteAnnouncementAttributes = siteAnnouncement?.attributes
    
    XCTAssertNotNil(siteAnnouncement)
    
    XCTAssertEqual(siteAnnouncement?.objectID, "4")
    XCTAssertEqual(siteAnnouncement?.type, "siteAnnouncements")
    
    XCTAssertNil(siteAnnouncementAttributes)
  }
  
  func testSiteAnnouncementInvalidNilData() {
    siteAnnouncement = parseJson(invalidNilDataJSON)
    siteAnnouncementAttributes = siteAnnouncement?.attributes
    
    XCTAssertNotNil(siteAnnouncement)
    
    XCTAssertEqual(siteAnnouncement?.objectID, "4")
    XCTAssertEqual(siteAnnouncement?.type, "siteAnnouncements")
    
    XCTAssertNil(siteAnnouncementAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> SiteAnnouncement? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(SiteAnnouncement.self, from: data!)
  }
}
