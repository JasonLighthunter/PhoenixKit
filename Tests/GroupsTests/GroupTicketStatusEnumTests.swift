import XCTest
@testable import Groups

class TicketStatusTests: XCTestCase {
  func testGroupTicketStatus() {
    XCTAssertEqual(TicketStatus(rawValue: "created"), .created)
    XCTAssertEqual(TicketStatus(rawValue: "assigned"), .assigned)
    XCTAssertEqual(TicketStatus(rawValue: "resolved"), .resolved)
    XCTAssertNil(TicketStatus(rawValue: "InvalidInput"))
  }
}
