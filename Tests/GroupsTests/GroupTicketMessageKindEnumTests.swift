import XCTest
@testable import Groups

class TicketMessageKindTests: XCTestCase {
  func testTicketMessageKind() {
    XCTAssertEqual(TicketMessageKind(rawValue: "message"), .message)
    XCTAssertEqual(TicketMessageKind(rawValue: "mod_note"), .modNote)
    XCTAssertNil(TicketMessageKind(rawValue: "InvalidInput"))
  }
}
