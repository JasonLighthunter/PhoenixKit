import XCTest
@testable import Groups

class ActionLogTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupActionLogs",
    "links": [
      "self": "https://kitsu.io/api/edge/group-action-logs/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "verb": "about_changed"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupActionLogs",
    "links": [
      "self": "https://kitsu.io/api/edge/group-action-logs/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "verb": "about_changed"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupActionLogs",
    "links": [
      "self": "https://kitsu.io/api/edge/group-action-logs/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": nil,
      "verb" : "about_changed"
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupActionLogs",
    "links": [
      "self": "https://kitsu.io/api/edge/group-action-logs/1"
    ],
    "attributes": [
      "verb": "about_changed"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupActionLogs",
    "links": [
      "self": "https://kitsu.io/api/edge/group-action-logs/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "verb": nil
    ]
  ]
  
  var groupActionLog: ActionLog?
  var groupActionLogAttributes: ActionLogAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    groupActionLog = nil
    groupActionLogAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupActionLogFullyFilled() {
    groupActionLog = parseJson(fullyFilledJSON)
    groupActionLogAttributes = groupActionLog?.attributes
    
    XCTAssertNotNil(groupActionLog)
    
    XCTAssertEqual(groupActionLog?.objectID, "1")
    XCTAssertEqual(groupActionLog?.type, "groupActionLogs")
    
    XCTAssertNotNil(groupActionLog?.links)
    
    XCTAssertNotNil(groupActionLogAttributes)
    
    XCTAssertEqual(
      groupActionLogAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupActionLogAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupActionLogAttributes?.verb, "about_changed")
  }
  
  func testGroupActionLogValidMissingData() {
    groupActionLog = parseJson(validMissingDataJSON)
    groupActionLogAttributes = groupActionLog?.attributes
    
    XCTAssertNotNil(groupActionLog)
    
    XCTAssertEqual(groupActionLog?.objectID, "1")
    XCTAssertEqual(groupActionLog?.type, "groupActionLogs")
    
    XCTAssertNotNil(groupActionLog?.links)
    
    XCTAssertNotNil(groupActionLogAttributes)
    
    XCTAssertEqual(
      groupActionLogAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertNil(groupActionLogAttributes?.updatedAt)
    XCTAssertEqual(groupActionLogAttributes?.verb, "about_changed")
  }
  
  func testGroupActionLogValidNilData() {
    groupActionLog = parseJson(validNilDataJSON)
    groupActionLogAttributes = groupActionLog?.attributes
    
    XCTAssertNotNil(groupActionLog)
    
    XCTAssertEqual(groupActionLog?.objectID, "1")
    XCTAssertEqual(groupActionLog?.type, "groupActionLogs")
    
    XCTAssertNotNil(groupActionLog?.links)
    
    XCTAssertNotNil(groupActionLogAttributes)
    
    XCTAssertEqual(
      groupActionLogAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertNil(groupActionLogAttributes?.updatedAt)
    XCTAssertEqual(groupActionLogAttributes?.verb, "about_changed")
  }
  
  func testGroupActionLogInvalidMissingData() {
    groupActionLog = parseJson(invalidMissingDataJSON)
    groupActionLogAttributes = groupActionLog?.attributes
    
    XCTAssertNotNil(groupActionLog)
    
    XCTAssertEqual(groupActionLog?.objectID, "1")
    XCTAssertEqual(groupActionLog?.type, "groupActionLogs")
    
    XCTAssertNotNil(groupActionLog?.links)
    
    XCTAssertNil(groupActionLogAttributes)
  }
  
  func testGroupActionLogInvalidNilData() {
    groupActionLog = parseJson(invalidNilDataJSON)
    groupActionLogAttributes = groupActionLog?.attributes
    
    XCTAssertNotNil(groupActionLog)
    
    XCTAssertEqual(groupActionLog?.objectID, "1")
    XCTAssertEqual(groupActionLog?.type, "groupActionLogs")
    
    XCTAssertNotNil(groupActionLog?.links)
    
    XCTAssertNil(groupActionLogAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> ActionLog? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(ActionLog.self, from: data!)
  }
}
