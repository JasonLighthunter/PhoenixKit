import XCTest
@testable import Groups

class ReportStatusTests: XCTestCase {
  func testReportStatus() {
    XCTAssertEqual(ReportStatus(rawValue: "reported"), .reported)
    XCTAssertEqual(ReportStatus(rawValue: "resolved"), .resolved)
    XCTAssertEqual(ReportStatus(rawValue: "declined"), .declined)
    XCTAssertNil(ReportStatus(rawValue: "InvalidInput"))
  }
}
