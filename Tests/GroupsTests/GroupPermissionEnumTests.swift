import XCTest
@testable import Groups

class GroupPermissionEnumTests: XCTestCase {
  func testGroupPermissionScope() {
    XCTAssertEqual(PermissionScope(rawValue: "owner"), .owner)
    XCTAssertEqual(PermissionScope(rawValue: "tickets"), .tickets)
    XCTAssertEqual(PermissionScope(rawValue: "members"), .members)
    XCTAssertEqual(PermissionScope(rawValue: "leaders"), .leaders)
    XCTAssertEqual(PermissionScope(rawValue: "community"), .community)
    XCTAssertEqual(PermissionScope(rawValue: "content"), .content)
    XCTAssertNil(PermissionScope(rawValue: "InvalidInput"))
  }
}
