import XCTest
@testable import Groups

class GroupNeighborTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupNeighbors",
    "links": [
      "self": "https://kitsu.io/api/edge/group-neighbors/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "1",
//    "type": "groupNeighbors",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-neighbors/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z"
//    ]
//  ]
//
//  let validNilDataJSON: [String : Any?] = [
//    "id": "1",
//    "type": "groupNeighbors",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-neighbors/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupNeighbors",
    "links": [
      "self": "https://kitsu.io/api/edge/group-neighbors/1"
    ],
    "attributes": [
      "updatedAt": "2017-10-26T20:51:24.215Z"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupNeighbors",
    "links": [
      "self": "https://kitsu.io/api/edge/group-neighbors/1"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": "2017-10-26T20:51:24.215Z"
    ]
  ]
  
  var groupNeighbor: Neighbor?
  var groupNeighborAttributes: NeighborAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    groupNeighbor = nil
    groupNeighborAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupNeighborFullyFilled() {
    groupNeighbor = parseJson(fullyFilledJSON)
    groupNeighborAttributes = groupNeighbor?.attributes
    
    XCTAssertNotNil(groupNeighbor)
    
    XCTAssertEqual(groupNeighbor?.objectID, "1")
    XCTAssertEqual(groupNeighbor?.type, "groupNeighbors")
    
    XCTAssertNotNil(groupNeighbor?.links)
    
    XCTAssertNotNil(groupNeighborAttributes)
    
    XCTAssertEqual(
      groupNeighborAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupNeighborAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
  }
  
//  func testGroupNeighborValidMissingData() {
//    groupNeighbor = parseJson(validMissingDataJSON)
//    groupNeighborAttributes = groupNeighbor?.attributes
//
//    XCTAssertNotNil(groupNeighbor)
//
//    XCTAssertEqual(groupNeighbor?.objectID, "1")
//    XCTAssertEqual(groupNeighbor?.type, "groupNeighbors")
//
//    XCTAssertNotNil(groupNeighbor?.links)
//
//    XCTAssertNotNil(groupNeighborAttributes)
//
//    XCTAssertEqual(groupNeighborAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupNeighborAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//  }
//
//  func testGroupNeighborValidNilData() {
//    groupNeighbor = parseJson(validNilDataJSON)
//    groupNeighborAttributes = groupNeighbor?.attributes
//
//    XCTAssertNotNil(groupNeighbor)
//
//    XCTAssertEqual(groupNeighbor?.objectID, "1")
//    XCTAssertEqual(groupNeighbor?.type, "groupNeighbors")
//
//    XCTAssertNotNil(groupNeighbor?.links)
//
//    XCTAssertNotNil(groupNeighborAttributes)
//
//    XCTAssertEqual(groupNeighborAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupNeighborAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//  }

  func testGroupNeighborInvalidMissingData() {
    groupNeighbor = parseJson(invalidMissingDataJSON)
    groupNeighborAttributes = groupNeighbor?.attributes
    
    XCTAssertNotNil(groupNeighbor)
    
    XCTAssertEqual(groupNeighbor?.objectID, "1")
    XCTAssertEqual(groupNeighbor?.type, "groupNeighbors")
    
    XCTAssertNotNil(groupNeighbor?.links)
    
    XCTAssertNil(groupNeighborAttributes)
  }
  
  func testGroupNeighborInvalidNilData() {
    groupNeighbor = parseJson(invalidNilDataJSON)
    groupNeighborAttributes = groupNeighbor?.attributes
    
    XCTAssertNotNil(groupNeighbor)
    
    XCTAssertEqual(groupNeighbor?.objectID, "1")
    XCTAssertEqual(groupNeighbor?.type, "groupNeighbors")
    
    XCTAssertNotNil(groupNeighbor?.links)
    
    XCTAssertNil(groupNeighborAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Neighbor? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Neighbor.self, from: data!)
  }
}
