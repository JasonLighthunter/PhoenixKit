import XCTest
@testable import Groups

class GroupPermissionTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "group-permissions",
    "links": [
      "self": "https://kitsu.io/api/edge/group-permissions/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "permission": "members"
    ]
  ]

  //  let validMissingDataJSON: [String : Any] = [
  //    "id": "1",
  //    "type": "group-permissions",
  //    "links": [
  //      "self": "https://kitsu.io/api/edge/group-permissions/1"
  //    ],
  //    "attributes": [
  //      "createdAt": "2015-02-17T21:16:53.207Z",
  //      "updatedAt": "2017-10-26T20:51:24.215Z",
  //      "permission": "members",
  //    ]
  //  ]

  //  let validNilDataJSON: [String : Any?] = [
  //    "id": "1",
  //    "type": "group-permissions",
  //    "links": [
  //      "self": "https://kitsu.io/api/edge/group-permissions/1"
  //    ],
  //    "attributes": [
  //      "createdAt": "2015-02-17T21:16:53.207Z",
  //      "updatedAt": "2017-10-26T20:51:24.215Z",
  //      "permission": "members",
  //    ]
  //  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "group-permissions",
    "links": [
      "self": "https://kitsu.io/api/edge/group-permissions/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "group-permissions",
    "links": [
      "self": "https://kitsu.io/api/edge/group-permissions/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": nil,
      "permission": "members",
    ]
  ]
  
  var groupPermission: Permission?
  var groupPermissionAttributes: PermissionAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    groupPermission = nil
    groupPermissionAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupPermissionFullyFilled() {
    groupPermission = parseJson(fullyFilledJSON)
    groupPermissionAttributes = groupPermission?.attributes
    
    XCTAssertNotNil(groupPermission)
    
    XCTAssertEqual(groupPermission?.objectID, "1")
    XCTAssertEqual(groupPermission?.type, "group-permissions")
    
    XCTAssertNotNil(groupPermission?.links)
    
    XCTAssertNotNil(groupPermissionAttributes)
    
    XCTAssertEqual(
      groupPermissionAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupPermissionAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupPermissionAttributes?.permission, .members)
  }
  
  //  func testGroupPermissionValidMissingData() {
  //    groupPermission = parseJson(validMissingDataJSON)
  //    groupPermissionAttributes = groupPermission?.attributes
  //
  //    XCTAssertNotNil(groupPermission)
  //
  //    XCTAssertEqual(groupPermission?.objectID, "1")
  //    XCTAssertEqual(groupPermission?.type, "group-permissions")
  //
  //    XCTAssertNotNil(groupPermission?.links)
  //
  //    XCTAssertNotNil(groupPermissionAttributes)
  //
  //    XCTAssertEqual(groupPermissionAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
  //    XCTAssertEqual(groupPermissionAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
  //    XCTAssertEqual(groupPermissionAttributes?.permission, GroupPermissionEnum.members)
  //  }
  
  //  func testGroupPermissionValidNilData() {
  //    groupPermission = parseJson(validNilDataJSON)
  //    groupPermissionAttributes = groupPermission?.attributes
  //
  //    XCTAssertNotNil(groupPermission)
  //
  //    XCTAssertEqual(groupPermission?.objectID, "1")
  //    XCTAssertEqual(groupPermission?.type, "group-permissions")
  //
  //    XCTAssertNotNil(groupPermission?.links)
  //
  //    XCTAssertNotNil(groupPermissionAttributes)
  //
  //    XCTAssertEqual(groupPermissionAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
  //    XCTAssertEqual(groupPermissionAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
  //    XCTAssertEqual(groupPermissionAttributes?.permission, GroupPermissionEnum.members)
  //  }

  func testGroupPermissionInvalidMissingData() {
    groupPermission = parseJson(invalidMissingDataJSON)
    groupPermissionAttributes = groupPermission?.attributes
    
    XCTAssertNotNil(groupPermission)
    
    XCTAssertEqual(groupPermission?.objectID, "1")
    XCTAssertEqual(groupPermission?.type, "group-permissions")
    
    XCTAssertNotNil(groupPermission?.links)
    
    XCTAssertNil(groupPermissionAttributes)
  }
  
  func testGroupPermissionInvalidNilData() {
    groupPermission = parseJson(invalidNilDataJSON)
    groupPermissionAttributes = groupPermission?.attributes
    
    XCTAssertNotNil(groupPermission)
    
    XCTAssertEqual(groupPermission?.objectID, "1")
    XCTAssertEqual(groupPermission?.type, "group-permissions")
    
    XCTAssertNotNil(groupPermission?.links)
    
    XCTAssertNil(groupPermissionAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Permission? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Permission.self, from: data!)
  }
}
