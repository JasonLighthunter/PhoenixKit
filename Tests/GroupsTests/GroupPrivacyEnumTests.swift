import XCTest
@testable import Groups

class PrivacyScopeTests: XCTestCase {
  func testPrivacyScope() {
    XCTAssertEqual(PrivacyScope(rawValue: "open"), .open)
    XCTAssertEqual(PrivacyScope(rawValue: "closed"), .closed)
    XCTAssertEqual(PrivacyScope(rawValue: "restricted"), .restricted)
    XCTAssertNil(PrivacyScope(rawValue: "InvalidInput"))
  }
}
