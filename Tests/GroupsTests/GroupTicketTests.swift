import XCTest
@testable import Groups

class GroupTicketTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupTickets",
    "links": [
      "self": "https://kitsu.io/api/edge/group-tickets/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "status": "resolved"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "1",
//    "type": "groupTickets",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-tickets/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//      "status": "resolved"
//    ]
//  ]
//
//  let validNilDataJSON: [String : Any?] = [
//    "id": "1",
//    "type": "groupTickets",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-tickets/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//      "status": "resolved"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupTickets",
    "links": [
      "self": "https://kitsu.io/api/edge/group-tickets/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "status": "resolved"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupTickets",
    "links": [
      "self": "https://kitsu.io/api/edge/group-tickets/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": nil,
      "status": "resolved"
    ]
  ]
  
  var groupTicket: Ticket?
  var groupTicketAttributes: TicketAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    groupTicket = nil
    groupTicketAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupTicketFullyFilled() {
    groupTicket = parseJson(fullyFilledJSON)
    groupTicketAttributes = groupTicket?.attributes
    
    XCTAssertNotNil(groupTicket)
    
    XCTAssertEqual(groupTicket?.objectID, "1")
    XCTAssertEqual(groupTicket?.type, "groupTickets")
    
    XCTAssertNotNil(groupTicket?.links)
    
    XCTAssertNotNil(groupTicketAttributes)
    
    XCTAssertEqual(
      groupTicketAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupTicketAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupTicketAttributes?.status, .resolved)
  }
  
//  func testGroupTicketValidMissingData() {
//    groupTicket = parseJson(validMissingDataJSON)
//    groupTicketAttributes = groupTicket?.attributes
//
//    XCTAssertNotNil(groupTicket)
//
//    XCTAssertEqual(groupTicket?.objectID, "1")
//    XCTAssertEqual(groupTicket?.type, "groupTickets")
//
//    XCTAssertNotNil(groupTicket?.links)
//
//    XCTAssertNotNil(groupTicketAttributes)
//
//    XCTAssertEqual(groupTicketAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupTicketAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//    XCTAssertEqual(groupTicketAttributes?.status, GroupTicketStatusEnum.resolved)
//  }
//
//  func testGroupTicketValidNilData() {
//    groupTicket = parseJson(validNilDataJSON)
//    groupTicketAttributes = groupTicket?.attributes
//
//    XCTAssertNotNil(groupTicket)
//
//    XCTAssertEqual(groupTicket?.objectID, "1")
//    XCTAssertEqual(groupTicket?.type, "groupTickets")
//
//    XCTAssertNotNil(groupTicket?.links)
//
//    XCTAssertNotNil(groupTicketAttributes)
//
//    XCTAssertEqual(groupTicketAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupTicketAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//    XCTAssertEqual(groupTicketAttributes?.status, GroupTicketStatusEnum.resolved)
//  }

  func testGroupTicketInvalidMissingData() {
    groupTicket = parseJson(invalidMissingDataJSON)
    groupTicketAttributes = groupTicket?.attributes
    
    XCTAssertNotNil(groupTicket)
    
    XCTAssertEqual(groupTicket?.objectID, "1")
    XCTAssertEqual(groupTicket?.type, "groupTickets")
    
    XCTAssertNotNil(groupTicket?.links)
    
    XCTAssertNil(groupTicketAttributes)
  }
  
  func testGroupTicketInvalidNilData() {
    groupTicket = parseJson(invalidNilDataJSON)
    groupTicketAttributes = groupTicket?.attributes
    
    XCTAssertNotNil(groupTicket)
    
    XCTAssertEqual(groupTicket?.objectID, "1")
    XCTAssertEqual(groupTicket?.type, "groupTickets")
    
    XCTAssertNotNil(groupTicket?.links)
    
    XCTAssertNil(groupTicketAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Ticket? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Ticket.self, from: data!)
  }
}
