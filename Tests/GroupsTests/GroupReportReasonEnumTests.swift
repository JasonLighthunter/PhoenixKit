import XCTest
@testable import Groups

class GroupReportReasonEnumTests: XCTestCase {
  func testGroupReportReasonEnum() {
    XCTAssertEqual(ReportReason(rawValue: "nsfw"), .nsfw)
    XCTAssertEqual(ReportReason(rawValue: "offensive"), .offensive)
    XCTAssertEqual(ReportReason(rawValue: "spoiler"), .spoiler)
    XCTAssertEqual(ReportReason(rawValue: "bullying"), .bullying)
    XCTAssertEqual(ReportReason(rawValue: "other"), .other)
    XCTAssertEqual(ReportReason(rawValue: "spam"), .spam)
    XCTAssertNil(ReportReason(rawValue: "InvalidInput"))
  }
}
