import XCTest
@testable import Groups

class LeaderChatMessageTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "leaderChatMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/leader-chat-messages/1"
    ],
    "attributes": [
      "createdAt": "2017-03-12T17:23:36.640Z",
      "updatedAt": "2017-03-12T17:23:36.640Z",
      "content": "Does this work?",
      "contentFormatted": "<p>Does this work?</p>\\n",
      "editedAt": "2017-03-12T17:23:36.640Z"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "leaderChatMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/leader-chat-messages/1"
    ],
    "attributes": [
      "createdAt": "2017-03-12T17:23:36.640Z",
      "updatedAt": "2017-03-12T17:23:36.640Z",
      "content": "Does this work?",
      "contentFormatted": "<p>Does this work?</p>\\n"
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "leaderChatMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/leader-chat-messages/1"
    ],
    "attributes": [
      "createdAt": "2017-03-12T17:23:36.640Z",
      "updatedAt": "2017-03-12T17:23:36.640Z",
      "content": "Does this work?",
      "contentFormatted": "<p>Does this work?</p>\\n",
      "editedAt": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "leaderChatMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/leader-chat-messages/1"
    ],
    "attributes": [
      "createdAt": "2017-03-12T17:23:36.640Z",
      "updatedAt": "2017-03-12T17:23:36.640Z",
      "content": "Does this work?",
      "editedAt": "2017-03-12T17:23:36.640Z"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "leaderChatMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/leader-chat-messages/1"
    ],
    "attributes": [
      "createdAt": "2017-03-12T17:23:36.640Z",
      "updatedAt": "2017-03-12T17:23:36.640Z",
      "content": nil,
      "contentFormatted": "<p>Does this work?</p>\\n",
      "editedAt": "2017-03-12T17:23:36.640Z"
    ]
  ]
  
  var leaderChatMessage: LeaderChatMessage?
  var leaderChatMessageAttributes: LeaderChatMessageAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    leaderChatMessage = nil
    leaderChatMessageAttributes = nil
    
    super.tearDown()
  }
  
  func testLeaderChatMessageFullyFilled() {
    leaderChatMessage = parseJson(fullyFilledJSON)
    leaderChatMessageAttributes = leaderChatMessage?.attributes
    
    XCTAssertNotNil(leaderChatMessage)
    
    XCTAssertEqual(leaderChatMessage?.objectID, "1")
    XCTAssertEqual(leaderChatMessage?.type, "leaderChatMessages")
    
    XCTAssertNotNil(leaderChatMessage?.links)
    
    XCTAssertNotNil(leaderChatMessageAttributes)
    
    XCTAssertEqual(
      leaderChatMessageAttributes?.createdAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
    XCTAssertEqual(
      leaderChatMessageAttributes?.updatedAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
    XCTAssertEqual(leaderChatMessageAttributes?.content, "Does this work?")
    XCTAssertEqual(leaderChatMessageAttributes?.contentFormatted, "<p>Does this work?</p>\\n")
    XCTAssertEqual(
      leaderChatMessageAttributes?.editedAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
  }
  
  func testLeaderChatMessageValidMissingData() {
    leaderChatMessage = parseJson(validMissingDataJSON)
    leaderChatMessageAttributes = leaderChatMessage?.attributes
    
    XCTAssertNotNil(leaderChatMessage)
    
    XCTAssertEqual(leaderChatMessage?.objectID, "1")
    XCTAssertEqual(leaderChatMessage?.type, "leaderChatMessages")
    
    XCTAssertNotNil(leaderChatMessage?.links)
    
    XCTAssertNotNil(leaderChatMessageAttributes)
    
    XCTAssertEqual(
      leaderChatMessageAttributes?.createdAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
    XCTAssertEqual(
      leaderChatMessageAttributes?.updatedAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
    XCTAssertEqual(leaderChatMessageAttributes?.content, "Does this work?")
    XCTAssertEqual(leaderChatMessageAttributes?.contentFormatted, "<p>Does this work?</p>\\n")
    XCTAssertNil(leaderChatMessageAttributes?.editedAt)
  }
  
  func testLeaderChatMessageValidNilData() {
    leaderChatMessage = parseJson(validNilDataJSON)
    leaderChatMessageAttributes = leaderChatMessage?.attributes
    
    XCTAssertNotNil(leaderChatMessage)
    
    XCTAssertEqual(leaderChatMessage?.objectID, "1")
    XCTAssertEqual(leaderChatMessage?.type, "leaderChatMessages")
    
    XCTAssertNotNil(leaderChatMessage?.links)
    
    XCTAssertNotNil(leaderChatMessageAttributes)
    
    XCTAssertEqual(
      leaderChatMessageAttributes?.createdAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
    XCTAssertEqual(
      leaderChatMessageAttributes?.updatedAt, dateFormatter.date(from: "2017-03-12T17:23:36.640Z")
    )
    XCTAssertEqual(leaderChatMessageAttributes?.content, "Does this work?")
    XCTAssertEqual(leaderChatMessageAttributes?.contentFormatted, "<p>Does this work?</p>\\n")
    XCTAssertNil(leaderChatMessageAttributes?.editedAt)
  }
  
  func testLeaderChatMessageInvalidMissingData() {
    leaderChatMessage = parseJson(invalidMissingDataJSON)
    leaderChatMessageAttributes = leaderChatMessage?.attributes
    
    XCTAssertNotNil(leaderChatMessage)
    
    XCTAssertEqual(leaderChatMessage?.objectID, "1")
    XCTAssertEqual(leaderChatMessage?.type, "leaderChatMessages")
    
    XCTAssertNotNil(leaderChatMessage?.links)
    
    XCTAssertNil(leaderChatMessageAttributes)
  }
  
  func testLeaderChatMessageInvalidNilData() {
    leaderChatMessage = parseJson(invalidNilDataJSON)
    leaderChatMessageAttributes = leaderChatMessage?.attributes
    
    XCTAssertNotNil(leaderChatMessage)
    
    XCTAssertEqual(leaderChatMessage?.objectID, "1")
    XCTAssertEqual(leaderChatMessage?.type, "leaderChatMessages")
    
    XCTAssertNotNil(leaderChatMessage?.links)
    
    XCTAssertNil(leaderChatMessageAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> LeaderChatMessage? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(LeaderChatMessage.self, from: data!)
  }
}
