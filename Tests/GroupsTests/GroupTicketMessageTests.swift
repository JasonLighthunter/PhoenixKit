import XCTest
@testable import Groups

class GroupTicketMessageTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupTicketMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/group-ticket-messages/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "kind": "message",
      "content": "Could u please add this"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "1",
//    "type": "groupTicketMessages",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-reports/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//      "kind": "message",
//      "content": "Could u please add this"
//    ]
//  ]

//  let validNilDataJSON: [String : Any?] = [
//    "id": "1",
//    "type": "groupTicketMessages",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-reports/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//      "kind": "message",
//      "content": "Could u please add this"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupTicketMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "kind": "message",
      "content": "Could u please add this recap episode"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupTicketMessages",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "kind": "message",
      "content": nil
    ]
  ]
  
  var groupTicketMessage: TicketMessage?
  var groupTicketMessageAttributes: TicketMessageAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    groupTicketMessage = nil
    groupTicketMessageAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupTicketMessageFullyFilled() {
    groupTicketMessage = parseJson(fullyFilledJSON)
    groupTicketMessageAttributes = groupTicketMessage?.attributes
    
    XCTAssertNotNil(groupTicketMessage)
    
    XCTAssertEqual(groupTicketMessage?.objectID, "1")
    XCTAssertEqual(groupTicketMessage?.type, "groupTicketMessages")
    
    XCTAssertNotNil(groupTicketMessage?.links)
    
    XCTAssertNotNil(groupTicketMessageAttributes)
    
    XCTAssertEqual(
      groupTicketMessageAttributes?.createdAt,
      dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupTicketMessageAttributes?.updatedAt,
      dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupTicketMessageAttributes?.kind, .message)
    XCTAssertEqual(groupTicketMessageAttributes?.content, "Could u please add this")
  }
  
//  func testGroupTicketMessageValidMissingData() {
//    groupTicketMessage = parseJson(validMissingDataJSON)
//    groupTicketMessageAttributes = groupTicketMessage?.attributes
//
//    XCTAssertNotNil(groupTicketMessage)
//
//    XCTAssertEqual(groupTicketMessage?.objectID, "1")
//    XCTAssertEqual(groupTicketMessage?.type, "groupTicketMessages")
//
//    XCTAssertNotNil(groupTicketMessage?.links)
//
//    XCTAssertNotNil(groupTicketMessageAttributes)
//
//    XCTAssertEqual(groupTicketMessageAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupTicketMessageAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//    XCTAssertEqual(groupTicketMessageAttributes?.kind, GroupTicketMessageKindEnum.message)
//    XCTAssertEqual(groupTicketMessageAttributes?.content, "Could u please add this")
//  }

//  func testGroupTicketMessageValidNilData() {
//    groupTicketMessage = parseJson(validNilDataJSON)
//    groupTicketMessageAttributes = groupTicketMessage?.attributes
//
//    XCTAssertNotNil(groupTicketMessage)
//
//    XCTAssertEqual(groupTicketMessage?.objectID, "1")
//    XCTAssertEqual(groupTicketMessage?.type, "groupTicketMessages")
//
//    XCTAssertNotNil(groupTicketMessage?.links)
//
//    XCTAssertNotNil(groupTicketMessageAttributes)
//
//    XCTAssertEqual(groupTicketMessageAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupTicketMessageAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//    XCTAssertEqual(groupTicketMessageAttributes?.kind, GroupTicketMessageKindEnum.message)
//    XCTAssertEqual(groupTicketMessageAttributes?.content, "Could u please add this")
//  }

  func testGroupTicketMessageInvalidMissingData() {
   groupTicketMessage = parseJson(invalidMissingDataJSON)
    groupTicketMessageAttributes = groupTicketMessage?.attributes
    
    XCTAssertNotNil(groupTicketMessage)
    
    XCTAssertEqual(groupTicketMessage?.objectID, "1")
    XCTAssertEqual(groupTicketMessage?.type, "groupTicketMessages")
    
    XCTAssertNotNil(groupTicketMessage?.links)
    
    XCTAssertNil(groupTicketMessageAttributes)
  }
  
  func testGroupTicketMessageInvalidNilData() {
    groupTicketMessage = parseJson(invalidNilDataJSON)
    groupTicketMessageAttributes = groupTicketMessage?.attributes
    
    XCTAssertNotNil(groupTicketMessage)
    
    XCTAssertEqual(groupTicketMessage?.objectID, "1")
    XCTAssertEqual(groupTicketMessage?.type, "groupTicketMessages")
    
    XCTAssertNotNil(groupTicketMessage?.links)
    
    XCTAssertNil(groupTicketMessageAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> TicketMessage? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(TicketMessage.self, from: data!)
  }
}
