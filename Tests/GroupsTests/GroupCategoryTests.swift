import XCTest
@testable import Groups

class GroupCategoryTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupCategories",
    "links": [
      "self": "https://kitsu.io/api/edge/group-categories/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "name": "testName",
      "slug": "one-piece-group",
      "description": "Group for fans of the One Piece",
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupCategories",
    "links": [
      "self": "https://kitsu.io/api/edge/group-categories/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "name": "testName",
      "slug": "one-piece-group",
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupCategories",
    "links": [
      "self": "https://kitsu.io/api/edge/group-categories/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "name": "testName",
      "slug": "one-piece-group",
      "description": nil,
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupCategories",
    "links": [
      "self": "https://kitsu.io/api/edge/group-categories/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "name": "testName",
      "slug": "one-piece-group",
      "description": "Group for fans of the One Piece",
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupCategories",
    "links": [
      "self": "https://kitsu.io/api/edge/group-categories/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "name": nil,
      "slug": "one-piece-group",
      "description": "Group for fans of the One Piece",
    ]
  ]
  
  var groupCategory: Groups.Category?
  var groupCategoryAttributes: CategoryAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    groupCategory = nil
    groupCategoryAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupCategoryFullyFilled() {
    groupCategory = parseJson(fullyFilledJSON)
    groupCategoryAttributes = groupCategory?.attributes
    
    XCTAssertNotNil(groupCategory)
    
    XCTAssertEqual(groupCategory?.objectID, "1")
    XCTAssertEqual(groupCategory?.type, "groupCategories")
    
    XCTAssertNotNil(groupCategory?.links)
    
    XCTAssertNotNil(groupCategoryAttributes)
    
    XCTAssertEqual(
      groupCategoryAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupCategoryAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupCategoryAttributes?.name, "testName")
    XCTAssertEqual(groupCategoryAttributes?.slug, "one-piece-group")
    XCTAssertEqual(groupCategoryAttributes?.description, "Group for fans of the One Piece")
  }
  
  func testGroupCategoryValidMissingData() {
    groupCategory = parseJson(validMissingDataJSON)
    groupCategoryAttributes = groupCategory?.attributes
    
    XCTAssertNotNil(groupCategory)
    
    XCTAssertEqual(groupCategory?.objectID, "1")
    XCTAssertEqual(groupCategory?.type, "groupCategories")
    
    XCTAssertNotNil(groupCategory?.links)
    
    XCTAssertNotNil(groupCategoryAttributes)
    
    XCTAssertEqual(
      groupCategoryAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupCategoryAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupCategoryAttributes?.name, "testName")
    XCTAssertEqual(groupCategoryAttributes?.slug, "one-piece-group")
    XCTAssertNil(groupCategoryAttributes?.description)
  }
  
  func testGroupCategoryValidNilData() {
    groupCategory = parseJson(validNilDataJSON)
    groupCategoryAttributes = groupCategory?.attributes
    
    XCTAssertNotNil(groupCategory)
    
    XCTAssertEqual(groupCategory?.objectID, "1")
    XCTAssertEqual(groupCategory?.type, "groupCategories")
    
    XCTAssertNotNil(groupCategory?.links)
    
    XCTAssertNotNil(groupCategoryAttributes)
    
    XCTAssertEqual(
      groupCategoryAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupCategoryAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupCategoryAttributes?.name, "testName")
    XCTAssertEqual(groupCategoryAttributes?.slug, "one-piece-group")
    XCTAssertNil(groupCategoryAttributes?.description)
  }
  
  func testGroupCategoryInvalidMissingData() {
    groupCategory = parseJson(invalidMissingDataJSON)
    groupCategoryAttributes = groupCategory?.attributes
    
    XCTAssertNotNil(groupCategory)
    
    XCTAssertEqual(groupCategory?.objectID, "1")
    XCTAssertEqual(groupCategory?.type, "groupCategories")
    
    XCTAssertNotNil(groupCategory?.links)
    
    XCTAssertNil(groupCategoryAttributes)
  }
  
  func testGroupCategoryInvalidNilData() {
    groupCategory = parseJson(invalidNilDataJSON)
    groupCategoryAttributes = groupCategory?.attributes
    
    XCTAssertNotNil(groupCategory)
    
    XCTAssertEqual(groupCategory?.objectID, "1")
    XCTAssertEqual(groupCategory?.type, "groupCategories")
    
    XCTAssertNotNil(groupCategory?.links)
    
    XCTAssertNil(groupCategoryAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Groups.Category? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Groups.Category.self, from: data!)
  }
}
