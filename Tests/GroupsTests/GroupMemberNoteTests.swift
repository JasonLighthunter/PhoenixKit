import XCTest
@testable import Groups

class GroupMemberNoteTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupMemberNotes",
    "links": [
      "self": "https://kitsu.io/api/edge/group-member-notes/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "content": "testContent",
      "contentFormatted": "testContentFormatted"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "1",
//    "type": "groupMemberNotes",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-member-notes/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//      "content": "testContent",
//      "contentFormatted": "testContentFormatted"
//    ]
//  ]

//  let validNilDataJSON: [String : Any?] = [
//    "id": "1",
//    "type": "groupMemberNotes",
//    "links": [
//      "self": "https://kitsu.io/api/edge/group-member-notes/1"
//    ],
//    "attributes": [
//      "createdAt": "2015-02-17T21:16:53.207Z",
//      "updatedAt": "2017-10-26T20:51:24.215Z",
//      "content": "testContent",
//      "contentFormatted": "testContentFormatted"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupMemberNotes",
    "links": [
      "self": "https://kitsu.io/api/edge/group-member-notes/1"
    ],
    "attributes": [
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "content": "testContent",
      "contentFormatted": "testContentFormatted"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupMemberNotes",
    "links": [
      "self": "https://kitsu.io/api/edge/group-member-notes/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "content": nil,
      "contentFormatted": "testContentFormatted"
    ]
  ]
  
  var groupMemberNote: MemberNote?
  var groupMemberNoteAttributes: MemberNoteAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    groupMemberNote = nil
    groupMemberNoteAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupMemberNoteFullyFilled() {
    groupMemberNote = parseJson(fullyFilledJSON)
    groupMemberNoteAttributes = groupMemberNote?.attributes
    
    XCTAssertNotNil(groupMemberNote)
    
    XCTAssertEqual(groupMemberNote?.objectID, "1")
    XCTAssertEqual(groupMemberNote?.type, "groupMemberNotes")
    
    XCTAssertNotNil(groupMemberNote?.links)
    
    XCTAssertNotNil(groupMemberNoteAttributes)
    
    XCTAssertEqual(
      groupMemberNoteAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupMemberNoteAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupMemberNoteAttributes?.content, "testContent")
    XCTAssertEqual(groupMemberNoteAttributes?.contentFormatted, "testContentFormatted")
  }
  
//  func testGroupMemberNoteValidMissingData() {
//    groupMemberNote = parseJson(validMissingDataJSON)
//    groupMemberNoteAttributes = groupMemberNote?.attributes
//
//    XCTAssertNotNil(groupMemberNote)
//
//    XCTAssertEqual(groupMemberNote?.objectID, "1")
//    XCTAssertEqual(groupMemberNote?.type, "groupMemberNotes")
//
//    XCTAssertNotNil(groupMemberNote?.links)
//
//    XCTAssertNotNil(groupMemberNoteAttributes)
//
//    XCTAssertEqual(groupMemberNoteAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupMemberNoteAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//    XCTAssertEqual(groupMemberNoteAttributes?.content, "testContent")
//    XCTAssertEqual(groupMemberNoteAttributes?.contentFormatted, "testContentFormatted")
//  }

//  func testGroupMemberNoteValidNilData() {
//    groupMemberNote = parseJson(validNilDataJSON)
//    groupMemberNoteAttributes = groupMemberNote?.attributes
//
//    XCTAssertNotNil(groupMemberNote)
//
//    XCTAssertEqual(groupMemberNote?.objectID, "1")
//    XCTAssertEqual(groupMemberNote?.type, "groupMemberNotes")
//
//    XCTAssertNotNil(groupMemberNote?.links)
//
//    XCTAssertNotNil(groupMemberNoteAttributes)
//
//    XCTAssertEqual(groupMemberNoteAttributes?.createdAt, "2015-02-17T21:16:53.207Z")
//    XCTAssertEqual(groupMemberNoteAttributes?.updatedAt, "2017-10-26T20:51:24.215Z")
//    XCTAssertEqual(groupMemberNoteAttributes?.content, "testContent")
//    XCTAssertEqual(groupMemberNoteAttributes?.contentFormatted, "testContentFormatted")
//  }

  func testGroupMemberNoteInvalidMissingData() {
    groupMemberNote = parseJson(invalidMissingDataJSON)
    groupMemberNoteAttributes = groupMemberNote?.attributes
    
    XCTAssertNotNil(groupMemberNote)
    
    XCTAssertEqual(groupMemberNote?.objectID, "1")
    XCTAssertEqual(groupMemberNote?.type, "groupMemberNotes")
    
    XCTAssertNotNil(groupMemberNote?.links)
    
    XCTAssertNil(groupMemberNoteAttributes)
  }
  
  func testGroupMemberNoteInvalidNilData() {
    groupMemberNote = parseJson(invalidNilDataJSON)
    groupMemberNoteAttributes = groupMemberNote?.attributes
    
    XCTAssertNotNil(groupMemberNote)
    
    XCTAssertEqual(groupMemberNote?.objectID, "1")
    XCTAssertEqual(groupMemberNote?.type, "groupMemberNotes")
    
    XCTAssertNotNil(groupMemberNote?.links)
    
    XCTAssertNil(groupMemberNoteAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> MemberNote? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(MemberNote.self, from: data!)
  }
}
