import XCTest
@testable import Groups

class GroupReportTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupReports",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "reason": "nsfw",
      "status": "reported",
      "explanation": "testExplanation",
      "naughtyType": "Comment",
      "naughtyId": 1
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupReports",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "reason": "nsfw",
      "status": "reported",
      "naughtyType": "Comment",
      "naughtyId": 1
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupReports",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "reason": "nsfw",
      "status": "reported",
      "explanation": nil,
      "naughtyType": "Comment",
      "naughtyId": 1
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupReports",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "reason": "nsfw",
      "status": "reported",
      "explanation": "testExplanation",
      "naughtyType": "Comment",
      "naughtyId": 1
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupReports",
    "links": [
      "self": "https://kitsu.io/api/edge/group-reports/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "reason": "nsfw",
      "status": nil,
      "explanation": "testExplanation",
      "naughtyType": "Comment",
      "naughtyId": 1
    ]
  ]
  
  var groupReport: Report?
  var groupReportAttributes: ReportAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    groupReport = nil
    groupReportAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupReportFullyFilled() {
    groupReport = parseJson(fullyFilledJSON)
    groupReportAttributes = groupReport?.attributes
    
    XCTAssertNotNil(groupReport)
    
    XCTAssertEqual(groupReport?.objectID, "1")
    XCTAssertEqual(groupReport?.type, "groupReports")
    
    XCTAssertNotNil(groupReport?.links)
    
    XCTAssertNotNil(groupReportAttributes)
    
    XCTAssertEqual(
      groupReportAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupReportAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupReportAttributes?.reason, .nsfw)
    XCTAssertEqual(groupReportAttributes?.status, .reported)
    XCTAssertEqual(groupReportAttributes?.explanation, "testExplanation")
    XCTAssertEqual(groupReportAttributes?.naughtyType, "Comment")
    XCTAssertEqual(groupReportAttributes?.naughtyID, 1)
  }
  
  func testGroupReportValidMissingData() {
    groupReport = parseJson(validMissingDataJSON)
    groupReportAttributes = groupReport?.attributes
    
    XCTAssertNotNil(groupReport)
    
    XCTAssertEqual(groupReport?.objectID, "1")
    XCTAssertEqual(groupReport?.type, "groupReports")
    
    XCTAssertNotNil(groupReport?.links)
    
    XCTAssertNotNil(groupReportAttributes)
    
    XCTAssertEqual(
      groupReportAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupReportAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupReportAttributes?.reason, .nsfw)
    XCTAssertEqual(groupReportAttributes?.status, .reported)
    XCTAssertNil(groupReportAttributes?.explanation)
    XCTAssertEqual(groupReportAttributes?.naughtyType, "Comment")
    XCTAssertEqual(groupReportAttributes?.naughtyID, 1)
  }
  
  func testGroupReportValidNilData() {
    groupReport = parseJson(validNilDataJSON)
    groupReportAttributes = groupReport?.attributes
    
    XCTAssertNotNil(groupReport)
    
    XCTAssertEqual(groupReport?.objectID, "1")
    XCTAssertEqual(groupReport?.type, "groupReports")
    
    XCTAssertNotNil(groupReport?.links)
    
    XCTAssertNotNil(groupReportAttributes)
    
    XCTAssertEqual(
      groupReportAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupReportAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupReportAttributes?.reason, .nsfw)
    XCTAssertEqual(groupReportAttributes?.status, .reported)
    XCTAssertNil(groupReportAttributes?.explanation)
    XCTAssertEqual(groupReportAttributes?.naughtyType, "Comment")
    XCTAssertEqual(groupReportAttributes?.naughtyID, 1)
  }
  
  func testGroupReportInvalidMissingData() {
    groupReport = parseJson(invalidMissingDataJSON)
    groupReportAttributes = groupReport?.attributes
    
    XCTAssertNotNil(groupReport)
    
    XCTAssertEqual(groupReport?.objectID, "1")
    XCTAssertEqual(groupReport?.type, "groupReports")
    
    XCTAssertNotNil(groupReport?.links)
    
    XCTAssertNil(groupReportAttributes)
  }
  
  func testGroupReportInvalidNilData() {
    groupReport = parseJson(invalidNilDataJSON)
    groupReportAttributes = groupReport?.attributes
    
    XCTAssertNotNil(groupReport)
    
    XCTAssertEqual(groupReport?.objectID, "1")
    XCTAssertEqual(groupReport?.type, "groupReports")
    
    XCTAssertNotNil(groupReport?.links)
    
    XCTAssertNil(groupReportAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Report? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Report.self, from: data!)
  }
}
