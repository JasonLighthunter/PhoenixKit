import XCTest
@testable import Groups

class GroupMemberRankEnumTests: XCTestCase {
  func testGroupMemberRankEnum() {
    XCTAssertEqual(MemberRank(rawValue: "pleb"), .pleb)
    XCTAssertEqual(MemberRank(rawValue: "mod"), .mod)
    XCTAssertEqual(MemberRank(rawValue: "admin"), .admin)
    XCTAssertNil(MemberRank(rawValue: "InvalidInput"))
  }
}
