import XCTest
@testable import Groups

class BanTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "1",
    "type": "groupBans",
    "links": [
      "self": "https://kitsu.io/api/edge/group-bans/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "notes": "testNotes",
      "notesFormatted": "testNotesFormatted"
    ]
  ]
  
  let validMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupBans",
    "links": [
      "self": "https://kitsu.io/api/edge/group-bans/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
    ]
  ]
  
  let validNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupBans",
    "links": [
      "self": "https://kitsu.io/api/edge/group-bans/1"
    ],
    "attributes": [
      "createdAt": "2015-02-17T21:16:53.207Z",
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "notes": nil,
      "notesFormatted": nil
    ]
  ]
  
  let invalidMissingDataJSON: [String : Any] = [
    "id": "1",
    "type": "groupBans",
    "links": [
      "self": "https://kitsu.io/api/edge/group-bans/1"
    ],
    "attributes": [
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "notes": "testNotes",
      "notesFormatted": "testNotesFormatted"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "1",
    "type": "groupBans",
    "links": [
      "self": "https://kitsu.io/api/edge/group-bans/1"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": "2017-10-26T20:51:24.215Z",
      "notes": "testNotes",
      "notesFormatted": "testNotesFormatted"
    ]
  ]
  
  var groupBan: GroupBan?
  var groupBanAttributes: GroupBanAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    groupBan = nil
    groupBanAttributes = nil
    
    super.tearDown()
  }
  
  func testGroupBanFullyFilled() {
    groupBan = parseJson(fullyFilledJSON)
    groupBanAttributes = groupBan?.attributes
    
    XCTAssertNotNil(groupBan)
    
    XCTAssertEqual(groupBan?.objectID, "1")
    XCTAssertEqual(groupBan?.type, "groupBans")
    
    XCTAssertNotNil(groupBan?.links)
    
    XCTAssertNotNil(groupBanAttributes)
    
    XCTAssertEqual(
      groupBanAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupBanAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertEqual(groupBanAttributes?.notes, "testNotes")
    XCTAssertEqual(groupBanAttributes?.notesFormatted, "testNotesFormatted")
  }
  
  func testGroupBanValidMissingData() {
    groupBan = parseJson(validMissingDataJSON)
    groupBanAttributes = groupBan?.attributes
    
    XCTAssertNotNil(groupBan)
    
    XCTAssertEqual(groupBan?.objectID, "1")
    XCTAssertEqual(groupBan?.type, "groupBans")
    
    XCTAssertNotNil(groupBan?.links)
    
    XCTAssertNotNil(groupBanAttributes)
    
    XCTAssertEqual(
      groupBanAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupBanAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertNil(groupBanAttributes?.notes)
    XCTAssertNil(groupBanAttributes?.notesFormatted)
  }
  
  func testGroupBanValidNilData() {
    groupBan = parseJson(validNilDataJSON)
    groupBanAttributes = groupBan?.attributes
    
    XCTAssertNotNil(groupBan)
    
    XCTAssertEqual(groupBan?.objectID, "1")
    XCTAssertEqual(groupBan?.type, "groupBans")
    
    XCTAssertNotNil(groupBan?.links)
    
    XCTAssertNotNil(groupBanAttributes)
    
    XCTAssertEqual(
      groupBanAttributes?.createdAt, dateFormatter.date(from: "2015-02-17T21:16:53.207Z")
    )
    XCTAssertEqual(
      groupBanAttributes?.updatedAt, dateFormatter.date(from: "2017-10-26T20:51:24.215Z")
    )
    XCTAssertNil(groupBanAttributes?.notes)
    XCTAssertNil(groupBanAttributes?.notesFormatted)
  }
  
  func testGroupBanInvalidMissingData() {
    groupBan = parseJson(invalidMissingDataJSON)
    groupBanAttributes = groupBan?.attributes
    
    XCTAssertNotNil(groupBan)
    
    XCTAssertEqual(groupBan?.objectID, "1")
    XCTAssertEqual(groupBan?.type, "groupBans")
    
    XCTAssertNotNil(groupBan?.links)
    
    XCTAssertNil(groupBanAttributes)
  }
  
  func testGroupBanInvalidNilData() {
    groupBan = parseJson(invalidNilDataJSON)
    groupBanAttributes = groupBan?.attributes
    
    XCTAssertNotNil(groupBan)
    
    XCTAssertEqual(groupBan?.objectID, "1")
    XCTAssertEqual(groupBan?.type, "groupBans")
    
    XCTAssertNotNil(groupBan?.links)
    
    XCTAssertNil(groupBanAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> GroupBan? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(GroupBan.self, from: data!)
  }
}
