import XCTest
@testable import Media

class StreamingLinkTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var streamingLink: StreamingLink?
  var streamingLinkAttributes: StreamingLinkAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    streamingLink = nil
    streamingLinkAttributes = nil

    super.tearDown()
  }

  func testStreamingLinkFullyFilled() {
    streamingLink = parseJson(StreamingLinkTestData.fullyFilledJSON)
    streamingLinkAttributes = streamingLink?.attributes

    XCTAssertNotNil(streamingLink)

    XCTAssertEqual(streamingLink?.objectID, "6")
    XCTAssertEqual(streamingLink?.type, "streamingLinks")

    XCTAssertNotNil(streamingLink?.links)

    XCTAssertNotNil(streamingLinkAttributes)

    XCTAssertEqual(
      streamingLinkAttributes?.createdAt, dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
    )
    XCTAssertEqual(
      streamingLinkAttributes?.updatedAt, dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
    )
    XCTAssertEqual(streamingLinkAttributes?.url, "http://example.com")
    XCTAssertEqual((streamingLinkAttributes?.subs)!, ["en"])
    XCTAssertEqual((streamingLinkAttributes?.dubs)!, ["ja"])
  }

  func testStreamingLinkValidMissingData() {
    streamingLink = parseJson(StreamingLinkTestData.validMissingDataJSON)
    streamingLinkAttributes = streamingLink?.attributes

    XCTAssertNotNil(streamingLink)

    XCTAssertEqual(streamingLink?.objectID, "6")
    XCTAssertEqual(streamingLink?.type, "streamingLinks")

    XCTAssertNotNil(streamingLink?.links)

    XCTAssertNotNil(streamingLinkAttributes)

    XCTAssertNil(streamingLinkAttributes?.createdAt)
    XCTAssertNil(streamingLinkAttributes?.updatedAt)
    XCTAssertEqual(streamingLinkAttributes?.url, "http://example.com")
    XCTAssertEqual((streamingLinkAttributes?.subs)!, ["en"])
    XCTAssertEqual((streamingLinkAttributes?.dubs)!, ["ja"])
  }

  func testStreamingLinkValidNilData() {
    streamingLink = parseJson(StreamingLinkTestData.validNilDataJSON)
    streamingLinkAttributes = streamingLink?.attributes

    XCTAssertNotNil(streamingLink)

    XCTAssertEqual(streamingLink?.objectID, "6")
    XCTAssertEqual(streamingLink?.type, "streamingLinks")

    XCTAssertNotNil(streamingLink?.links)

    XCTAssertNotNil(streamingLinkAttributes)

    XCTAssertNil(streamingLinkAttributes?.createdAt)
    XCTAssertNil(streamingLinkAttributes?.updatedAt)
    XCTAssertEqual(streamingLinkAttributes?.url, "http://example.com")
    XCTAssertEqual((streamingLinkAttributes?.subs)!, ["en"])
    XCTAssertEqual((streamingLinkAttributes?.dubs)!, ["ja"])
  }

  func testStreamingLinkInvalidMissingData() {
    streamingLink = parseJson(StreamingLinkTestData.invalidMissingDataJSON)
    streamingLinkAttributes = streamingLink?.attributes

    XCTAssertNotNil(streamingLink)

    XCTAssertEqual(streamingLink?.objectID, "6")
    XCTAssertEqual(streamingLink?.type, "streamingLinks")

    XCTAssertNotNil(streamingLink?.links)

    XCTAssertNil(streamingLinkAttributes)
  }

  func testStreamingLinkInvalidNilData() {
    streamingLink = parseJson(StreamingLinkTestData.invalidNilDataJSON)
    streamingLinkAttributes = streamingLink?.attributes

    XCTAssertNotNil(streamingLink)

    XCTAssertEqual(streamingLink?.objectID, "6")
    XCTAssertEqual(streamingLink?.type, "streamingLinks")

    XCTAssertNotNil(streamingLink?.links)

    XCTAssertNil(streamingLinkAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> StreamingLink? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(StreamingLink.self, from: data!)
  }
}
