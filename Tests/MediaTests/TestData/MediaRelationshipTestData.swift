class MediaRelationshipTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "mediaRelationships",
    "links": [
      "self": "https://kitsu.io/api/edge/media-relationships/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "role": "prequel"
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "mediaRelationships",
    "links": [
      "self": "https://kitsu.io/api/edge/media-relationships/6"
    ],
    "attributes": [
      "role": "prequel"
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "mediaRelationships",
    "links": [
      "self": "https://kitsu.io/api/edge/media-relationships/6"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "role": "prequel"
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "mediaRelationships",
    "links": [
      "self": "https://kitsu.io/api/edge/media-relationships/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z"
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "mediaRelationships",
    "links": [
      "self": "https://kitsu.io/api/edge/media-relationships/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "role": nil
    ]
  ]
}
