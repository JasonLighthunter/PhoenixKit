class EpisodeTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "episodes",
    "links": [
      "self": "https://kitsu.io/api/edge/episodes/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Episode 5"
      ],
      "canonicalTitle": "Episode 5",
      "seasonNumber": 1,
      "number": 5,
      "relativeNumber": 5,
      "synopsis": "testSynopsis",
      "airdate": "2018-01-23",
      "length": 45,
      "thumbnail": [
        "original": "https://media.kitsu.io/anime/poster_images/4444/original.jpg?1408452570",
        "meta": [
          "dimensions": [:]
        ]
      ]
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "episodes",
    "links": [
      "self": "https://kitsu.io/api/edge/episodes/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Episode 5"
      ],
      "canonicalTitle": "Episode 5"
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "episodes",
    "links": [
      "self": "https://kitsu.io/api/edge/episodes/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Episode 5"
      ],
      "canonicalTitle": "Episode 5",
      "seasonNumber": nil,
      "number": nil,
      "relativeNumber": nil,
      "synopsis": nil,
      "airdate": nil,
      "length": nil,
      "thumbnail": nil
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "episodes",
    "links": [
      "self": "https://kitsu.io/api/edge/episodes/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Episode 5"
      ],
      "canonicalTitle": "Episode 5",
      "seasonNumber": 1,
      "number": 5,
      "relativeNumber": 5,
      "synopsis": "testSynopsis",
      "airdate": "2018-01-23",
      "length": 45,
      "thumbnail": [
        "original": "https://media.kitsu.io/anime/poster_images/4444/original.jpg?1408452570",
        "meta": [
          "dimensions": [:]
        ]
      ]
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "episodes",
    "links": [
      "self": "https://kitsu.io/api/edge/episodes/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Episode 5"
      ],
      "canonicalTitle": nil,
      "seasonNumber": 1,
      "number": 5,
      "relativeNumber": 5,
      "synopsis": "testSynopsis",
      "airdate": "2018-01-23",
      "length": 45,
      "thumbnail": [
        "original": "https://media.kitsu.io/anime/poster_images/4444/original.jpg?1408452570",
        "meta": [
          "dimensions": [:]
        ]
      ]
    ]
  ]
}
