class ChapterTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "chapters",
    "links": [
      "self": "https://kitsu.io/api/edge/chapters/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Chapter 5"
      ],
      "canonicalTitle": "Chapter 5",
      "volumeNumber": 1,
      "number": 5,
      "synopsis": "testSynopsis",
      "published": "2018-01-23",
      "length": 45,
      "thumbnail": [
        "original": "https://media.kitsu.io/anime/poster_images/4444/original.jpg?1408452570",
        "meta": [
          "dimensions": [:]
        ]
      ]
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "chapters",
    "links": [
      "self": "https://kitsu.io/api/edge/chapters/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Chapter 5"
      ],
      "canonicalTitle": "Chapter 5",
      "number": 5
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "chapters",
    "links": [
      "self": "https://kitsu.io/api/edge/chapters/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Chapter 5"
      ],
      "canonicalTitle": "Chapter 5",
      "volumeNumber": nil,
      "number": 5,
      "synopsis": nil,
      "published": nil,
      "length": nil,
      "thumbnail": nil
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "chapters",
    "links": [
      "self": "https://kitsu.io/api/edge/chapters/6"
    ],
    "attributes": [
      "createdAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Chapter 5"
      ],
      "canonicalTitle": "Chapter 5",
      "volumeNumber": 1,
      "number": 5,
      "synopsis": "testSynopsis",
      "published": "2018-01-23",
      "length": 45,
      "thumbnail": [
        "original": "https://media.kitsu.io/anime/poster_images/4444/original.jpg?1408452570",
        "meta": [
          "dimensions": [:]
        ]
      ]
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "chapters",
    "links": [
      "self": "https://kitsu.io/api/edge/chapters/6"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": "2017-06-30T16:07:59.760Z",
      "titles": [
        "en_jp": "Chapter 5"
      ],
      "canonicalTitle": "Chapter 5",
      "volumeNumber": 1,
      "number": 5,
      "synopsis": "testSynopsis",
      "published": "2018-01-23",
      "length": 45,
      "thumbnail": [
        "original": "https://media.kitsu.io/anime/poster_images/4444/original.jpg?1408452570",
        "meta": [
          "dimensions": [:]
        ]
      ]
    ]
  ]
}
