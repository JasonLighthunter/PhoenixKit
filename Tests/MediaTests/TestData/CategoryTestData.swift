class CategoryTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "categories",
    "links": [
      "self": "https://kitsu.io/api/edge/categories/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "title": "All Girls School",
      "description": "This school is only for female students.",
      "totalMediaCount": 72,
      "slug": "all-girls-school",
      "nsfw": false,
      "childCount": 0,
      "image": [
        "tiny": "https://media.kitsu.io/categories/images/6/tiny.jpg?1496212709",
        "original": "https://media.kitsu.io/categories/images/6/original.jpg?1496212709",
        "meta": [
          "dimensions": [
            "tiny": [
              "width": nil,
              "height": nil
            ]
          ]
        ]
      ]
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "categories",
    "links": [
      "self": "https://kitsu.io/api/edge/categories/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "title": "All Girls School",
      "slug": "all-girls-school",
      "nsfw": false,
      "childCount": 0
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "categories",
    "links": [
      "self": "https://kitsu.io/api/edge/categories/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "title": "All Girls School",
      "description": nil,
      "totalMediaCount": nil,
      "slug": "all-girls-school",
      "nsfw": false,
      "childCount": 0,
      "image": nil
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "categories",
    "links": [
      "self": "https://kitsu.io/api/edge/categories/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "description": "This school is only for female students",
      "totalMediaCount": 72,
      "slug": "all-girls-school",
      "nsfw": false,
      "childCount": 0,
      "image": [
        "tiny": "https://media.kitsu.io/categories/images/6/tiny.jpg?1496212709",
        "original": "https://media.kitsu.io/categories/images/6/original.jpg?1496212709",
        "meta": [
          "dimensions": [
            "tiny": [
              "width": nil,
              "height": nil
            ]
          ]
        ]
      ]
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "categories",
    "links": [
      "self": "https://kitsu.io/api/edge/categories/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "title": "All Girls School",
      "description": "This school is only for female students.",
      "totalMediaCount": 72,
      "slug": nil,
      "nsfw": false,
      "childCount": 0,
      "image": [
        "tiny": "https://media.kitsu.io/categories/images/6/tiny.jpg?1496212709",
        "original": "https://media.kitsu.io/categories/images/6/original.jpg?1496212709",
        "meta": [
          "dimensions": [
            "tiny": [
              "width": nil,
              "height": nil
            ]
          ]
        ]
      ]
    ]
  ]
}
