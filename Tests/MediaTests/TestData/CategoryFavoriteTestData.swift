class CategoryFavoriteTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "categoryFavorites",
    "links": [
      "self": "https://kitsu.io/api/edge/category-favorites/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z"
    ]
  ]

  static let validMissingDataJSON: [String : Any] = [
    "id": "6",
    "type": "categoryFavorites",
    "links": [
      "self": "https://kitsu.io/api/edge/category-favorites/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
    ]
  ]

  static let validNilDataJSON: [String : Any?] = [
    "id": "6",
    "type": "categoryFavorites",
    "links": [
      "self": "https://kitsu.io/api/edge/category-favorites/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "categoryFavorites",
    "links": [
      "self": "https://kitsu.io/api/edge/category-favorites/6"
    ],
    "attributes": [
      "updatedAt": "2017-05-31T06:39:36.788Z"
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "categoryFavorites",
    "links": [
      "self": "https://kitsu.io/api/edge/category-favorites/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": nil
    ]
  ]
}
