class StreamingLinkTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "streamingLinks",
    "links": [
      "self": "https://kitsu.io/api/edge/streaming-links/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "url": "http://example.com",
      "subs": [
        "en"
      ],
      "dubs": [
        "ja"
      ]
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "streamingLinks",
    "links": [
      "self": "https://kitsu.io/api/edge/streaming-links/6"
    ],
    "attributes": [
      "url": "http://example.com",
      "subs": [
        "en"
      ],
      "dubs": [
        "ja"
      ]
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "streamingLinks",
    "links": [
      "self": "https://kitsu.io/api/edge/streaming-links/6"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "url": "http://example.com",
      "subs": [
        "en"
      ],
      "dubs": [
        "ja"
      ]
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "streamingLinks",
    "links": [
      "self": "https://kitsu.io/api/edge/streaming-links/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "url": "http://www.hulu.com/hacklegend-of-the-twilight",
      "dubs": [
        "ja"
      ]
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "streamingLinks",
    "links": [
      "self": "https://kitsu.io/api/edge/streaming-links/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "url": nil,
      "subs": [
        "en"
      ],
      "dubs": [
        "ja"
      ]
    ]
  ]
}
