class MappingTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "mappings",
    "links": [
      "self": "https://kitsu.io/api/edge/mappings/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "externalSite": "myanimelist/anime",
      "externalId": "4718"
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "mappings",
    "links": [
      "self": "https://kitsu.io/api/edge/mappings/6"
    ],
    "attributes": [
      "externalSite": "myanimelist/anime",
      "externalId": "4718"
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "mappings",
    "links": [
      "self": "https://kitsu.io/api/edge/mappings/6"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "externalSite": "myanimelist/anime",
      "externalId": "4718"
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "mappings",
    "links": [
      "self": "https://kitsu.io/api/edge/mappings/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "externalId": "4718"
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "mappings",
    "links": [
      "self": "https://kitsu.io/api/edge/mappings/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "externalSite": "myanimelist/anime",
      "externalId": nil
    ]
  ]
}
