class StreamerTestData {
  static let fullyFilledJSON: [String: Any] = [
    "id": "6",
    "type": "streamers",
    "links": [
      "self": "https://kitsu.io/api/edge/streamers/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "siteName": "Hulu",
      "streamingLinksCount": 760,
      "logo": "logo"
    ]
  ]

  static let validMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "streamers",
    "links": [
      "self": "https://kitsu.io/api/edge/streamers/6"
    ],
    "attributes": [
      "siteName": "Hulu",
      "streamingLinksCount": 760
    ]
  ]

  static let validNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "streamers",
    "links": [
      "self": "https://kitsu.io/api/edge/streamers/6"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": nil,
      "siteName": "Hulu",
      "streamingLinksCount": 760,
      "logo": nil
    ]
  ]

  static let invalidMissingDataJSON: [String: Any] = [
    "id": "6",
    "type": "streamers",
    "links": [
      "self": "https://kitsu.io/api/edge/streamers/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "siteName": "Hulu",
      "logo": "logo"
    ]
  ]

  static let invalidNilDataJSON: [String: Any?] = [
    "id": "6",
    "type": "streamers",
    "links": [
      "self": "https://kitsu.io/api/edge/streamers/6"
    ],
    "attributes": [
      "createdAt": "2017-05-31T06:38:29.320Z",
      "updatedAt": "2017-05-31T06:39:36.788Z",
      "siteName": "Hulu",
      "streamingLinksCount": nil,
      "logo": "logo"
    ]
  ]
}
