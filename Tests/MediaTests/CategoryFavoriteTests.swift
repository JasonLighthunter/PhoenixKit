import XCTest
@testable import Media

class CategoryFavoriteTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var favorite: CategoryFavorite?
  var favoriteAttributes: CategoryFavoriteAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    favorite = nil
    favoriteAttributes = nil

    super.tearDown()
  }

  func testCategoryFavoriteFullyFilled() {
    favorite = parseJson(CategoryFavoriteTestData.fullyFilledJSON)
    favoriteAttributes = favorite?.attributes

    XCTAssertNotNil(favorite)

    XCTAssertEqual(favorite?.objectID, "6")
    XCTAssertEqual(favorite?.type, "categoryFavorites")

    XCTAssertNotNil(favorite?.links)

    XCTAssertNotNil(favoriteAttributes)

    let createdAtString = "2017-05-31T06:38:29.320Z"
    XCTAssertEqual(favoriteAttributes?.createdAt, dateFormatter.date(from: createdAtString))
    let updatedAtString = "2017-05-31T06:39:36.788Z"
    XCTAssertEqual(favoriteAttributes?.updatedAt, dateFormatter.date(from: updatedAtString))
  }

//  func testCategoryFavoriteValidMissingData() {
//    categoryFavorite = parseJson(CategoryFavoriteTestData.validMissingDataJSON)
//    categoryFavoriteAttributes = categoryFavorite?.attributes
//
//    XCTAssertNotNil(categoryFavorite)
//
//    XCTAssertEqual(categoryFavorite?.objectID, "6")
//    XCTAssertEqual(categoryFavorite?.type, "categories")
//
//    XCTAssertNotNil(categoryFavorite?.links)
//
//    XCTAssertNotNil(categoryFavoriteAttributes)
//
//    XCTAssertEqual(
//      categoryFavoriteAttributes?.createdAt,
//      dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
//    )
//    XCTAssertEqual(
//      categoryFavoriteAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
//    )
//  }

//  func testCategoryFavoriteValidNilData() {
//    categoryFavorite = parseJson(CategoryFavoriteTestData.validNilDataJSON)
//    categoryFavoriteAttributes = categoryFavorite?.attributes
//
//    XCTAssertNotNil(categoryFavorite)
//
//    XCTAssertEqual(categoryFavorite?.objectID, "6")
//    XCTAssertEqual(categoryFavorite?.type, "categories")
//
//    XCTAssertNotNil(categoryFavorite?.links)
//
//    XCTAssertNotNil(categoryFavoriteAttributes)
//
//    XCTAssertEqual(
//      categoryFavoriteAttributes?.createdAt,
//      dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
//    )
//    XCTAssertEqual(
//      categoryFavoriteAttributes?.updatedAt,
//      dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
//    )
//  }

  func testCategoryFavoriteInvalidMissingData() {
    favorite = parseJson(CategoryFavoriteTestData.invalidMissingDataJSON)
    favoriteAttributes = favorite?.attributes

    XCTAssertNotNil(favorite)

    XCTAssertEqual(favorite?.objectID, "6")
    XCTAssertEqual(favorite?.type, "categoryFavorites")

    XCTAssertNotNil(favorite?.links)

    XCTAssertNil(favoriteAttributes)
  }

  func testCategoryFavoriteInvalidNilData() {
    favorite = parseJson(CategoryFavoriteTestData.invalidNilDataJSON)
    favoriteAttributes = favorite?.attributes

    XCTAssertNotNil(favorite)

    XCTAssertEqual(favorite?.objectID, "6")
    XCTAssertEqual(favorite?.type, "categoryFavorites")

    XCTAssertNotNil(favorite?.links)

    XCTAssertNil(favoriteAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> CategoryFavorite? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(CategoryFavorite.self, from: data!)
  }
}
