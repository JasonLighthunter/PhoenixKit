import XCTest
@testable import Media

class MangaTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let isoDateFormatter = DateFormatter()
  let shortDateFormatter = DateFormatter()

  var manga: Manga?
  var mangaAttributes: MangaAttributes?

  override func setUp() {
    isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    shortDateFormatter.dateFormat = "yyyy-MM-dd"
    JSONdecoder.dateDecodingStrategy = .formatted(isoDateFormatter)
  }

  override func tearDown() {
    manga = nil
    mangaAttributes = nil

    super.tearDown()
  }

  func testMangaFullyFilled() {
    manga = parseJson(MangaTestData.fullyFilledJSON)
    mangaAttributes = manga?.attributes

    XCTAssertNotNil(manga)
    XCTAssertEqual(manga?.objectID, "4")
    XCTAssertEqual(manga?.type, "manga")
    XCTAssertNotNil(manga?.links)

    XCTAssertNotNil(mangaAttributes)
    XCTAssertEqual(
      mangaAttributes?.createdAt, isoDateFormatter.date(from: "2013-12-18T13:48:24.164Z")
    )
    XCTAssertEqual(
      mangaAttributes?.updatedAt, isoDateFormatter.date(from: "2018-01-08T12:01:41.130Z")
    )
    XCTAssertEqual(mangaAttributes?.slug, "guardian-dog")
    XCTAssertEqual(mangaAttributes?.synopsis, "Gengo Kurosaka leads a normal life.")
    XCTAssertNotNil(mangaAttributes?.titles)
    XCTAssertEqual(mangaAttributes?.canonicalTitle, "Guardian Dog")
    XCTAssertEqual((mangaAttributes?.abbreviatedTitles)!, ["abbreviated1"])
    XCTAssertEqual(mangaAttributes?.averageRating, "34.65")
    XCTAssertEqual((mangaAttributes?.ratingFrequencies)!, ["2": "0", "3": "0"])
    XCTAssertEqual(mangaAttributes?.userCount, 47)
    XCTAssertEqual(mangaAttributes?.favoritesCount, 1)
    XCTAssertEqual(mangaAttributes?.startDate, shortDateFormatter.date(from: "2005-01-01"))
    XCTAssertEqual(mangaAttributes?.endDate, shortDateFormatter.date(from: "2005-01-01"))
    XCTAssertEqual(mangaAttributes?.popularityRank, 4546)
    XCTAssertEqual(mangaAttributes?.ratingRank, 34)
    XCTAssertEqual(mangaAttributes?.ageRating, .generalAudiences)
    XCTAssertEqual(mangaAttributes?.ageRatingGuide, "Horror")
    XCTAssertEqual(mangaAttributes?.subtype, .manga)
    XCTAssertEqual(mangaAttributes?.status, .current)
    XCTAssertEqual(mangaAttributes?.toBeAnnounced, "Winter 2020")
    XCTAssertNotNil(mangaAttributes?.posterImage)
    XCTAssertNotNil(mangaAttributes?.coverImage)
    XCTAssertEqual(mangaAttributes?.chapterCount, 22)
    XCTAssertEqual(mangaAttributes?.volumeCount, 4)
    XCTAssertEqual(mangaAttributes?.serialization, "Comic Rush")
  }

  func testMangaValidMissingData() {
    manga = parseJson(MangaTestData.validMissingDataJSON)
    mangaAttributes = manga?.attributes

    XCTAssertNotNil(manga)
    XCTAssertEqual(manga?.objectID, "4")
    XCTAssertEqual(manga?.type, "manga")
    XCTAssertNotNil(manga?.links)

    XCTAssertNotNil(mangaAttributes)
    XCTAssertEqual(
      mangaAttributes?.createdAt, isoDateFormatter.date(from: "2013-12-18T13:48:24.164Z")
    )
    XCTAssertEqual(
      mangaAttributes?.updatedAt, isoDateFormatter.date(from: "2018-01-08T12:01:41.130Z")
    )
    XCTAssertNil(mangaAttributes?.slug)
    XCTAssertNil(mangaAttributes?.synopsis)
    XCTAssertNotNil(mangaAttributes?.titles)
    XCTAssertEqual(mangaAttributes?.canonicalTitle, "Guardian Dog")
    XCTAssertNil(mangaAttributes?.abbreviatedTitles)
    XCTAssertNil(mangaAttributes?.averageRating)
    XCTAssertEqual((mangaAttributes?.ratingFrequencies)!, ["2": "0", "3": "0"])
    XCTAssertEqual(mangaAttributes?.userCount, 47)
    XCTAssertEqual(mangaAttributes?.favoritesCount, 1)
    XCTAssertNil(mangaAttributes?.startDate)
    XCTAssertNil(mangaAttributes?.endDate)
    XCTAssertNil(mangaAttributes?.popularityRank)
    XCTAssertNil(mangaAttributes?.ratingRank)
    XCTAssertNil(mangaAttributes?.ageRating)
    XCTAssertNil(mangaAttributes?.ageRatingGuide)
    XCTAssertEqual(mangaAttributes?.subtype, MangaType.manga)
    XCTAssertEqual(mangaAttributes?.status, ReleaseStatus.current)
    XCTAssertNil(mangaAttributes?.toBeAnnounced)
    XCTAssertNil(mangaAttributes?.posterImage)
    XCTAssertNil(mangaAttributes?.coverImage)
    XCTAssertNil(mangaAttributes?.chapterCount)
    XCTAssertNil(mangaAttributes?.volumeCount)
    XCTAssertNil(mangaAttributes?.serialization)
  }

  func testMangaValidNilData() {
    manga = parseJson(MangaTestData.validNilDataJSON)
    mangaAttributes = manga?.attributes

    XCTAssertNotNil(manga)
    XCTAssertEqual(manga?.objectID, "4")
    XCTAssertEqual(manga?.type, "manga")
    XCTAssertNotNil(manga?.links)

    XCTAssertNotNil(mangaAttributes)
    XCTAssertEqual(
      mangaAttributes?.createdAt, isoDateFormatter.date(from: "2013-12-18T13:48:24.164Z")
    )
    XCTAssertEqual(
      mangaAttributes?.updatedAt, isoDateFormatter.date(from: "2018-01-08T12:01:41.130Z")
    )
    XCTAssertNil(mangaAttributes?.slug)
    XCTAssertNil(mangaAttributes?.synopsis)
    XCTAssertNotNil(mangaAttributes?.titles)
    XCTAssertEqual(mangaAttributes?.canonicalTitle, "Guardian Dog")
    XCTAssertNil(mangaAttributes?.abbreviatedTitles)
    XCTAssertNil(mangaAttributes?.averageRating)
    XCTAssertEqual((mangaAttributes?.ratingFrequencies)!, ["2": "0", "3": "0"])
    XCTAssertEqual(mangaAttributes?.userCount, 47)
    XCTAssertEqual(mangaAttributes?.favoritesCount, 1)
    XCTAssertNil(mangaAttributes?.startDate)
    XCTAssertNil(mangaAttributes?.endDate)
    XCTAssertNil(mangaAttributes?.popularityRank)
    XCTAssertNil(mangaAttributes?.ratingRank)
    XCTAssertNil(mangaAttributes?.ageRating)
    XCTAssertNil(mangaAttributes?.ageRatingGuide)
    XCTAssertEqual(mangaAttributes?.subtype, .manga)
    XCTAssertEqual(mangaAttributes?.status, .current)
    XCTAssertNil(mangaAttributes?.toBeAnnounced)
    XCTAssertNil(mangaAttributes?.posterImage)
    XCTAssertNil(mangaAttributes?.coverImage)
    XCTAssertNil(mangaAttributes?.chapterCount)
    XCTAssertNil(mangaAttributes?.volumeCount)
    XCTAssertNil(mangaAttributes?.serialization)
  }

  func testMangaInvalidMissingData() {
    manga = parseJson(MangaTestData.invalidMissingDataJSON)
    mangaAttributes = manga?.attributes

    XCTAssertNotNil(manga)
    XCTAssertEqual(manga?.objectID, "4")
    XCTAssertEqual(manga?.type, "manga")
    XCTAssertNotNil(manga?.links)

    XCTAssertNil(mangaAttributes)
  }

  func testMangaInvalidNilData() {
    manga = parseJson(MangaTestData.invalidNilDataJSON)
    mangaAttributes = manga?.attributes

    XCTAssertNotNil(manga)
    XCTAssertEqual(manga?.objectID, "4")
    XCTAssertEqual(manga?.type, "manga")
    XCTAssertNotNil(manga?.links)

    XCTAssertNil(mangaAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Manga? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Manga.self, from: data!)
  }
}
