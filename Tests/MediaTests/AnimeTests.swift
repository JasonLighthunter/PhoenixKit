import XCTest
@testable import Media

class AnimeTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let isoDateFormatter = DateFormatter()
  let shortDateFormatter = DateFormatter()

  var anime: Anime?
  var animeAttributes: AnimeAttributes?

  override func setUp() {
    isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    shortDateFormatter.dateFormat = "yyyy-MM-dd"
    JSONdecoder.dateDecodingStrategy = .formatted(isoDateFormatter)
  }

  override func tearDown() {
    anime = nil
    animeAttributes = nil

    super.tearDown()
  }

  func testAnimeFullyFilled() {
    anime = parseJson(AnimeTestData.fullyFilledJSON)
    animeAttributes = anime?.attributes

    XCTAssertNotNil(anime)
    XCTAssertEqual(anime?.objectID, "4")
    XCTAssertEqual(anime?.type, "anime")
    XCTAssertNotNil(anime?.links)

    XCTAssertNotNil(animeAttributes)

    XCTAssertEqual(
      animeAttributes?.createdAt, isoDateFormatter.date(from: "2013-02-20T17:08:20.229Z")
    )
    XCTAssertEqual(
      animeAttributes?.updatedAt, isoDateFormatter.date(from: "2018-01-07T18:00:14.142Z")
    )
    XCTAssertEqual(animeAttributes?.slug, "yurumates")
    XCTAssertEqual(animeAttributes?.synopsis, "testSynopsis")
    XCTAssertNotNil(animeAttributes?.titles)
    XCTAssertEqual(animeAttributes?.canonicalTitle, "Yurumates")
    XCTAssertEqual((animeAttributes?.abbreviatedTitles)!, ["test"])
    XCTAssertEqual(animeAttributes?.averageRating, "67.0")
    XCTAssertEqual((animeAttributes?.ratingFrequencies)!, ["2": "1", "3": "1"])
    XCTAssertEqual(animeAttributes?.userCount, 504)
    XCTAssertEqual(animeAttributes?.favoritesCount, 2)
    XCTAssertEqual(animeAttributes?.startDate, shortDateFormatter.date(from: "2009-04-24"))
    XCTAssertEqual(animeAttributes?.endDate, shortDateFormatter.date(from: "2009-04-24"))
    XCTAssertEqual(animeAttributes?.popularityRank, 3872)
    XCTAssertEqual(animeAttributes?.ratingRank, 4339)
    XCTAssertEqual(animeAttributes?.ageRating, .parentalGuidance)
    XCTAssertEqual(animeAttributes?.ageRatingGuide, "Teens 13 or older")
    XCTAssertEqual(animeAttributes?.subtype, .OVA)
    XCTAssertEqual(animeAttributes?.status, .finished)
    XCTAssertEqual(animeAttributes?.toBeAnnounced, "winter 2020")
    XCTAssertNotNil(animeAttributes?.posterImage)
    XCTAssertNotNil(animeAttributes?.coverImage)
    XCTAssertEqual(animeAttributes?.episodeCount, 1)
    XCTAssertEqual(animeAttributes?.episodeLength, 37)
    XCTAssertEqual(animeAttributes?.youtubeVideoID, "youtubeq1w2")
    XCTAssertFalse((animeAttributes?.isNSFW)!)
  }

  func testAnimeValidMissingData() {
    anime = parseJson(AnimeTestData.validMissingDataJSON)
    animeAttributes = anime?.attributes

    XCTAssertNotNil(anime)
    XCTAssertEqual(anime?.objectID, "4")
    XCTAssertEqual(anime?.type, "anime")
    XCTAssertNotNil(anime?.links)

    XCTAssertNotNil(animeAttributes)

    let createdAtString = "2013-02-20T17:08:20.229Z"
    XCTAssertEqual(animeAttributes?.createdAt, isoDateFormatter.date(from: createdAtString))
    let updatedAtString = "2018-01-07T18:00:14.142Z"
    XCTAssertEqual(animeAttributes?.updatedAt, isoDateFormatter.date(from: updatedAtString))
    XCTAssertNil(animeAttributes?.slug)
    XCTAssertEqual(animeAttributes?.synopsis, "testSynopsis")
    XCTAssertNotNil(animeAttributes?.titles)
    XCTAssertEqual(animeAttributes?.canonicalTitle, "Yurumates")
    XCTAssertNil(animeAttributes?.abbreviatedTitles)
    XCTAssertNil(animeAttributes?.averageRating)
    XCTAssertEqual((animeAttributes?.ratingFrequencies)!, [:])
    XCTAssertEqual(animeAttributes?.userCount, 504)
    XCTAssertEqual(animeAttributes?.favoritesCount, 2)
    XCTAssertNil(animeAttributes?.startDate)
    XCTAssertNil(animeAttributes?.endDate)
    XCTAssertNil(animeAttributes?.popularityRank)
    XCTAssertNil(animeAttributes?.ratingRank)
    XCTAssertNil(animeAttributes?.ageRating)
    XCTAssertNil(animeAttributes?.ageRatingGuide)
    XCTAssertEqual(animeAttributes?.subtype, .OVA)
    XCTAssertEqual(animeAttributes?.status, .finished)
    XCTAssertNil(animeAttributes?.toBeAnnounced)
    XCTAssertNil(animeAttributes?.posterImage)
    XCTAssertNil(animeAttributes?.coverImage)
    XCTAssertNil(animeAttributes?.episodeCount)
    XCTAssertNil(animeAttributes?.episodeLength)
    XCTAssertNil(animeAttributes?.youtubeVideoID)
    XCTAssertFalse((animeAttributes?.isNSFW)!)
  }

  func testAnimeValidNilData() {
    anime = parseJson(AnimeTestData.validNilDataJSON)
    animeAttributes = anime?.attributes

    XCTAssertNotNil(anime)
    XCTAssertEqual(anime?.objectID, "4")
    XCTAssertEqual(anime?.type, "anime")
    XCTAssertNotNil(anime?.links)

    XCTAssertNotNil(animeAttributes)

    XCTAssertEqual(
      animeAttributes?.createdAt, isoDateFormatter.date(from: "2013-02-20T17:08:20.229Z")
    )
    XCTAssertEqual(
      animeAttributes?.updatedAt, isoDateFormatter.date(from: "2018-01-07T18:00:14.142Z")
    )
    XCTAssertNil(animeAttributes?.slug)
    XCTAssertEqual(animeAttributes?.synopsis, "testSynopsis")
    XCTAssertNotNil(animeAttributes?.titles)
    XCTAssertEqual(animeAttributes?.canonicalTitle, "Yurumates")
    XCTAssertNil(animeAttributes?.abbreviatedTitles)
    XCTAssertNil(animeAttributes?.averageRating)
    XCTAssertEqual((animeAttributes?.ratingFrequencies)!, [:])
    XCTAssertEqual(animeAttributes?.userCount, 504)
    XCTAssertEqual(animeAttributes?.favoritesCount, 2)
    XCTAssertNil(animeAttributes?.startDate)
    XCTAssertNil(animeAttributes?.endDate)
    XCTAssertNil(animeAttributes?.popularityRank)
    XCTAssertNil(animeAttributes?.ratingRank)
    XCTAssertNil(animeAttributes?.ageRating)
    XCTAssertNil(animeAttributes?.ageRatingGuide)
    XCTAssertEqual(animeAttributes?.subtype, AnimeType.OVA)
    XCTAssertEqual(animeAttributes?.status, ReleaseStatus.finished)
    XCTAssertNil(animeAttributes?.toBeAnnounced)
    XCTAssertNil(animeAttributes?.posterImage)
    XCTAssertNil(animeAttributes?.coverImage)
    XCTAssertNil(animeAttributes?.episodeCount)
    XCTAssertNil(animeAttributes?.episodeLength)
    XCTAssertNil(animeAttributes?.youtubeVideoID)
    XCTAssertFalse((animeAttributes?.isNSFW)!)
  }

  func testAnimeInvalidMissingData() {
    anime = parseJson(AnimeTestData.invalidMissingDataJSON)
    animeAttributes = anime?.attributes

    XCTAssertNotNil(anime)
    XCTAssertEqual(anime?.objectID, "4")
    XCTAssertEqual(anime?.type, "anime")
    XCTAssertNotNil(anime?.links)

    XCTAssertNil(animeAttributes)
  }

  func testAnimeInvalidNilData() {
    anime = parseJson(AnimeTestData.invalidNilDataJSON)
    animeAttributes = anime?.attributes

    XCTAssertNotNil(anime)
    XCTAssertEqual(anime?.objectID, "4")
    XCTAssertEqual(anime?.type, "anime")
    XCTAssertNotNil(anime?.links)

    XCTAssertNil(animeAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Anime? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Anime.self, from: data!)
  }
}
