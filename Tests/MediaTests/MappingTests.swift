import XCTest
@testable import Media

class MappingTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var mapping: Mapping?
  var mappingAttributes: MappingAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    mapping = nil
    mappingAttributes = nil

    super.tearDown()
  }

  func testMappingFullyFilled() {
    mapping = parseJson(MappingTestData.fullyFilledJSON)
    mappingAttributes = mapping?.attributes

    XCTAssertNotNil(mapping)

    XCTAssertEqual(mapping?.objectID, "6")
    XCTAssertEqual(mapping?.type, "mappings")

    XCTAssertNotNil(mapping?.links)

    XCTAssertNotNil(mappingAttributes)

    XCTAssertEqual(
      mappingAttributes?.createdAt, dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
    )
    XCTAssertEqual(
      mappingAttributes?.updatedAt, dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
    )
    XCTAssertEqual(mappingAttributes?.externalSite, .myAnimeListAnime)
    XCTAssertEqual(mappingAttributes?.externalID, "4718")
  }

  func testMappingValidMissingData() {
    mapping = parseJson(MappingTestData.validMissingDataJSON)
    mappingAttributes = mapping?.attributes

    XCTAssertNotNil(mapping)

    XCTAssertEqual(mapping?.objectID, "6")
    XCTAssertEqual(mapping?.type, "mappings")

    XCTAssertNotNil(mapping?.links)

    XCTAssertNotNil(mappingAttributes)

    XCTAssertNil(mappingAttributes?.createdAt)
    XCTAssertNil(mappingAttributes?.updatedAt)
    XCTAssertEqual(mappingAttributes?.externalSite, .myAnimeListAnime)
    XCTAssertEqual(mappingAttributes?.externalID, "4718")
  }

  func testMappingValidNilData() {
    mapping = parseJson(MappingTestData.validNilDataJSON)
    mappingAttributes = mapping?.attributes

    XCTAssertNotNil(mapping)

    XCTAssertEqual(mapping?.objectID, "6")
    XCTAssertEqual(mapping?.type, "mappings")

    XCTAssertNotNil(mapping?.links)

    XCTAssertNotNil(mappingAttributes)

    XCTAssertNil(mappingAttributes?.createdAt)
    XCTAssertNil(mappingAttributes?.updatedAt)
    XCTAssertEqual(mappingAttributes?.externalSite, .myAnimeListAnime)
    XCTAssertEqual(mappingAttributes?.externalID, "4718")
  }

  func testMappingInvalidMissingData() {
    mapping = parseJson(MappingTestData.invalidMissingDataJSON)
    mappingAttributes = mapping?.attributes

    XCTAssertNotNil(mapping)

    XCTAssertEqual(mapping?.objectID, "6")
    XCTAssertEqual(mapping?.type, "mappings")

    XCTAssertNotNil(mapping?.links)

    XCTAssertNil(mappingAttributes)
  }

  func testMappingInvalidNilData() {
    mapping = parseJson(MappingTestData.invalidNilDataJSON)
    mappingAttributes = mapping?.attributes

    XCTAssertNotNil(mapping)

    XCTAssertEqual(mapping?.objectID, "6")
    XCTAssertEqual(mapping?.type, "mappings")

    XCTAssertNotNil(mapping?.links)

    XCTAssertNil(mappingAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Mapping? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Mapping.self, from: data!)
  }
}
