import XCTest
@testable import Media

class ReleaseStatusTests: XCTestCase {
  func testReleaseStatus() {
    XCTAssertEqual(ReleaseStatus(rawValue: "tba"), .toBeAnnounced)
    XCTAssertEqual(ReleaseStatus(rawValue: "finished"), .finished)
    XCTAssertEqual(ReleaseStatus(rawValue: "current"), .current)
    XCTAssertEqual(ReleaseStatus(rawValue: "upcoming"), .upcoming)
    XCTAssertEqual(ReleaseStatus(rawValue: "unreleased"), .unreleased)
    XCTAssertNil(ReleaseStatus(rawValue: "InvalidInput"))
  }
}
