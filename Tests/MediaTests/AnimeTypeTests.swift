import XCTest
@testable import Media

class AnimeTypeTests: XCTestCase {
  func testAnimeType() {
    XCTAssertEqual(AnimeType(rawValue: "TV"), .television)
    XCTAssertEqual(AnimeType(rawValue: "special"), .special)
    XCTAssertEqual(AnimeType(rawValue: "OVA"), .OVA)
    XCTAssertEqual(AnimeType(rawValue: "ONA"), .ONA)
    XCTAssertEqual(AnimeType(rawValue: "movie"), .movie)
    XCTAssertEqual(AnimeType(rawValue: "music"), .music)
    XCTAssertNil(AnimeType(rawValue: "InvalidInput"))
  }
}
