import XCTest
@testable import Media

class MediaRelationshipTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var mediaRelationship: MediaRelationship?
  var mediaRelationshipAttributes: MediaRelationshipAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    mediaRelationship = nil
    mediaRelationshipAttributes = nil

    super.tearDown()
  }

  func testMediaRelationshipFullyFilled() {
    mediaRelationship = parseJson(MediaRelationshipTestData.fullyFilledJSON)
    mediaRelationshipAttributes = mediaRelationship?.attributes

    XCTAssertNotNil(mediaRelationship)

    XCTAssertEqual(mediaRelationship?.objectID, "6")
    XCTAssertEqual(mediaRelationship?.type, "mediaRelationships")

    XCTAssertNotNil(mediaRelationship?.links)

    XCTAssertNotNil(mediaRelationshipAttributes)

    XCTAssertEqual(
      mediaRelationshipAttributes?.createdAt,
      dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
    )
    XCTAssertEqual(
      mediaRelationshipAttributes?.updatedAt,
      dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
    )
    XCTAssertEqual(mediaRelationshipAttributes?.role, .prequel)
  }

  func testMediaRelationshipValidMissingData() {
    mediaRelationship = parseJson(MediaRelationshipTestData.validMissingDataJSON)
    mediaRelationshipAttributes = mediaRelationship?.attributes

    XCTAssertNotNil(mediaRelationship)

    XCTAssertEqual(mediaRelationship?.objectID, "6")
    XCTAssertEqual(mediaRelationship?.type, "mediaRelationships")

    XCTAssertNotNil(mediaRelationship?.links)

    XCTAssertNotNil(mediaRelationshipAttributes)

    XCTAssertNil(mediaRelationshipAttributes?.createdAt)
    XCTAssertNil(mediaRelationshipAttributes?.updatedAt)
    XCTAssertEqual(mediaRelationshipAttributes?.role, MediaRelationshipRole.prequel)
  }

  func testMediaRelationshipValidNilData() {
    mediaRelationship = parseJson(MediaRelationshipTestData.validNilDataJSON)
    mediaRelationshipAttributes = mediaRelationship?.attributes

    XCTAssertNotNil(mediaRelationship)

    XCTAssertEqual(mediaRelationship?.objectID, "6")
    XCTAssertEqual(mediaRelationship?.type, "mediaRelationships")

    XCTAssertNotNil(mediaRelationship?.links)

    XCTAssertNotNil(mediaRelationshipAttributes)

    XCTAssertNil(mediaRelationshipAttributes?.createdAt)
    XCTAssertNil(mediaRelationshipAttributes?.updatedAt)
    XCTAssertEqual(mediaRelationshipAttributes?.role, MediaRelationshipRole.prequel)
  }

  func testMediaRelationshipInvalidMissingData() {
    mediaRelationship = parseJson(MediaRelationshipTestData.invalidMissingDataJSON)
    mediaRelationshipAttributes = mediaRelationship?.attributes

    XCTAssertNotNil(mediaRelationship)

    XCTAssertEqual(mediaRelationship?.objectID, "6")
    XCTAssertEqual(mediaRelationship?.type, "mediaRelationships")

    XCTAssertNotNil(mediaRelationship?.links)

    XCTAssertNil(mediaRelationshipAttributes)
  }

  func testMediaRelationshipInvalidNilData() {
    mediaRelationship = parseJson(MediaRelationshipTestData.invalidNilDataJSON)
    mediaRelationshipAttributes = mediaRelationship?.attributes

    XCTAssertNotNil(mediaRelationship)

    XCTAssertEqual(mediaRelationship?.objectID, "6")
    XCTAssertEqual(mediaRelationship?.type, "mediaRelationships")

    XCTAssertNotNil(mediaRelationship?.links)

    XCTAssertNil(mediaRelationshipAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> MediaRelationship? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(MediaRelationship.self, from: data!)
  }
}
