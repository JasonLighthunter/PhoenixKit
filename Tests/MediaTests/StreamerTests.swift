import XCTest
@testable import Media

class StreamerTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var streamer: Streamer?
  var streamerAttributes: StreamerAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }

  override func tearDown() {
    streamer = nil
    streamerAttributes = nil

    super.tearDown()
  }

  func testStreamerFullyFilled() {
    streamer = parseJson(StreamerTestData.fullyFilledJSON)
    streamerAttributes = streamer?.attributes

    XCTAssertNotNil(streamer)

    XCTAssertEqual(streamer?.objectID, "6")
    XCTAssertEqual(streamer?.type, "streamers")

    XCTAssertNotNil(streamer?.links)

    XCTAssertNotNil(streamerAttributes)

    XCTAssertEqual(
      streamerAttributes?.createdAt, dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
    )
    XCTAssertEqual(
      streamerAttributes?.updatedAt, dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
    )
    XCTAssertEqual(streamerAttributes?.siteName, "Hulu")
    XCTAssertEqual(streamerAttributes?.streamingLinksCount, 760)
    XCTAssertEqual(streamerAttributes?.logo, "logo")
  }

  func testStreamerValidMissingData() {
    streamer = parseJson(StreamerTestData.validMissingDataJSON)
    streamerAttributes = streamer?.attributes

    XCTAssertNotNil(streamer)

    XCTAssertEqual(streamer?.objectID, "6")
    XCTAssertEqual(streamer?.type, "streamers")

    XCTAssertNotNil(streamer?.links)

    XCTAssertNotNil(streamerAttributes)

    XCTAssertNil(streamerAttributes?.createdAt)
    XCTAssertNil(streamerAttributes?.updatedAt)
    XCTAssertEqual(streamerAttributes?.siteName, "Hulu")
    XCTAssertEqual(streamerAttributes?.streamingLinksCount, 760)
    XCTAssertNil(streamerAttributes?.logo)
  }

  func testStreamerValidNilData() {
    streamer = parseJson(StreamerTestData.validNilDataJSON)
    streamerAttributes = streamer?.attributes

    XCTAssertNotNil(streamer)

    XCTAssertEqual(streamer?.objectID, "6")
    XCTAssertEqual(streamer?.type, "streamers")

    XCTAssertNotNil(streamer?.links)

    XCTAssertNotNil(streamerAttributes)

    XCTAssertNil(streamerAttributes?.createdAt)
    XCTAssertNil(streamerAttributes?.updatedAt)
    XCTAssertEqual(streamerAttributes?.siteName, "Hulu")
    XCTAssertEqual(streamerAttributes?.streamingLinksCount, 760)
    XCTAssertNil(streamerAttributes?.logo)
  }

  func testStreamerInvalidMissingData() {
    streamer = parseJson(StreamerTestData.invalidMissingDataJSON)
    streamerAttributes = streamer?.attributes

    XCTAssertNotNil(streamer)

    XCTAssertEqual(streamer?.objectID, "6")
    XCTAssertEqual(streamer?.type, "streamers")

    XCTAssertNotNil(streamer?.links)

    XCTAssertNil(streamerAttributes)
  }

  func testStreamerInvalidNilData() {
    streamer = parseJson(StreamerTestData.invalidNilDataJSON)
    streamerAttributes = streamer?.attributes

    XCTAssertNotNil(streamer)

    XCTAssertEqual(streamer?.objectID, "6")
    XCTAssertEqual(streamer?.type, "streamers")

    XCTAssertNotNil(streamer?.links)

    XCTAssertNil(streamerAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Streamer? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Streamer.self, from: data!)
  }
}
