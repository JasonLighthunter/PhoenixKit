import XCTest
@testable import Media

class CategoryTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var category: Media.Category?
  var categoryAttributes: CategoryAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    category = nil
    categoryAttributes = nil

    super.tearDown()
  }

  func testCategoryFullyFilled() {
    category = parseJson(CategoryTestData.fullyFilledJSON)
    categoryAttributes = category?.attributes

    XCTAssertNotNil(category)
    XCTAssertEqual(category?.objectID, "6")
    XCTAssertEqual(category?.type, "categories")
    XCTAssertNotNil(category?.links)

    XCTAssertNotNil(categoryAttributes)
    XCTAssertEqual(
      categoryAttributes?.createdAt, dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
    )
    XCTAssertEqual(
      categoryAttributes?.updatedAt, dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
    )
    XCTAssertEqual(categoryAttributes?.title, "All Girls School")
    XCTAssertEqual(categoryAttributes?.description, "This school is only for female students.")
    XCTAssertEqual(categoryAttributes?.totalMediaCount, 72)
    XCTAssertEqual(categoryAttributes?.slug, "all-girls-school")
    XCTAssertFalse((categoryAttributes?.isNSFW)!)
    XCTAssertEqual(categoryAttributes?.childCount, 0)
    XCTAssertNotNil(categoryAttributes?.image)
  }

  func testCategoryValidMissingData() {
    category = parseJson(CategoryTestData.validMissingDataJSON)
    categoryAttributes = category?.attributes

    XCTAssertNotNil(category)
    XCTAssertEqual(category?.objectID, "6")
    XCTAssertEqual(category?.type, "categories")
    XCTAssertNotNil(category?.links)

    XCTAssertNotNil(categoryAttributes)
    let createdAtString = "2017-05-31T06:38:29.320Z"
    XCTAssertEqual(categoryAttributes?.createdAt, dateFormatter.date(from: createdAtString))
    let updatedAtString = "2017-05-31T06:39:36.788Z"
    XCTAssertEqual(categoryAttributes?.updatedAt, dateFormatter.date(from: updatedAtString))
    XCTAssertEqual(categoryAttributes?.title, "All Girls School")
    XCTAssertNil(categoryAttributes?.description)
    XCTAssertNil(categoryAttributes?.totalMediaCount)
    XCTAssertEqual(categoryAttributes?.slug, "all-girls-school")
    XCTAssertFalse((categoryAttributes?.isNSFW)!)
    XCTAssertEqual(categoryAttributes?.childCount, 0)
    XCTAssertNil(categoryAttributes?.image)
  }

  func testCategoryValidNilData() {
    category = parseJson(CategoryTestData.validNilDataJSON)
    categoryAttributes = category?.attributes

    XCTAssertNotNil(category)
    XCTAssertEqual(category?.objectID, "6")
    XCTAssertEqual(category?.type, "categories")
    XCTAssertNotNil(category?.links)

    XCTAssertNotNil(categoryAttributes)
    XCTAssertEqual(
      categoryAttributes?.createdAt, dateFormatter.date(from: "2017-05-31T06:38:29.320Z")
    )
    XCTAssertEqual(
      categoryAttributes?.updatedAt, dateFormatter.date(from: "2017-05-31T06:39:36.788Z")
    )
    XCTAssertEqual(categoryAttributes?.title, "All Girls School")
    XCTAssertNil(categoryAttributes?.description)
    XCTAssertNil(categoryAttributes?.totalMediaCount)
    XCTAssertEqual(categoryAttributes?.slug, "all-girls-school")
    XCTAssertFalse((categoryAttributes?.isNSFW)!)
    XCTAssertEqual(categoryAttributes?.childCount, 0)
    XCTAssertNil(categoryAttributes?.image)
  }

  func testCategoryInvalidMissingData() {
    category = parseJson(CategoryTestData.invalidMissingDataJSON)
    categoryAttributes = category?.attributes

    XCTAssertNotNil(category)
    XCTAssertEqual(category?.objectID, "6")
    XCTAssertEqual(category?.type, "categories")
    XCTAssertNotNil(category?.links)

    XCTAssertNil(categoryAttributes)
  }

  func testCategoryInvalidNilData() {
    category = parseJson(CategoryTestData.invalidNilDataJSON)
    categoryAttributes = category?.attributes

    XCTAssertNotNil(category)
    XCTAssertEqual(category?.objectID, "6")
    XCTAssertEqual(category?.type, "categories")
    XCTAssertNotNil(category?.links)

    XCTAssertNil(categoryAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Media.Category? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Media.Category.self, from: data!)
  }
}
