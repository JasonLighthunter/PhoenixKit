import XCTest
@testable import Media

class MangaTypeTests: XCTestCase {
  func testMediaType() {
    XCTAssertEqual(MangaType(rawValue: "manga"), .manga)
    XCTAssertEqual(MangaType(rawValue: "novel"), .novel)
    XCTAssertEqual(MangaType(rawValue: "manhua"), .manhua)
    XCTAssertEqual(MangaType(rawValue: "oneshot"), .oneshot)
    XCTAssertEqual(MangaType(rawValue: "doujin"), .doujin)
    XCTAssertEqual(MangaType(rawValue: "manhwa"), .manhwa)
    XCTAssertEqual(MangaType(rawValue: "oel"), .oel)
    XCTAssertNil(MangaType(rawValue: "InvalidInput"))
  }
}
