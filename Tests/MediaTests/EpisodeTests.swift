import XCTest
@testable import Media

class EpisodeTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var episode: Episode?
  var episodeAttributes: EpisodeAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    episode = nil
    episodeAttributes = nil

    super.tearDown()
  }

  func testEpisodeFullyFilled() {
    episode = parseJson(EpisodeTestData.fullyFilledJSON)
    episodeAttributes = episode?.attributes

    XCTAssertNotNil(episode)

    XCTAssertEqual(episode?.objectID, "6")
    XCTAssertEqual(episode?.type, "episodes")

    XCTAssertNotNil(episode?.links)

    XCTAssertNotNil(episodeAttributes)

    XCTAssertEqual(
      episodeAttributes?.createdAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertEqual(
      episodeAttributes?.updatedAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertNotNil(episodeAttributes?.titles)
    XCTAssertEqual(episodeAttributes?.canonicalTitle, "Episode 5")
    XCTAssertEqual(episodeAttributes?.seasonNumber, 1)
    XCTAssertEqual(episodeAttributes?.number, 5)
    XCTAssertEqual(episodeAttributes?.relativeNumber, 5)
    XCTAssertEqual(episodeAttributes?.synopsis, "testSynopsis")
    XCTAssertEqual(episodeAttributes?.airdate, "2018-01-23")
    XCTAssertEqual(episodeAttributes?.length, 45)

    XCTAssertNotNil(episodeAttributes?.thumbnail)
  }

  func testEpisodeValidMissingData() {
    episode = parseJson(EpisodeTestData.validMissingDataJSON)
    episodeAttributes = episode?.attributes

    XCTAssertNotNil(episode)

    XCTAssertEqual(episode?.objectID, "6")
    XCTAssertEqual(episode?.type, "episodes")

    XCTAssertNotNil(episode?.links)

    XCTAssertNotNil(episodeAttributes)

    XCTAssertEqual(
      episodeAttributes?.createdAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertEqual(
      episodeAttributes?.updatedAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertNotNil(episodeAttributes?.titles)
    XCTAssertEqual(episodeAttributes?.canonicalTitle, "Episode 5")
    XCTAssertNil(episodeAttributes?.seasonNumber)
    XCTAssertNil(episodeAttributes?.number)
    XCTAssertNil(episodeAttributes?.relativeNumber)
    XCTAssertNil(episodeAttributes?.synopsis)
    XCTAssertNil(episodeAttributes?.airdate)
    XCTAssertNil(episodeAttributes?.length)
    XCTAssertNil(episodeAttributes?.thumbnail)
  }

  func testEpisodeValidNilData() {
    episode = parseJson(EpisodeTestData.validNilDataJSON)
    episodeAttributes = episode?.attributes

    XCTAssertNotNil(episode)

    XCTAssertEqual(episode?.objectID, "6")
    XCTAssertEqual(episode?.type, "episodes")

    XCTAssertNotNil(episode?.links)

    XCTAssertNotNil(episodeAttributes)

    XCTAssertEqual(
      episodeAttributes?.createdAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertEqual(
      episodeAttributes?.updatedAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertNotNil(episodeAttributes?.titles)
    XCTAssertEqual(episodeAttributes?.canonicalTitle, "Episode 5")
    XCTAssertNil(episodeAttributes?.seasonNumber)
    XCTAssertNil(episodeAttributes?.number)
    XCTAssertNil(episodeAttributes?.relativeNumber)
    XCTAssertNil(episodeAttributes?.synopsis)
    XCTAssertNil(episodeAttributes?.airdate)
    XCTAssertNil(episodeAttributes?.length)
    XCTAssertNil(episodeAttributes?.thumbnail)
  }

  func testEpisodeInvalidMissingData() {
    episode = parseJson(EpisodeTestData.invalidMissingDataJSON)
    episodeAttributes = episode?.attributes

    XCTAssertNotNil(episode)

    XCTAssertEqual(episode?.objectID, "6")
    XCTAssertEqual(episode?.type, "episodes")

    XCTAssertNotNil(episode?.links)

    XCTAssertNil(episodeAttributes)
  }

  func testEpisodeInvalidNilData() {
    episode = parseJson(EpisodeTestData.invalidNilDataJSON)
    episodeAttributes = episode?.attributes

    XCTAssertNotNil(episode)

    XCTAssertEqual(episode?.objectID, "6")
    XCTAssertEqual(episode?.type, "episodes")

    XCTAssertNotNil(episode?.links)

    XCTAssertNil(episodeAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Episode? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Episode.self, from: data!)
  }
}
