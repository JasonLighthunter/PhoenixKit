import XCTest
@testable import Media

class MediaRelationshipRoleEnumTests: XCTestCase {
  func testMediaRelationshipRoleEnum() {
    XCTAssertEqual(MediaRelationshipRole(rawValue: "sequel"), .sequel)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "prequel"), .prequel)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "alternative_setting"), .alternativeSetting)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "alternative_version"), .alternativeVersion)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "side_story"), .sideStory)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "parent_story"), .parentStory)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "summary"), .summary)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "full_story"), .fullStory)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "spinoff"), .spinoff)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "adaptation"), .adaptation)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "character"), .character)
    XCTAssertEqual(MediaRelationshipRole(rawValue: "other"), .other)
    XCTAssertNil(MediaRelationshipRole(rawValue: "InvalidInput"))
  }
}
