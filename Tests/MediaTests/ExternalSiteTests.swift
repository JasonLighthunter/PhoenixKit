import XCTest
@testable import Media

class ExternalSiteTests: XCTestCase {
  func testExternalSite() {
    XCTAssertEqual(ExternalSite(rawValue: "anidb"), .anidb)
    XCTAssertEqual(ExternalSite(rawValue: "myanimelist/anime"), .myAnimeListAnime)
    XCTAssertEqual(ExternalSite(rawValue: "myanimelist/manga"), .myAnimeListManga)
    XCTAssertEqual(ExternalSite(rawValue: "thetvdb/season"), .TVDBSeason)
    XCTAssertEqual(ExternalSite(rawValue: "thetvdb/series"), .TVDBSeries)
    XCTAssertNil(ExternalSite(rawValue: "InvalidInput"))
  }
}
