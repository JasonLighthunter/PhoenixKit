import XCTest
@testable import Media

class AgeRatingEnumTests: XCTestCase {
  func testAgeRatingEnum() {
    XCTAssertEqual(AgeRating(rawValue: "G"), .generalAudiences)
    XCTAssertEqual(AgeRating(rawValue: "R"), .restricted)
    XCTAssertEqual(AgeRating(rawValue: "R18"), .restricted18)
    XCTAssertEqual(AgeRating(rawValue: "PG"), .parentalGuidance)
    XCTAssertNil(AgeRating(rawValue: "InvalidInput"))
  }
}
