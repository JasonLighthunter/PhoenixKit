import XCTest
@testable import Media

class ChapterTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  var chapter: Chapter?
  var chapterAttributes: ChapterAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    chapter = nil
    chapterAttributes = nil

    super.tearDown()
  }

  func testChapterFullyFilled() {
    chapter = parseJson(ChapterTestData.fullyFilledJSON)
    chapterAttributes = chapter?.attributes

    XCTAssertNotNil(chapter)

    XCTAssertEqual(chapter?.objectID, "6")
    XCTAssertEqual(chapter?.type, "chapters")

    XCTAssertNotNil(chapter?.links)

    XCTAssertNotNil(chapterAttributes)

    XCTAssertEqual(
      chapterAttributes?.createdAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertEqual(
      chapterAttributes?.updatedAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertNotNil(chapterAttributes?.titles)
    XCTAssertEqual(chapterAttributes?.canonicalTitle, "Chapter 5")
    XCTAssertEqual(chapterAttributes?.volumeNumber, 1)
    XCTAssertEqual(chapterAttributes?.number, 5)
    XCTAssertEqual(chapterAttributes?.synopsis, "testSynopsis")
    XCTAssertEqual(chapterAttributes?.published, "2018-01-23")
    XCTAssertEqual(chapterAttributes?.length, 45)

    XCTAssertNotNil(chapterAttributes?.thumbnail)
  }

  func testChapterValidMissingData() {
    chapter = parseJson(ChapterTestData.validMissingDataJSON)
    chapterAttributes = chapter?.attributes

    XCTAssertNotNil(chapter)

    XCTAssertEqual(chapter?.objectID, "6")
    XCTAssertEqual(chapter?.type, "chapters")

    XCTAssertNotNil(chapter?.links)

    XCTAssertNotNil(chapterAttributes)

    XCTAssertEqual(
      chapterAttributes?.createdAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertEqual(
      chapterAttributes?.updatedAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertNotNil(chapterAttributes?.titles)
    XCTAssertEqual(chapterAttributes?.canonicalTitle, "Chapter 5")
    XCTAssertNil(chapterAttributes?.volumeNumber)
    XCTAssertEqual(chapterAttributes?.number, 5)
    XCTAssertNil(chapterAttributes?.synopsis)
    XCTAssertNil(chapterAttributes?.published)
    XCTAssertNil(chapterAttributes?.length)
    XCTAssertNil(chapterAttributes?.thumbnail)
  }

  func testChapterValidNilData() {
    chapter = parseJson(ChapterTestData.validNilDataJSON)
    chapterAttributes = chapter?.attributes

    XCTAssertNotNil(chapter)

    XCTAssertEqual(chapter?.objectID, "6")
    XCTAssertEqual(chapter?.type, "chapters")

    XCTAssertNotNil(chapter?.links)

    XCTAssertNotNil(chapterAttributes)

    XCTAssertEqual(
      chapterAttributes?.createdAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertEqual(
      chapterAttributes?.updatedAt, dateFormatter.date(from: "2017-06-30T16:07:59.760Z")
    )
    XCTAssertNotNil(chapterAttributes?.titles)
    XCTAssertEqual(chapterAttributes?.canonicalTitle, "Chapter 5")
    XCTAssertNil(chapterAttributes?.volumeNumber)
    XCTAssertEqual(chapterAttributes?.number, 5)
    XCTAssertNil(chapterAttributes?.synopsis)
    XCTAssertNil(chapterAttributes?.published)
    XCTAssertNil(chapterAttributes?.length)
    XCTAssertNil(chapterAttributes?.thumbnail)
  }

  func testChapterInvalidMissingData() {
    chapter = parseJson(ChapterTestData.invalidMissingDataJSON)
    chapterAttributes = chapter?.attributes

    XCTAssertNotNil(chapter)

    XCTAssertEqual(chapter?.objectID, "6")
    XCTAssertEqual(chapter?.type, "chapters")

    XCTAssertNotNil(chapter?.links)

    XCTAssertNil(chapterAttributes)
  }

  func testChapterInvalidNilData() {
    chapter = parseJson(ChapterTestData.invalidNilDataJSON)
    chapterAttributes = chapter?.attributes

    XCTAssertNotNil(chapter)

    XCTAssertEqual(chapter?.objectID, "6")
    XCTAssertEqual(chapter?.type, "chapters")

    XCTAssertNotNil(chapter?.links)

    XCTAssertNil(chapterAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> Chapter? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(Chapter.self, from: data!)
  }
}
