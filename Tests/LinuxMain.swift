import XCTest

import CoreTests
import MediaTests
import ImageTests
import GroupsTests
import UsersTests
import ReactionsTests
import CharactersAndPeopleTests
import PostsTests
import SiteAnnouncements

var tests = [XCTestCaseEntry]()
tests += CoreTests.allTests()
tests += MediaTests.allTests()
tests += ImageTests.allTests()
tests += GroupsTests.allTests()
tests += ReactionsTests.allTests()
tests += UsersTests.allTests()
tests += CharactersAndPeopleTests.allTests()
tests += PostsTests.allTests()
tests += SiteAnnouncements.allTests()
XCTMain(tests)
