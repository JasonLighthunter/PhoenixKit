import XCTest
@testable import Posts

class CommentLikeTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()

  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "commentLikes",
    "links": [
      "self": "https://kitsu.io/api/edge/comment-likes/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "4",
//    "type": "commentLikes",
//    "links": [
//      "self": "https://kitsu.io/api/edge/comment-likes/4"
//    ],
//    "attributes": [
//      "createdAt": "2017-08-08T12:39:19.217Z",
//      "updatedAt": "2017-08-08T12:39:19.217Z"
//    ]
//  ]

//  let validNilDataJSON: [String : Any] = [
//    "id": "4",
//    "type": "commentLikes",
//    "links": [
//      "self": "https://kitsu.io/api/edge/comment-likes/4"
//    ],
//    "attributes": [
//      "createdAt": "2017-08-08T12:39:19.217Z",
//      "updatedAt": "2017-08-08T12:39:19.217Z"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "commentLikes",
    "links": [
      "self": "https://kitsu.io/api/edge/comment-likes/4"
    ],
    "attributes": [
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "commentLikes",
    "links": [
      "self": "https://kitsu.io/api/edge/comment-likes/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]

  var commentLike: CommentLike?
  var commentLikeAttributes: CommentLikeAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    commentLike = nil
    commentLikeAttributes = nil
    
    super.tearDown()
  }
  
  func testCommentLikeFullyFilled() {
    commentLike = parseJson(fullyFilledJSON)
    commentLikeAttributes = commentLike?.attributes
    
    XCTAssertNotNil(commentLike)
    
    XCTAssertEqual(commentLike?.objectID, "4")
    XCTAssertEqual(commentLike?.type, "commentLikes")
    
    XCTAssertNotNil(commentLikeAttributes)

    XCTAssertEqual(
      commentLikeAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      commentLikeAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
  }
  
//  func testCommentLikeValidMissingData() {
//    commentLike = parseJson(validMissingDataJSON)
//    commentLikeAttributes = commentLike?.attributes
//
//    XCTAssertNotNil(commentLike)
//
//    XCTAssertEqual(commentLike?.objectID, "4")
//    XCTAssertEqual(commentLike?.type, "commentLikes")
//
//    XCTAssertNotNil(commentLikeAttributes)
//
//    XCTAssertEqual(commentLikeAttributes?.createdAt, "2017-08-08T12:39:19.217Z")
//    XCTAssertEqual(commentLikeAttributes?.updatedAt, "2017-08-08T12:39:19.217Z")
//  }

//  func testCommentLikeValidNilData() {
//    commentLike = parseJson(validNilDataJSON)
//    commentLikeAttributes = commentLike?.attributes
//
//    XCTAssertNotNil(commentLike)
//
//    XCTAssertEqual(commentLike?.objectID, "4")
//    XCTAssertEqual(commentLike?.type, "commentLikes")
//
//    XCTAssertNotNil(commentLikeAttributes)
//
//    XCTAssertEqual(commentLikeAttributes?.createdAt, "2017-08-08T12:39:19.217Z")
//    XCTAssertEqual(commentLikeAttributes?.updatedAt, "2017-08-08T12:39:19.217Z")
//  }

  func testCommentLikeInvalidMissingData() {
    commentLike = parseJson(invalidMissingDataJSON)
    commentLikeAttributes = commentLike?.attributes
    
    XCTAssertNotNil(commentLike)
    
    XCTAssertEqual(commentLike?.objectID, "4")
    XCTAssertEqual(commentLike?.type, "commentLikes")
    
    XCTAssertNil(commentLikeAttributes)
  }
  
  func testCommentLikeInvalidNilData() {
    commentLike = parseJson(invalidNilDataJSON)
    commentLikeAttributes = commentLike?.attributes
    
    XCTAssertNotNil(commentLike)
    
    XCTAssertEqual(commentLike?.objectID, "4")
    XCTAssertEqual(commentLike?.type, "commentLikes")
    
    XCTAssertNil(commentLikeAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> CommentLike? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(CommentLike.self, from: data!)
  }
}
