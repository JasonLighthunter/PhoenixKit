import XCTest
@testable import Posts

class PostFollowTests: XCTestCase {
  let JSONdecoder = JSONDecoder()
  let dateFormatter = DateFormatter()
  
  let fullyFilledJSON: [String : Any] = [
    "id": "4",
    "type": "postFollows",
    "links": [
      "self": "https://kitsu.io/api/edge/post-follows/4"
    ],
    "attributes": [
      "createdAt": "2017-08-08T12:39:19.217Z",
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]
  
//  let validMissingDataJSON: [String : Any] = [
//    "id": "4",
//    "type": "postFollows",
//    "links": [
//      "self": "https://kitsu.io/api/edge/post-follows/4"
//    ],
//    "attributes": [
//      "createdAt": "2017-08-08T12:39:19.217Z",
//      "updatedAt": "2017-08-08T12:39:19.217Z"
//    ]
//  ]

//  let validNilDataJSON: [String : Any] = [
//    "id": "4",
//    "type": "postFollows",
//    "links": [
//      "self": "https://kitsu.io/api/edge/post-follows/4"
//    ],
//    "attributes": [
//      "createdAt": "2017-08-08T12:39:19.217Z",
//      "updatedAt": "2017-08-08T12:39:19.217Z"
//    ]
//  ]

  let invalidMissingDataJSON: [String : Any] = [
    "id": "4",
    "type": "postFollows",
    "links": [
      "self": "https://kitsu.io/api/edge/post-follows/4"
    ],
    "attributes": [
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]
  
  let invalidNilDataJSON: [String : Any?] = [
    "id": "4",
    "type": "postFollows",
    "links": [
      "self": "https://kitsu.io/api/edge/post-follows/4"
    ],
    "attributes": [
      "createdAt": nil,
      "updatedAt": "2017-08-08T12:39:19.217Z"
    ]
  ]
  
  var postFollow: PostFollow?
  var postFollowAttributes: PostFollowAttributes?

  override func setUp() {
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    JSONdecoder.dateDecodingStrategy = .formatted(dateFormatter)
  }
  
  override func tearDown() {
    postFollow = nil
    postFollowAttributes = nil
    
    super.tearDown()
  }
  
  func testPostFollowFullyFilled() {
    postFollow = parseJson(fullyFilledJSON)
    postFollowAttributes = postFollow?.attributes
    
    XCTAssertNotNil(postFollow)
    
    XCTAssertEqual(postFollow?.objectID, "4")
    XCTAssertEqual(postFollow?.type, "postFollows")
    
    XCTAssertNotNil(postFollowAttributes)

    XCTAssertEqual(
      postFollowAttributes?.createdAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
    XCTAssertEqual(
      postFollowAttributes?.updatedAt,
      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
    )
  }
  
//  func testPostFollowValidMissingData() {
//    postFollow = parseJson(validMissingDataJSON)
//    postFollowAttributes = postFollow?.attributes
//
//    XCTAssertNotNil(postFollow)
//
//    XCTAssertEqual(postFollow?.objectID, "4")
//    XCTAssertEqual(postFollow?.type, "postFollows")
//
//    XCTAssertNotNil(postFollowAttributes)
//
//    XCTAssertEqual(
//      postFollowAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      postFollowAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//  }

//  func testPostFollowValidNilData() {
//    postFollow = parseJson(validNilDataJSON)
//    postFollowAttributes = postFollow?.attributes
//
//    XCTAssertNotNil(postFollow)
//
//    XCTAssertEqual(postFollow?.objectID, "4")
//    XCTAssertEqual(postFollow?.type, "postFollows")
//
//    XCTAssertNotNil(postFollowAttributes)
//
//    XCTAssertEqual(
//      postFollowAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//    XCTAssertEqual(
//      postFollowAttributes?.createdAt,
//      dateFormatter.date(from: "2017-08-08T12:39:19.217Z")
//    )
//  }

  func testPostFollowInvalidMissingData() {
    postFollow = parseJson(invalidMissingDataJSON)
    postFollowAttributes = postFollow?.attributes
    
    XCTAssertNotNil(postFollow)
    
    XCTAssertEqual(postFollow?.objectID, "4")
    XCTAssertEqual(postFollow?.type, "postFollows")
    
    XCTAssertNil(postFollowAttributes)
  }
  
  func testPostFollowInvalidNilData() {
    postFollow = parseJson(invalidNilDataJSON)
    postFollowAttributes = postFollow?.attributes
    
    XCTAssertNotNil(postFollow)
    
    XCTAssertEqual(postFollow?.objectID, "4")
    XCTAssertEqual(postFollow?.type, "postFollows")
    
    XCTAssertNil(postFollowAttributes)
  }

  func parseJson(_ json: [String : Any?]) -> PostFollow? {
    guard JSONSerialization.isValidJSONObject(json as Any) else { return nil }

    let data = try? JSONSerialization.data(withJSONObject: json as Any)
    return try? JSONdecoder.decode(PostFollow.self, from: data!)
  }
}
