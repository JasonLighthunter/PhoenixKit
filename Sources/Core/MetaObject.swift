public struct MetaObject: Decodable {
  public let count: Int?
  public let statusCounts: StatusCounts?
}
