import Foundation

public struct Links: Decodable {
  public let selfLink: URL?

  private enum CodingKeys: String, CodingKey {
    case selfLink = "self"
  }
}
