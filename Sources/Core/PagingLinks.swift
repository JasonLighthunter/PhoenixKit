import Foundation

public struct PagingLinks: Decodable {
  public let first: URL
  public let prev: URL?
  public let next: URL?
  public let last: URL
}
