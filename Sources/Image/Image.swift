import Foundation

public class Image: Decodable {
  public let tiny: URL?
  public let small: URL?
  public let medium: URL?
  public let large: URL?
  public let original: URL?
  public let meta: ImageMeta?
}
