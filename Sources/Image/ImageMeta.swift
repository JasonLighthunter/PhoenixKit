public struct ImageMeta: Decodable {
  public let dimensions: ImageMetaDimensions?
}

public struct ImageMetaDimension: Decodable {
    public let width: Int?
    public let height: Int?
}

public struct ImageMetaDimensions: Decodable {
    public let tiny: ImageMetaDimension?
    public let small: ImageMetaDimension?
    public let medium: ImageMetaDimension?
    public let large: ImageMetaDimension?
}
