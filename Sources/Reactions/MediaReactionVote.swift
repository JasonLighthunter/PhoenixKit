import Core
import Requestable
import Foundation

public class MediaReactionVote: KitsuObject<MediaReactionVoteAttributes>, Requestable {
  public static var requestURLString = "media-reaction-votes"
}

public class MediaReactionVoteAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
