import Core
import Requestable
import Foundation

public class MediaReaction: KitsuObject<MediaReactionAttributes>, Requestable {
  public static var requestURLString = "media-reactions"
}

public class MediaReactionAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let reaction: String?
  public let upVotesCount: Int
}
