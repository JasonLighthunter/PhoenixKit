public enum RatingSystem: String, Decodable {
  case simple
  case advanced
  case regular
}
