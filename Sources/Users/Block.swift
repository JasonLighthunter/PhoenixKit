import Core
import Requestable
import Foundation

public class Block: KitsuObject<BlockAttributes>, Requestable {
  public static var requestURLString = "blocks"
}

public class BlockAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
