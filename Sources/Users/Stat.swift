import Core
import Requestable
import Foundation

public class Stat: KitsuObject<StatAttributes>, Requestable {
  public static var requestURLString = "stats"
}

public class StatAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let kind: StatKind
  public let statsData: StatData?
}
