public enum RegistrationStatus: String, Decodable {
  case unregistered
  case registered
}
