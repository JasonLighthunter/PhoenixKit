import Core
import Requestable
import Foundation

public class UserRole: KitsuObject<UserRoleAttributes>, Requestable {
  public static var requestURLString = "user-roles"
}

public class UserRoleAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
}
