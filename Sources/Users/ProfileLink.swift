import Core
import Requestable
import Foundation

public class ProfileLink: KitsuObject<ProfileLinkAttributes>, Requestable {
  public static var requestURLString = "profile-links"
}

public class ProfileLinkAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let url: URL
}
