import Core
import Requestable
import Foundation

public class Favorite: KitsuObject<FavoriteAttributes>, Requestable {
  public static var requestURLString = "favorites"
}

public class FavoriteAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let favoritesRank: Int
  
  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case favoritesRank = "favRank"
  }
}
