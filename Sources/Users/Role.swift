import Core
import Requestable
import Foundation

public class Role: KitsuObject<RoleAttributes>, Requestable {
  public static var requestURLString = "roles"
}

public class RoleAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let name: String?
  public let resourceID: String?
  public let resourceType: String?
  
  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case name
    case resourceID = "resourceId"
    case resourceType
  }
}
