import Core
import Requestable
import Foundation

public class ProfileLinkSite: KitsuObject<ProfileLinkSiteAttributes>, Requestable {
  public static var requestURLString = "profile-link-sites"
}

public class ProfileLinkSiteAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let name: String
}
