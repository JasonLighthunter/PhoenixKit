import Core
import Requestable
import Foundation

public class Follow: KitsuObject<FollowAttributes>, Requestable {
  public static var requestURLString = "follows"
}

public class FollowAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
