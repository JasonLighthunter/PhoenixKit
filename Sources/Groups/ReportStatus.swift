public enum ReportStatus: String, Decodable {
  case reported
  case resolved
  case declined
}
