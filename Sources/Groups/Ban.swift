import Core
import Requestable
import Foundation

public class GroupBan: KitsuObject<GroupBanAttributes>, Requestable {
  public static var requestURLString = "group-bans"
}

public class GroupBanAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let notes: String?
  public let notesFormatted: String?
}
