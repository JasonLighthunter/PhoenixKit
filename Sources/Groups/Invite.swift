import Core
import Requestable
import Foundation

public class Invite: KitsuObject<InviteAttributes>, Requestable {
  public static var requestURLString = "group-invites"
}

public class InviteAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let acceptedAt: Date?
  public let declinedAt: Date?
  public let revokedAt: Date?
}
