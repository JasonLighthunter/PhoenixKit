import Core
import Requestable
import Foundation

public class Ticket: KitsuObject<TicketAttributes>, Requestable {
  public static var requestURLString = "group-tickets"
}

public class TicketAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let status: TicketStatus
}
