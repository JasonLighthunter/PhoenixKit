import Core
import Requestable
import Foundation

public class LeaderChatMessage: KitsuObject<LeaderChatMessageAttributes>, Requestable {
  public static var requestURLString = "leader-chat-messages"
}

public class LeaderChatMessageAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let content: String
  public let contentFormatted: String
  public let editedAt: Date?
}
