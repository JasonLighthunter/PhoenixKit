import Core
import Requestable
import Foundation

public class TicketMessage: KitsuObject<TicketMessageAttributes>, Requestable {
  public static var requestURLString = "group-ticket-messages"
}

public class TicketMessageAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let kind: TicketMessageKind
  public let content: String
}
