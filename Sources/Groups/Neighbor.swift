import Core
import Requestable
import Foundation

public class Neighbor: KitsuObject<NeighborAttributes>, Requestable {
  public static var requestURLString = "group-neighbor"
}

public class NeighborAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
