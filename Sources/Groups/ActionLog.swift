import Core
import Requestable
import Foundation

public class ActionLog: KitsuObject<ActionLogAttributes>, Requestable {
  public static var requestURLString = "group-action-logs"
}

public class ActionLogAttributes: KitsuObjectAttributesWithCreatedAt {
  public let createdAt: Date
  public let updatedAt: Date?
  public let verb: String
}
