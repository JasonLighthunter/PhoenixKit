import Core
import Requestable
import Foundation

public class Ban: KitsuObject<BanAttributes>, Requestable {
  public static var requestURLString = "group-bans"
}

public class BanAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let notes: String?
  public let notesFormatted: String?
}
