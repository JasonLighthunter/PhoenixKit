public enum TicketStatus: String, Decodable {
  case created
  case assigned
  case resolved
}
