public enum MemberRank: String, Decodable {
  case pleb
  case mod
  case admin
}
