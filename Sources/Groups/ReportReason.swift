public enum ReportReason: String, Decodable {
  case nsfw
  case offensive
  case spoiler
  case bullying
  case other
  case spam
}
