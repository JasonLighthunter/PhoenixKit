import Core
import Requestable
import Foundation

public class Permission: KitsuObject<PermissionAttributes>, Requestable {
  public static var requestURLString = "group-permissions"
}

public class PermissionAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let permission: PermissionScope
}
