public enum PermissionScope: String, Decodable {
  case owner
  case tickets
  case members
  case leaders
  case community
  case content
}
