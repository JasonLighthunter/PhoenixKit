import Core
import Requestable

public class Member: KitsuObject<MemberAttributes>, Requestable {
  public static var requestURLString = "group-members"
}

public class MemberAttributes: KitsuObjectAttributes {
  public let createdAt: String?
  public let updatedAt: String?
  public let rank: MemberRank
  public let unreadCount: Int
}
