import Core
import Image
import Requestable
import Foundation

public class Group: KitsuObject<GroupAttributes>, Requestable {
  public static let requestURLString = "groups"
}

public class GroupAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let slug: String
  public let about: String
  public let locale: String?
  public let membersCount: Int
  public let name: String
  public let isNSFW: Bool
  public let privacy: PrivacyScope
  public let rules: String?
  public let rulesFormatted: String?
  public let leadersCount: Int
  public let neighborsCount: Int
  public let isFeatured: Bool
  public let tagline: String?
  public let lastActivityAt: Date?
  public let avatar: Image?
  public let coverImage: Image?

  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case slug
    case about
    case locale
    case membersCount
    case name
    case isNSFW = "nsfw"
    case privacy
    case rules
    case rulesFormatted
    case leadersCount
    case neighborsCount
    case isFeatured = "featured"
    case tagline
    case lastActivityAt
    case avatar
    case coverImage
  }
}
