public enum TicketMessageKind: String, Decodable {
  case message
  case modNote = "mod_note"
}
