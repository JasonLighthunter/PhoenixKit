import Core
import Requestable
import Foundation

public class MemberNote: KitsuObject<MemberNoteAttributes>, Requestable {
  public static var requestURLString = "group-member-notes"
}

public class MemberNoteAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let content: String
  public let contentFormatted: String
}
