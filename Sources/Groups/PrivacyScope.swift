public enum PrivacyScope: String, Decodable {
  case open
  case closed
  case restricted
}
