import Core
import Requestable
import Foundation

public class Report: KitsuObject<ReportAttributes>, Requestable {
  public static var requestURLString = "group-reports"
}

public class ReportAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let reason: ReportReason
  public let status: ReportStatus
  public let explanation: String?
  public let naughtyType: String
  public let naughtyID: Int
  
  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case reason
    case status
    case explanation
    case naughtyType
    case naughtyID = "naughtyId"
  }
}
