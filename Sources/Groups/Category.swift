import Core
import Requestable
import Foundation

public class Category: KitsuObject<CategoryAttributes>, Requestable {
  public static var requestURLString = "group-categories"
}

public class CategoryAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let name: String
  public let slug: String
  public let description: String?
}
