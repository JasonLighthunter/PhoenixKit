public enum AnimeType: String, Decodable {
  case television = "TV"
  case special
  case OVA
  case ONA
  case movie
  case music
}
