import Core
import Requestable
import Foundation

public class CategoryFavorite: KitsuObject<CategoryFavoriteAttributes>, Requestable {
  public static var requestURLString = "category-favorites"
}

public class CategoryFavoriteAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
