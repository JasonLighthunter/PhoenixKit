import Core
import Image
import Requestable
import Foundation

public class Chapter: KitsuObject<ChapterAttributes>, Requestable {
  public static var requestURLString = "chapters"
}

public class ChapterAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let titles: Titles
  public let canonicalTitle: String
  public let volumeNumber: Int?
  public let number: Int
  public let synopsis: String?
  public let published: String?
  public let length: Int?
  public let thumbnail: Image?
}
