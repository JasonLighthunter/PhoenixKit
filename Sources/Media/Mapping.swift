import Core
import Requestable
import Foundation

public class Mapping: KitsuObject<MappingAttributes>, Requestable {
  public static var requestURLString = "mappings"
}

public class MappingAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let externalSite: ExternalSite
  public let externalID: String

  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case externalSite
    case externalID = "externalId"
  }
}
