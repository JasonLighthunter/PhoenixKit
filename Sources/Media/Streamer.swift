import Core
import Requestable
import Foundation

public class Streamer: KitsuObject<StreamerAttributes>, Requestable {
  public static var requestURLString = "streamers"
}

public class StreamerAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let siteName: String
  public let streamingLinksCount: Int
  public let logo: String?
}
