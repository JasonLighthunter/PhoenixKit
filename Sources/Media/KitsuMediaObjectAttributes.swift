import Core
import Image
import Foundation

public protocol KitsuMediaObjectAttributes: KitsuObjectAttributesWithTimestamp {
  var slug: String? {get}
  var synopsis: String? {get}
  var titles: Titles {get}
  var canonicalTitle: String {get}
  var abbreviatedTitles: [String]? {get}
  var averageRating: String? {get}
  var ratingFrequencies: [String: String] {get}
  var userCount: Int {get}
  var favoritesCount: Int {get}
  var startDate: Date? {get}
  var endDate: Date? {get}
  var popularityRank: Int? {get}
  var ratingRank: Int? {get}
  var ageRating: AgeRating? {get}
  var ageRatingGuide: String? {get}
  var status: ReleaseStatus {get}
  var toBeAnnounced: String? {get}
  var posterImage: Image? {get}
  var coverImage: Image? {get}
}
