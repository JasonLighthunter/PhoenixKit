import Core
import Image
import Requestable
import Foundation

public class Anime: KitsuMediaObject<AnimeAttributes>, Requestable {
  public static var requestURLString = "anime"
}

public class AnimeAttributes: KitsuMediaObjectAttributes {
  public let createdAt: Date
  public let updatedAt: Date
  public let slug: String?
  public let synopsis: String?
  public let titles: Titles
  public let canonicalTitle: String
  public let abbreviatedTitles: [String]?
  public let averageRating: String?
  public let ratingFrequencies: [String: String]
  public let userCount: Int
  public let favoritesCount: Int
  public let startDate: Date?
  public let endDate: Date?
  public let popularityRank: Int?
  public let ratingRank: Int?
  public let ageRating: AgeRating?
  public let ageRatingGuide: String?
  public let subtype: AnimeType
  public let status: ReleaseStatus
  public let toBeAnnounced: String?
  public let posterImage: Image?
  public let coverImage: Image?
  public let episodeCount: Int?
  public let episodeLength: Int?
  public let youtubeVideoID: String?
  public let isNSFW: Bool

  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case slug
    case synopsis
    case titles
    case canonicalTitle
    case abbreviatedTitles
    case averageRating
    case ratingFrequencies
    case userCount
    case favoritesCount
    case startDate
    case endDate
    case popularityRank
    case ratingRank
    case ageRating
    case ageRatingGuide
    case subtype
    case status
    case toBeAnnounced = "tba"
    case posterImage
    case coverImage
    case episodeCount
    case episodeLength
    case youtubeVideoID = "youtubeVideoId"
    case isNSFW = "nsfw"
  }

  required public init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    createdAt = try values.decode(Date.self, forKey: .createdAt)
    updatedAt = try values.decode(Date.self, forKey: .updatedAt)
    slug = try values.decodeIfPresent(String.self, forKey: .slug)
    synopsis = try values.decodeIfPresent(String.self, forKey: .synopsis)
    titles = try values.decode(Titles.self, forKey: .titles)
    canonicalTitle = try values.decode(String.self, forKey: .canonicalTitle)
    abbreviatedTitles = try values.decodeIfPresent([String].self, forKey: .abbreviatedTitles)
    averageRating = try values.decodeIfPresent(String.self, forKey: .averageRating)
    ratingFrequencies = try values.decode([String: String].self, forKey: .ratingFrequencies)
    userCount = try values.decode(Int.self, forKey: .userCount)
    favoritesCount = try values.decode(Int.self, forKey: .favoritesCount)

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"

    let startDateString = try values.decodeIfPresent(String.self, forKey: .startDate) ?? ""
    startDate = dateFormatter.date(from: startDateString)
    let endDateString = try values.decodeIfPresent(String.self, forKey: .endDate) ?? ""
    endDate = dateFormatter.date(from: endDateString)

    popularityRank = try values.decodeIfPresent(Int.self, forKey: .popularityRank)
    ratingRank = try values.decodeIfPresent(Int.self, forKey: .ratingRank)
    ageRating = try values.decodeIfPresent(AgeRating.self, forKey: .ageRating)
    ageRatingGuide = try values.decodeIfPresent(String.self, forKey: .ageRatingGuide)
    subtype = try values.decode(AnimeType.self, forKey: .subtype)
    status = try values.decode(ReleaseStatus.self, forKey: .status)
    toBeAnnounced = try values.decodeIfPresent(String.self, forKey: .toBeAnnounced)
    posterImage = try values.decodeIfPresent(Image.self, forKey: .posterImage)
    coverImage = try values.decodeIfPresent(Image.self, forKey: .coverImage)
    episodeCount = try values.decodeIfPresent(Int.self, forKey: .episodeCount)
    episodeLength = try values.decodeIfPresent(Int.self, forKey: .episodeLength)
    youtubeVideoID = try values.decodeIfPresent(String.self, forKey: .youtubeVideoID)
    isNSFW = try values.decode(Bool.self, forKey: .isNSFW)
  }
}
