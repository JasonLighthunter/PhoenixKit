import Core
import Requestable
import Foundation

public class MediaRelationship: KitsuObject<MediaRelationshipAttributes>, Requestable {
  public static var requestURLString = "Media-relationships"
}

public class MediaRelationshipAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let role: MediaRelationshipRole
}
