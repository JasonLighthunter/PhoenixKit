import Core
import Requestable
import Foundation

public class StreamingLink: KitsuObject<StreamingLinkAttributes>, Requestable {
  public static var requestURLString = "streaming-links"
}

public class StreamingLinkAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let url: String
  public let subs: [String]
  public let dubs: [String]
}
