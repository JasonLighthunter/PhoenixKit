import Core
import Requestable
import Foundation

public class MangaCharacter: KitsuObject<MangaCharacterAttributes>, Requestable {
  public static var requestURLString = "manga-characters"
}

public class MangaCharacterAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let role: MediaCharacterRole
}
