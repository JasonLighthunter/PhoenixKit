import Core
import Requestable
import Foundation

public class AnimeCharacter: KitsuObject<AnimeCharacterAttributes>, Requestable {
  public static var requestURLString = "anime-characters"
}

public class AnimeCharacterAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let role: MediaCharacterRole
}
