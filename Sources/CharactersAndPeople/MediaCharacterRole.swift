// Manga, Drama and Anime Combined might change in the future
public enum MediaCharacterRole: String, Decodable {
  case main
  case supporting
}
