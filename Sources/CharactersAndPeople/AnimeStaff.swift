import Core
import Requestable
import Foundation

public class AnimeStaff: KitsuObject<AnimeStaffAttributes>, Requestable {
  public static var requestURLString = "anime-staff"
}

public class AnimeStaffAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let role: String?
}
