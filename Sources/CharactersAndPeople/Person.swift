import Core
import Requestable
import Foundation

public class Person: KitsuObject<PersonAttributes>, Requestable {
  public static var requestURLString = "people"
}

public class PersonAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let image: URL?
  public let name: String?
  public let myAnimeListID: Int?
  
  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case image
    case name
    case myAnimeListID = "malId"
  }
}
