import Core
import Requestable
import Foundation

public class Producer: KitsuObject<ProducerAttributes>, Requestable {
  public static var requestURLString = "producers"
}

public class ProducerAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let slug: String?
  public let name: String?
}
