import Core
import Requestable
import Foundation

public class AnimeProduction: KitsuObject<AnimeProductionAttributes>, Requestable {
  public static var requestURLString = "anime-productions"
}

public class AnimeProductionAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let role: AnimeProductionRole
}
