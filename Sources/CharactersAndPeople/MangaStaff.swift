import Core
import Requestable
import Foundation

public class MangaStaff: KitsuObject<MangaStaffAttributes>, Requestable {
  public static var requestURLString = "manga-staff"
}

public class MangaStaffAttributes: KitsuObjectAttributesWithOptionalTimestamp {
  public let createdAt: Date?
  public let updatedAt: Date?
  public let role: String?
}
