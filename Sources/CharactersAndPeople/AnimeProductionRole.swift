public enum AnimeProductionRole: String, Decodable {
  case producer
  case licensor
  case studio
}
