import Core
import Requestable
import Foundation

public class SiteAnnouncement: KitsuObject<SiteAnnouncementAttributes>, Requestable {
  public static var requestURLString = "site-announcements"
}

public class SiteAnnouncementAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
  public let title: String
  public let description: String?
  public let imageURL: URL?
  public let link: URL?

  private enum CodingKeys: String, CodingKey {
    case createdAt
    case updatedAt
    case title
    case description
    case imageURL = "imageUrl"
    case link
  }
}
