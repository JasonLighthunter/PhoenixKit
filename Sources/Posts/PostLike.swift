import Core
import Requestable
import Foundation

public class PostLike: KitsuObject<PostLikeAttributes>, Requestable {
  public static var requestURLString = "post-likes"
}

public class PostLikeAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
