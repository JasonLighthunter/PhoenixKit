import Core
import Requestable
import Foundation

public class PostFollow: KitsuObject<PostFollowAttributes>, Requestable {
  public static var requestURLString = "post-follows"
}

public class PostFollowAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
