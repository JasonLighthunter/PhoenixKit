import Core
import Requestable
import Foundation

public class CommentLike: KitsuObject<CommentLikeAttributes>, Requestable {
  public static var requestURLString = "comment-likes"
}

public class CommentLikeAttributes: KitsuObjectAttributesWithTimestamp {
  public let createdAt: Date
  public let updatedAt: Date
}
