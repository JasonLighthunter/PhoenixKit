// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "PhoenixKit",
  products: [
    .library(name: "Core", targets: ["Core"]),
    .library(name: "Groups", targets: ["Groups"]),
    .library(name: "Image", targets: ["Image"]),
    .library(name: "CharactersAndPeople", targets: ["CharactersAndPeople"]),
    .library(name: "Media", targets: ["Media"]),
    .library(name: "Reactions", targets: ["Reactions"]),
    .library(name: "Users", targets: ["Users"]),
    .library(name: "SiteAnnouncements", targets: ["SiteAnnouncements"]),
    .library(name: "Posts", targets: ["Posts"]),
  ],
  dependencies: [
    .package(url: "https://gitlab.com/JasonLighthunter/Requestable.git", from: "1.0.4"),
  ],
  targets: [
    .target(name: "Core", dependencies: ["Requestable"]),
    .target(name: "Image", dependencies: []),
    .target(name: "CharactersAndPeople", dependencies: ["Core", "Image"]),
    .target(name: "Groups", dependencies: ["Core", "Image"]),
    .target(name: "Posts", dependencies: ["Core", "Image"]),
    .target(name: "SiteAnnouncements", dependencies: ["Core", "Image"]),
    .target(name: "Media", dependencies: ["Core", "Image"]),
    .target(name: "Reactions", dependencies: ["Core"]),
    .target(name: "Users", dependencies: ["Core", "Image"]),
    .testTarget(name: "CoreTests", dependencies: ["Core"]),
    .testTarget(name: "GroupsTests", dependencies: ["Groups"]),
    .testTarget(name: "ImageTests", dependencies: ["Image"]),
    .testTarget(name: "PostsTests", dependencies: ["Posts"]),
    .testTarget(name: "SiteAnnouncementsTests", dependencies: ["SiteAnnouncements"]),
    .testTarget(name: "CharactersAndPeopleTests", dependencies: ["CharactersAndPeople"]),
    .testTarget(name: "MediaTests", dependencies: ["Media"]),
    .testTarget(name: "ReactionsTests", dependencies: ["Reactions"]),
    .testTarget(name: "UsersTests", dependencies: ["Users"]),
  ]
)
